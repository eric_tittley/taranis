#ifndef _CONFIG_H_
#define _CONFIG_H_

#define DEBUG
// Verbose 2 enables V_LOG output
#define VERBOSE 0

#define MIN_SOURCES_PER_GPU 4
#define MAX_SOURCES_PER_GPU 8

#define ADAPTIVE_TIMESCALE

// #define STATIC_TEST
#define GIZMO

#ifdef STATIC_TEST
/* Uncomment HEALPIX_RAYS for Test 3*/
#  define HEALPIX_RAYS
#  define DUMP_RT_DATA
#endif

/* COOLING / NO_COOLING when run with Gizmo
 * Cooling should be enabled in both Gizmo and Taranis.
 * There is a flag for EDINBURGH_RT in gizmo/cooling/cooling.c that
 * makes it only use the cooling coming from the hydro solver.
 * Taranis does all of the Hydrogen/Helium specific cooling.
 */

/* When run under Gizmo, sources can be generated from FOF halos
 * or from star particles.
 * There are two methods for generating sources from halos that are
 * found by Gizmo's FoF method:
 *  FOF_SOURCES  Turns on code in gizmoSetSources.cpp
 *               Places a source at the halo CoM
 *               Luminosity is sum of spectra of the star particles in that halo
 *  MASON_2015_SOURCES  Turns on code in gizmoHaloSources.cpp
 *                      Places a source at the halo CoM
 *                      Luminosity based on a halo mass to luminosity conversion
 * Both methods require FOF_RT_SOURCES and FOF set in gizmo-rt/Config.sh
 * There is one way to generate sources from stars: STELLAR_SOURCES
 *  STELLAR_SOURCES Turns code on in gizmoStellarSources.cpp
 *                  Takes each star particle as a source
 * Only use *one* of these *_SOURCES methods.
 */

#ifdef GIZMO
/* Uses healpix rays instead of random */
#  define HEALPIX_RAYS
// #  define NO_COOLING
// #  define STELLAR_SOURCES
// #  define FOF_SOURCES
#  define MASON_2015_SOURCES
#endif

/* NO_COOLING turns off cooling *other than cooling from recombination* */
// #define NO_COOLING

/* The Precision of the RT & RT_CUDA libraries.
 * Default is single */
// #define RT_DOUBLE

/* If running a test that has the source at the origin */
// #define SINGLE_OCTANT_RAYS

/* Define USE_B_RECOMBINATION_CASE to use the alpha_B and beta_B
 * coefficients when the optical depth is large (or in a refinement),
 * otherwise alpha_a and beta_a are used. */
#define USE_B_RECOMBINATION_CASE

/* USE_B_RECOMBINATION_CASE_ONLY is usually wrong. Here for testing purposes */
// #define USE_B_RECOMBINATION_CASE_ONLY

/* If neither USE_B_RECOMBINATION_CASE nor USE_B_RECOMBINATION_CASE_ONLY
 * are defined, then the A coefficients are used exclusively */

/* Uncomment to dump the calculated column densities */
// #define DUMP_COLUMN_DENSITIES

/* Uses healpix rays instead of random */
// #define HEALPIX_RAYS

/* Monitor the use of GPU memory.  Adds verbosity to stdout */
// #define MONITOR_GPU_MEMORY

/* Use times provided in output times file to create snapshots
 * Should not be used if coupled to GIZMO
 */
// #define DUMP_RT_DATA

/* Use the following to smooth equilibrium values using the form
 *  Vnew = 0.3 Vsugg + 0.7 Vold
 * Smoothing equilibrium values can be useful if oscillations occur.
 * Otherwise avoid because it reduces performance. */
// #define SMOOTH_EQUILIBRIUM_VALUES

/* Progressive MaxSizeTimeStep increases the MaxSizeTimeStep (allvars.h)
 * fractionally each step. This may help catch early-time ionization
 * hydrodynamic responses.
 * CURRENTLY THIS BREAKS GIZMO. LEADS TO BLOCKY STRUCTURE in Test 5 & Test 6. */
// #define PROGRESSIVE_MaxSizeTimestep

/* Track ionization rates.  Just information, no change to results. */
#define TRACK_IONIZATION_RATE

/*
 * Collisional Ionization caused by electrons
 */
// #define COLLISIONAL_IONIZATION

/*
 * For testing cooling and collisional ionizations.
 */
// #define DISABLE_RT_SOURCES

#endif /* _CONFIG_H_ */
