#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#include "RT_Precision.h"

struct ParametersT_struct {
 unsigned int ProblemType;
 char * GadgetFile;
 double InitialTemperature; /* K */
 double t_start;    /* s */
 double t_stop; /* s */
 int Nsph; /* Number of particles in an SPH search radius */
 size_t nSources;
 rt_float columnDensityThreshold_H1;
 rt_float columnDensityThreshold_He1;
 rt_float columnDensityThreshold_He2;
 rt_float totalSourceLuminosity;
 char * HaloCatalogueFile;
 size_t N_rays;
 double *InitialFractions;
 size_t NElements_InitialFractions;
 size_t OutputIterationCount;
 char * OutputTimesFile;
};

typedef struct ParametersT_struct ParametersT;

enum ParametersEntries { iProblemType,
                         iGadgetFile,
                         iInitialTemperature,
                         it_start,
                         it_stop,
                         iNsph,
                         inSources,
                         icolumnDensityThreshold_H1,
                         icolumnDensityThreshold_He1,
                         icolumnDensityThreshold_He2,
                         itotalSourceLuminosity,
                         iHaloCatalogueFile,
                         iN_rays,
                         iInitialFractions,
                         iOutputIterationCount,
                         iOutputTimesFile,
                         nParameters
                       };

#endif
