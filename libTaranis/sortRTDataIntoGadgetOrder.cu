#include <Taranis.h>
#include <Taranis.cuh>

void
sortRTDataIntoGadgetOrder(RT_Data * const Data_src,
                          thrust::host_vector<unsigned int> gadget_map) {

  size_t nParticles = Data_src->NumCells;

  RT_Data * Data_tmp = RT_Data_Allocate(nParticles);

 #pragma omp parallel for
  for(size_t i=0; i<nParticles; i++){
    size_t index = gadget_map[i];
    
    Data_tmp->R[index]       = Data_src->R[i];
    Data_tmp->dR[index]      = Data_src->dR[i];
    Data_tmp->Density[index] = Data_src->Density[i];
    Data_tmp->Entropy[index] = Data_src->Entropy[i];
    Data_tmp->T[index]       = Data_src->T[i];

    Data_tmp->n_H[index]   = Data_src->n_H[i];
    Data_tmp->f_H1[index]  = Data_src->f_H1[i];
    Data_tmp->f_H2[index]  = Data_src->f_H2[i];
    Data_tmp->n_He[index]  = Data_src->n_He[i];
    Data_tmp->f_He1[index] = Data_src->f_He1[i];
    Data_tmp->f_He2[index] = Data_src->f_He2[i];
    Data_tmp->f_He3[index] = Data_src->f_He3[i];

    Data_tmp->column_H1[index]  = Data_src->column_H1[i];
    Data_tmp->column_He1[index] = Data_src->column_He1[i];
    Data_tmp->column_He2[index] = Data_src->column_He2[i];
  }

  /* Data_tmp->MemSpace now points to the correctly sorted data.
   * and Data_tmp-><Vectors> are pointers to the correct space.
   * Need to free Data_src's memory and set Data_src to Data_tmp's Memspace
   * then set Data_src's pointers.
   * Finally, free Data_tmp (but not Data_tmp->MemSpace)
   */

  free(Data_src->MemSpace);
  Data_src->MemSpace = Data_tmp->MemSpace;
  RT_Data_Assign(Data_src);
  free(Data_tmp);
}
