#include <math.h>
#include <stdlib.h>
#include <sys/time.h>

#include <chrono>
#include <cstdint>

#include "Manidoowag.h"
#include "Taranis.cuh"
#include "Taranis.h"
#include "grace/cuda/generate_rays.cuh"
#include "grace/cuda/scan.cuh"
#include "grace/ray.h"
#include "grace/vector.h"

double randZeroToOne() { return (double)rand() / ((double)RAND_MAX + 1.); }

int generateHealpixRaysFromMultipleSources(thrust::device_vector<grace::Ray> &d_rays,
                                           size_t const N_rays_per_source,
                                           size_t const N_sources,
                                           const size_t source_start,
                                           RT_SourceT *sources,
                                           float const rayLength,
                                           thrust::device_vector<float> &ray_directions) {
  thrust::host_vector<float> rotation_matrix(N_sources * 9);
  for (int i = 0; i < N_sources; i++) {
    float Tx, Ty, Tz;
    Tx = (float)(randZeroToOne() * 2. * M_PI);
    Ty = (float)(randZeroToOne() * 2. * M_PI);
    Tz = (float)(randZeroToOne() * 2. * M_PI);
    /* Rotation Matrix */
    rotation_matrix[i * 9 + 0] = cosf(Ty) * cosf(Tz);
    rotation_matrix[i * 9 + 1] = -(cosf(Ty) * sinf(Tz));
    rotation_matrix[i * 9 + 2] = sinf(Ty);
    rotation_matrix[i * 9 + 3] = cosf(Tz) * sinf(Tx) * sinf(Ty) + cosf(Tx) * sinf(Tz);
    rotation_matrix[i * 9 + 4] = cosf(Tx) * cosf(Tz) - sinf(Tx) * sinf(Ty) * sinf(Tz);
    rotation_matrix[i * 9 + 5] = -(cosf(Ty) * sinf(Tx));
    rotation_matrix[i * 9 + 6] = -(cosf(Tx) * cosf(Tz) * sinf(Ty)) + sinf(Tx) * sinf(Tz);
    rotation_matrix[i * 9 + 7] = cosf(Tz) * sinf(Tx) + cosf(Tx) * sinf(Ty) * sinf(Tz);
    rotation_matrix[i * 9 + 8] = cosf(Tx) * cosf(Ty);
  }
  thrust::host_vector<float4> origins(N_sources);
  for (size_t i = source_start; i < source_start + N_sources; i++) {
    origins[i - source_start].x = sources[i].r[0];
    origins[i - source_start].y = sources[i].r[1];
    origins[i - source_start].z = sources[i].r[2];
    origins[i - source_start].w = rayLength;
  }

  thrust::device_vector<float> d_rotation_matrix = rotation_matrix;
  thrust::device_vector<float4> d_origins = origins;

  grace::healpix_rays_multiple_source(d_rays,
                                      N_rays_per_source,
                                      N_sources,
                                      thrust::raw_pointer_cast(d_origins.data()),
                                      ray_directions,
                                      d_rotation_matrix);
  return EXIT_SUCCESS;
}

int generateHealpixRays(thrust::device_vector<grace::Ray> &d_rays,
                        RT_SourceT const &source,
                        float const rayLength,
                        thrust::device_vector<float> &ray_directions) {
  thrust::host_vector<float> rotation_matrix(9);

  float Tx, Ty, Tz;
  Tx = (float)(randZeroToOne() * 2. * M_PI);
  Ty = (float)(randZeroToOne() * 2. * M_PI);
  Tz = (float)(randZeroToOne() * 2. * M_PI);

  /* Rotation Matrix */
  rotation_matrix[0] = cosf(Ty) * cosf(Tz);
  rotation_matrix[1] = -(cosf(Ty) * sinf(Tz));
  rotation_matrix[2] = sinf(Ty);
  rotation_matrix[3] = cosf(Tz) * sinf(Tx) * sinf(Ty) + cosf(Tx) * sinf(Tz);
  rotation_matrix[4] = cosf(Tx) * cosf(Tz) - sinf(Tx) * sinf(Ty) * sinf(Tz);
  rotation_matrix[5] = -(cosf(Ty) * sinf(Tx));
  rotation_matrix[6] = -(cosf(Tx) * cosf(Tz) * sinf(Ty)) + sinf(Tx) * sinf(Tz);
  rotation_matrix[7] = cosf(Tz) * sinf(Tx) + cosf(Tx) * sinf(Ty) * sinf(Tz);
  rotation_matrix[8] = cosf(Tx) * cosf(Ty);
  thrust::device_vector<float> d_rotation_matrix = rotation_matrix;
  grace::healpix_rays(d_rays,
                      (float)source.r[0], /* Gadget units */
                      (float)source.r[1], /* Gadget units */
                      (float)source.r[2], /* Gadget units */
                      rayLength,          /* Gadget units */
                      ray_directions,
                      d_rotation_matrix);

  return EXIT_SUCCESS;
}

unsigned long long rdtsc() {
  unsigned int lo, hi;
  __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
  return ((unsigned long long)hi << 32) | lo;
}

int generateRandomRays(thrust::device_vector<grace::Ray> &d_rays, RT_SourceT const &source, float const rayLength) {
  grace::Vector<3, float> origin;
  origin.x = (float)source.r[0]; /* Gadget units */
  origin.y = (float)source.r[1]; /* Gadget units */
  origin.z = (float)source.r[2]; /* Gadget units */

  grace::PrngStates rng_states(rdtsc());

  grace::uniform_random_rays(origin,
                             rayLength, /* Gadget units */
                             rng_states,
                             d_rays);
  return EXIT_SUCCESS;
}

int generatePlaneParallelRays(thrust::device_vector<grace::Ray> &d_rays, RT_SourceT source, float const rayLength) {
  int N_rays = d_rays.size();
  int N_rays_side = std::floor(std::pow((double)N_rays, 0.5000000000001));
  while (N_rays_side * N_rays_side > N_rays) {
    N_rays_side -= 1;
  }
  if ((N_rays_side * N_rays_side) != N_rays) {
    printf("ERROR: N_rays=%i is not a square of an integer\n", N_rays);
    return EXIT_FAILURE;
  }

  grace::plane_parallel_uniform_rays(d_rays, (size_t)N_rays_side, source.PlaneSide);

  return EXIT_SUCCESS;
}
