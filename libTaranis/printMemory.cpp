#include <stdio.h>

#include <cuda_runtime_api.h>

#include "Taranis.h"

void
printMemory(int const CudaDevice1) {

 int currentDevice;
 cudaError_t err;

 size_t free1, total1;

 // First, check if there are any residual errors
 err = cudaGetLastError();
 if(err != cudaSuccess ) {
  cudaGetDevice(&currentDevice);
#if CUDA_VERSION >= 6050
  printf("Residual error: device %i: error:%s : %s\n",
         currentDevice,
         cudaGetErrorName(err),
         cudaGetErrorString(err));
#else
  printf("Residual error: device %i: %s\n",
         currentDevice,
         cudaGetErrorString(err));
#endif
 }

 err = cudaGetDevice(&currentDevice);
 if(err != cudaSuccess ) {
  printf("cudaGetDevice returns: %s\n",
         cudaGetErrorString(err));
 }

 err = cudaSetDevice(CudaDevice1);
 if(err != cudaSuccess ) {
  printf("cudaSetDevice returns: %s\n",
         cudaGetErrorString(err));
 }

 err = cudaMemGetInfo(&free1,&total1);
 if(err != cudaSuccess ) {
  printf("cudaMemGetInfo(%i) returns: %s\n",
         CudaDevice1,cudaGetErrorString(err));
 }

 printf("Device %i: Total: %10lu Free: %10lu\n",
        CudaDevice1, total1, free1); 

 err = cudaSetDevice(currentDevice);
 if(err != cudaSuccess ) {
  printf("cudaSetDevice returns: %s\n",
         cudaGetErrorString(err));
 }

 return;
}
