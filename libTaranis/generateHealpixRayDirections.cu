#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#include "Taranis.h"
#include "Taranis.cuh"

#include "libChealpix/chealpix.h"

//Generates directions for healpix rays. only needs to be called once.
int
generateHealpixRayDirections(size_t const N_rays,
                             thrust::device_vector<float>& ray_directions) {

 thrust::host_vector<float> host_ray_directions(N_rays*3);

 long const nside = npix2nside((long)N_rays);
 if(nside<0) {
  printf("%s: %i: ERROR: N_Rays must be an integer that satisfies 12 N^2\n where N is an integer; N_rays=%lu\n",
         __FILE__,__LINE__,N_rays);
  return EXIT_FAILURE;
 }

 /* Generate the ray vectors, returns normalised unit vectors */
 for(long vec_index=0; vec_index<N_rays; ++vec_index) {
  double rayVector[3];
  pix2vec_ring(nside, vec_index, &rayVector[0]);
  host_ray_directions[vec_index]          = (float)rayVector[0];
  host_ray_directions[vec_index+  N_rays] = (float)rayVector[1];
  host_ray_directions[vec_index+2*N_rays] = (float)rayVector[2];
 }
 //copy to gpu
 ray_directions = host_ray_directions;
 return EXIT_SUCCESS;
}
