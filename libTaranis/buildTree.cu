#include "Taranis.cuh"

#include "grace/cuda/build_sph.cuh"
#include "grace/cuda/sort.cuh"

int buildTree(thrust::device_vector<SphereType> &d_particles,
              /* Output required for grace */
              grace::CudaBvh &d_tree,
              /* Output made available to host code */
              thrust::host_vector<unsigned int> &gadget_map) {
 size_t N = d_particles.size();

 /* Build the tree. */

 // Can be grace::uinteger64 for 63-bit Morton keys.
 thrust::device_vector<grace::uinteger32> d_keys(N);
 grace::morton_keys_sph(d_particles, d_keys);

 // gadget_map[i] is the pre-morton-key-ordered index of the ith particle.
 //
 // Using d_map and order_by_index, we could alternatively re-order
 // h_gadget_IDs instead!  A map is probably more useful in general.
 thrust::device_vector<unsigned int> d_map(N);
 grace::sort_and_map(d_keys, d_map);
 grace::order_by_index(d_map, d_particles);
 /* Copy gadget_map to host memory at leisure */
 gadget_map = d_map;
 d_map.clear(); d_map.shrink_to_fit();

 thrust::device_vector<float> d_deltas(N+1);
 grace::euclidean_deltas_sph(d_particles, d_deltas);
 grace::ALBVH_sph(d_particles, d_deltas, d_tree);

 return EXIT_SUCCESS;
}
