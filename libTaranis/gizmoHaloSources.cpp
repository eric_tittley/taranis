#include <allvars.h>

#include "config.h"

#ifdef MASON_2015_SOURCES
extern "C" {
#  include <proto.h>
#  include <structure/fof.h>

#  include "RT_Luminosity.h"
}

#  include <algorithm>
#  include <cstdio>
#  include <numeric>
#  include <vector>

#  include "Mason_2015_tables.h"
#  include "ERT_gizmo.h"
#  include "taranis_vars.h"

constexpr double ESCAPE_FRACTION = 0.5;

// hopefully correct factors to scale luminosities by to get the same number of ionizing photons
// trunc is if we set L(nu>nu_2) = 0 relative to the starburst spectrum
// full is if we extrapolate to infinity.
constexpr double trunc_factor = 1.6237;
constexpr double full_factor = 0.8793;

// list is ordered in ascending values and return the first index where list[i] > target
int find_upper_index(rt_float *list, rt_float target) {
  for (int i = 0; i < 61; i++) {
    if (list[i] > target) {
      return i;
    }
  }
  printf("Halo Mass too large\n");
}

rt_float interpolate_segment(rt_float x0, rt_float x1, rt_float y0, rt_float y1, rt_float x) {
  float t;

  if (x <= x0) {
    return y0;
  }
  if (x >= x1) {
    return y1;
  }

  t = (x - x0);
  t /= (x1 - x0);

  return y0 + t * (y1 - y0);
}

rt_float get_sfr_from_halo_mass(rt_float mass, rt_float redshift) {
  if (redshift > 11.9) {
    int idx = find_upper_index(&Mason_z_120_mass[0], mass);
    return interpolate_segment(
        Mason_z_120_mass[idx - 1], Mason_z_120_mass[idx], Mason_sfr[idx - 1], Mason_sfr[idx], mass);
  }
  if (redshift > 9.9) {
    int idx = find_upper_index(&Mason_z_104_mass[0], mass);
    return interpolate_segment(
        Mason_z_104_mass[idx - 1], Mason_z_104_mass[idx], Mason_sfr[idx - 1], Mason_sfr[idx], mass);
  }
  if (redshift > 8.9) {
    int idx = find_upper_index(&Mason_z_090_mass[0], mass);
    return interpolate_segment(
        Mason_z_090_mass[idx - 1], Mason_z_090_mass[idx], Mason_sfr[idx - 1], Mason_sfr[idx], mass);
  }
  if (redshift > 7.8) {
    int idx = find_upper_index(&Mason_z_079_mass[0], mass);
    return interpolate_segment(
        Mason_z_079_mass[idx - 1], Mason_z_079_mass[idx], Mason_sfr[idx - 1], Mason_sfr[idx], mass);
  }
  if (redshift > 6.7) {
    int idx = find_upper_index(&Mason_z_068_mass[0], mass);
    return interpolate_segment(
        Mason_z_068_mass[idx - 1], Mason_z_068_mass[idx], Mason_sfr[idx - 1], Mason_sfr[idx], mass);
  }
  printf("Current redshift is not supported\n");
  exit(1);
  return -1;
}

static rt_float RT_Luminosity_Mason_2015(RT_LUMINOSITY_ARGS) {
  rt_float mass = Source->halo_mass;
  rt_float redshift = 1. / All.Time - 1.0;
  rt_float sfr = get_sfr_from_halo_mass(mass, redshift);

  rt_float L_nu = 0;
#  ifdef SOLAR_METALICITY
  /*
   * Fits to starburst model with Geneva v00 tracks, Metalilicity 0.014 and a continuous SFR of 1MSun/yr.
   * Units W/Hz
   * Data taken at 50Ma but the models reach equilibrium much sooner.
   */
  if (nu < Constants->nu_1) {
    L_nu = 2.28399568601793e+20 * std::pow(nu / Constants->nu_0, -0.483522157519880);
  } else if (nu < Constants->nu_2) {
    L_nu = 5.59448395402245e+20 * std::pow(nu / Constants->nu_0, -1.08549456983205);
  } else {
    L_nu = 2.88598313537573e+22 * std::pow(nu / Constants->nu_0, -1.91408055518706);
  }
#  else
  /*
   * Fits to starburst model with Geneva v00 tracks, Metalilicity 0.002 and a continuous SFR of 1MSun/yr.
   * Units W/Hz
   * Data taken at 50Ma but the models reach equilibrium much sooner.
   */
  if (nu < Constants->nu_1) {
    L_nu = 3.154495209079984e+20 * std::pow(nu / Constants->nu_0, -0.381806264792354);
  } else if (nu < Constants->nu_2) {
    L_nu = 2.695780339861039e+20 * std::pow(nu / Constants->nu_0, -0.566416780514909);
  } else {
    L_nu = 2.701975034726230e+21 * std::pow(nu / Constants->nu_0, -1.456779037197478);
  }
#  endif
  return L_nu * sfr * Source->luminosity_scale_factor;
}

void ert_set_sources_from_fof(fof_particle_list *particleList) {
  // Ngroups and Group are available from fof
  printf("Setting sources [MASON_2015_SOURCES]...\n");
#  ifdef DISABLE_RT_SOURCES
  return
#  endif
      _num_sources = Ngroups;
  rt_float total_sfr = 0;
  for (size_t i = 0; i < Ngroups; i++) {
    const group_properties &group = Group[i];
    for (size_t j = 0; j < 3; j++) _Source[i].r[j] = group.CM[j];
    _Source[i].AttenuationGeometry = RaySphericalWave;
    _Source[i].SourceType = SourceUser;
    _Source[i].LuminosityFunction = (SourceFunctionT)&RT_Luminosity_Mason_2015;
    _Source[i].halo_mass = group.Mass * UNIT_MASS_IN_SOLAR;
    _Source[i].luminosity_scale_factor = ESCAPE_FRACTION / _Parameters.N_rays;
    rt_float redshift = 1. / All.Time - 1.0;
    rt_float sfr = get_sfr_from_halo_mass(_Source[i].halo_mass, redshift);
    total_sfr += sfr;
    printf("Source %d: mass=%e\n", i, group.Mass * UNIT_MASS_IN_SOLAR);
    /*
        rt_float nu_end = 50000.0 * _Constants.nu_0;
        dnu = FMAX(1e12, Source->nu_end * (10.0 * std::numeric_limits<rt_float>::epsilon));
        printf("nu_end=%5.3e; nu_0=%5.3e; dnu=%5.3e; \n", nu_end, _Constants.nu_0, dnu);
        rt_float Redshift = (Source->z_on + Source->z_off) / 2.0;
        rt_float (*LuminosityFunction)(RT_LUMINOSITY_ARGS) =
       (rt_float(*)(RT_LUMINOSITY_ARGS))_Source[i]->LuminosityFunction; rt_float sum = 0.0; rt_float nu; for (nu =
       nu_end; nu >= _Constants.nu_0; nu -= dnu) { sum += (*(LuminosityFunction))(nu, Redshift, _Source[i], &_Constants)
       / (nu * _Constants.h_p * 1e30);  //PetaN per Hz if (!isfinite(sum)) printf("nu=%5.3e; sum=%5.3e\n", nu, sum);
        }
        sum *= dnu;
        printf("Between nu = %5.3e and %5.3e,\n", _Constants.nu_0, nu_end);
        printf(
            "Source generates %5.3e x 1e30 ionizing photons per second at "
            "z=%5.3e\n",
            sum,
            Redshift);
        printf("hopefully to within 1%%.\n");
        printf(" Note: not using the same integration scheme as used in the code\n");
        (void)fflush(stdout);
    */
  }
  printf("Total sfr (MSun/yr) in all sources = %e\n", total_sfr);
}

#endif  // MASON_2015_Sources