#include <stdlib.h>
extern "C" {
#include "RT.h"
}

size_t const NumValidRays=25;
size_t const HealpixValidRays[] = {
    192,         768,        1728,        3072,        4800,
   6912,        9408,       12288,       15552,       19200,
  23232,       27648,       32448,       37632,       43200,
  49152,       55488,       62208,       69312,       76800,
  84672,       92928,      101568,      110592,      120000
 };

size_t
progressiveHealpixRays(rt_float FractionOfTotalTime,
                       size_t MaxNRays)
{
#ifndef HEALPIX_RAYS
 printf("ERROR: progressiveHealpixRays called but HEALPIX_RAYS not defined. Exiting.\n");
 exit(-1);
#endif
 size_t SuggestedNRays=(size_t)floor(FractionOfTotalTime*(rt_float)MaxNRays);
 size_t closestNRays = HealpixValidRays[0];
 for(size_t i=1; i<NumValidRays; ++i) {
  if(  labs(closestNRays-SuggestedNRays)
     > labs(HealpixValidRays[i]-SuggestedNRays) )
  {
   closestNRays = HealpixValidRays[i];
  }
 }
 
 return closestNRays;
}