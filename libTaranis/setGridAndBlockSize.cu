#include <cuda_runtime.h>

#include "Taranis.h"

void
setGridAndBlockSize(size_t const nLoopElements,
                    void* KernelPointer,
                    int* gridSize,
                    int* blockSize ) {

 int maxGridSize;    // The maximum grid size allowed. This depends
                     // on the device architecture. (65535 for compute
                     // capability < 3.0, 2^31 - 1 otherwise.)
 int deviceID;
 cudaGetDevice(&deviceID);
 cudaDeviceGetAttribute(&maxGridSize, cudaDevAttrMaxGridDimX, deviceID);
#if CUDA_VERSION >= 6050
 int minGridSize;    // The minimum grid size needed to achieve the
                     // maximum occupancy for a full device
                     // launch.
 cudaOccupancyMaxPotentialBlockSize(
     &minGridSize,
     blockSize,
     KernelPointer,
     0,
     (int)nLoopElements);
#else
 /* 512 threads/block is a sensible upper-limit when occupancy cannot be
  * (conveniently) calculated for us; however, it may be too large and cause a
  * "too many resources requested for launch" error. */
 *blockSize = 512;
 cudaFuncAttributes attr;
 cudaError_t cuError = cudaFuncGetAttributes(&attr, KernelPointer);
 /* API does not state that a multiple of the warp size, 32, is guaranteed. */
 if(*blockSize > attr.maxThreadsPerBlock) {
    *blockSize = (attr.maxThreadsPerBlock / 32) * 32;
 }
#endif

 // Round up according to array size
 *gridSize = (int)((nLoopElements + (size_t)(*blockSize) - 1) / (size_t)(*blockSize));
 if (*gridSize > maxGridSize) {
     *gridSize = maxGridSize;
 }

}
