#include "Taranis.cuh"

__device__ rt_float
retrieveArrayElementInSPPOrder(size_t const i, /* Index in SPP order */
			       size_t const nParticles,
                               rt_float const * const ArrayHostOrder,
                               unsigned int const * const particle_indices,
                               unsigned int const * const gadget_map,
                               rt_float const FillValueForEoRMarker ) {
 rt_float ElementSSPOrder;

 size_t const PI = (size_t) particle_indices[i];
 if( PI < nParticles ) {
  size_t const GM = gadget_map[PI];
  ElementSSPOrder = ArrayHostOrder[GM];
 } else {
  /* Index points to an End-Of-Ray marker */
  ElementSSPOrder = FillValueForEoRMarker;
 }
 return ElementSSPOrder;
}
