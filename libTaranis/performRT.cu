#include "RT_CUDA.h"
#include "Taranis.cuh"
#include "Taranis.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "RT_FileHeaderT.h"
#ifdef __cplusplus
}
#endif

#include <omp.h>
#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/system_error.h>
#include <unistd.h>

#include "nccl.h"

int performRT(/* Input, not modified. Supplied by host code. */
              RT_SourceT *const Source,
              size_t const nSource,
              thrust::host_vector<SphereType> &h_particles,
              rt_float const t_start,
              rt_float const t_stop,
              rt_float const UnitLength,
              rt_float const Redshift,
              rt_float const BoxSize,
              float const RayLength,
              size_t const N_rays,
              double const N_H_per_particle,
              double const N_He_per_particle,
              RT_ConstantsT Constants,
              int const CudaDeviceTaranis, /* Is this needed anymore? */
              /* Modified */
              RT_Data Data_host,
              /* Optional, not modified */
              rt_float const *const OutputTimes,
              rt_float const outputTimesUnit,
              size_t const nOutputTimes,
              size_t const FirstOutputTimeIndex,
              size_t const DumpIterationCount) {
  char Dir[] = "./save";
  size_t const LABEL_STR_SIZE = 32;
  char RTDataFile[LABEL_STR_SIZE];

  size_t const nParticles = h_particles.size();
  lists_t lists;

  int ierr;
  rt_float InitialTemperature = 0.0;
  if (Source->SourceType == SourceMonochromatic) {
    /* Test 1 is ISOTHERMAL
     * Remember Entropy and Temperature
     * This is a horrible test making a horrible assumption. */
    InitialTemperature = Data_host.T[0];
  }

  /* The following should be a parameter */
  const int max_per_leaf = 32;  // for best performance.  =1 for original.
  cudaError_t cudaError;

#ifdef MONITOR_GPU_MEMORY
  printMemory(CudaDeviceTaranis);
#endif

  /* Control the visible GPUs via the environment variable
   * CUDA_VISIBLE_DEVICES [comma-separated list of devices]
   */
  int nDev = 0;
  cudaGetDeviceCount(&nDev);
  printf("Using nDev=%i GPU(s) for the RT\n",nDev);

  int *devs = (int *)malloc(sizeof(int) * nDev);
  for (int i = 0; i < nDev; i++) {
    devs[i] = i;
  }

  /* Increase the stack size per thread */
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    size_t pValue;
    bool const TerminateOnCudaError = true;
    cudaError = cudaDeviceGetLimit(&pValue, cudaLimitStackSize);
    checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
    printf("Current LimitStackSize=%lu. Setting to 2048 bytes\n", pValue);
    cudaError = cudaDeviceSetLimit(cudaLimitStackSize, 2048);
    checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  }

  /* Copy particle data to the GPU and build tree */
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    thrust::device_vector<SphereType> *d_particles = new thrust::device_vector<SphereType>(nParticles);
    thrust::host_vector<unsigned int> *host_map = new thrust::host_vector<unsigned int>(nParticles);
    grace::CudaBvh *d_tree = new grace::CudaBvh(nParticles, max_per_leaf);

    thrust::copy(h_particles.begin(), h_particles.end(), (*d_particles).begin());

    buildTree(*d_particles, *d_tree, *host_map);

    lists.particles.push_back(d_particles);
    lists.tree.push_back(d_tree);
    lists.map.push_back(host_map);
  }
  std::cout << "CUDA_VERSION = " << CUDA_VERSION << std::endl;

  /* Allocate RT Data on the GPU
   * then copy from the host to the device. */
  /*
   * We only need to copy RT_Data to devs[0]. The first step each dt in
   * loopOverTime will sync all devices to the RT_Data on devs[0]. It's better
   * to use a nccl broadcast to do the copies rather than doing each one manually.
   */
#if VERBOSE > 1
  std::cout << "Allocating memory for RT Data and copying to device...\n";
#endif
  cudaSetDevice(devs[0]);
  RT_Data Data_dev;
  copyRTDataToDeviceInGraceOrder(Data_host, &Data_dev, *lists.map[0]);
  lists.data.push_back(&Data_dev);
  size_t const sizeOfHostMemSpace = Data_host.NBytes;
  for (int i = 1; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    RT_Data *Data_dev_i = new RT_Data();
    cudaMalloc(&(Data_dev_i->MemSpace), sizeOfHostMemSpace);
    Data_dev_i->NBytes = Data_host.NBytes;
    Data_dev_i->NumCells = Data_host.NumCells;
    RT_Data_Assign(Data_dev_i);
    lists.data.push_back(Data_dev_i);
  }
#if VERBOSE > 1
  std::cout << " done" << std::endl;
#endif
#ifdef MONITOR_GPU_MEMORY
  printMemory(CudaDeviceTaranis);
#endif
  /* Allocate Rates on GPU, a set for each particle */
#if VERBOSE > 1
  std::cout << "Allocating memory for Rates ...";
#endif
  lists.rates.resize(nDev);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    lists.rates[i] = RT_CUDA_RatesBlock_Allocate(nParticles);
    lists.rates[i].NumCells = nParticles;
  }
  cudaSetDevice(devs[0]);
  /* Rates_dev_RT is a host variable with pointers that point to device
   * memory */
#if VERBOSE > 1
  std::cout << " done" << std::endl;
#endif
#ifdef MONITOR_GPU_MEMORY
  printMemory(CudaDeviceTaranis);
#endif

// generate healpix directions
#ifdef HEALPIX_RAYS
  std::cout << "Using HEALPIX Rays" << std::endl;
#  if VERBOSE > 1
  std::cout << "Generating Healpix directions...";
#  endif
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    thrust::device_vector<float> *ray_dir = new thrust::device_vector<float>();
    ierr = generateHealpixRayDirections(N_rays, *ray_dir);
    if (ierr == EXIT_FAILURE) {
      printf("ERROR: %s: %i: Failed to generate Healpix Ray Directions.\n", __FILE__, __LINE__);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
    lists.ray_dir.push_back(ray_dir);
  }
  cudaSetDevice(devs[0]);
#  if VERBOSE > 1
  std::cout << " done" << std::endl;
#  endif
#endif  // HEALPIX_RAYS

  // TODO move to function
  /* Save the initial conditions, prior to the first iteration */
  ierr = snprintf(RTDataFile, LABEL_STR_SIZE - 1, "%s/RTData_t=START", Dir);
  if (ierr > (int)LABEL_STR_SIZE) {
    printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTDataFile);
    exit(EXIT_FAILURE);
  }
  RT_FileHeaderT FileHeader;
  rt_float const ExpansionFactor = 1. / (Redshift + 1.);
  FileHeader.ExpansionFactor = (float)ExpansionFactor;
  FileHeader.Redshift = (float)Redshift;
  FileHeader.Time = 0.0;
  RT_Data *PointerToRT_Data = &Data_host;
  RT_Data **PointerToArrayOfPointersToRT_Data = &PointerToRT_Data;
  RT_SaveData(RTDataFile, PointerToArrayOfPointersToRT_Data, 1, FileHeader);

  // TODO: free RT_Data_host

#if VERBOSE < 1
  std::cout << "Starting main loop..." << std::endl;

#endif

  omp_set_dynamic(0);
  omp_set_nested(1);  // enables nested parallelism
#pragma omp parallel num_threads(2)
  {
#pragma omp master
    {
      loopOverTime(Source,
                   nSource,
                   t_start,
                   t_stop,
                   UnitLength,
                   Redshift,
                   BoxSize,
                   RayLength,
                   N_rays,
                   N_H_per_particle,
                   N_He_per_particle,
                   Constants,
                   nDev,
                   devs,
                   nParticles,
                   &lists,
/* Optional, not modified */
#ifdef DUMP_RT_DATA
                   OutputTimes,
                   outputTimesUnit,
                   nOutputTimes,
                   FirstOutputTimeIndex,
                   DumpIterationCount,
                   Dir,
#endif
                   InitialTemperature);
    }
  }
#if VERBOSE > 1
  std::cout << "...done loop over time" << std::endl;
#endif

  std::cout << "Freeing Data_dev.Memspace" << std::endl;
  for (int i = 0; i < nDev; i++) {
    cudaError = cudaFree((*lists.data[i]).MemSpace);
    if (cudaError != cudaSuccess) {
      printf("cudaFree on device %i returns: %s\n", devs[i], cudaGetErrorString(cudaError));
    }
  }

  for (int i = 0; i < nDev; i++) {
    delete lists.tree[i];
    delete lists.map[i];
    delete lists.particles[i];
  }
  free(devs);

  return EXIT_SUCCESS;
}
