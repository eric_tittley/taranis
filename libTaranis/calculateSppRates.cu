#include <nppdefs.h>
#include <thrust/device_vector.h>

#include "RT_CUDA.h"
#include "Taranis.cuh"
#include "Taranis.h"
#include "cudasupport.h"

// Shared memory for IntegralInvolatiles
extern __shared__ rt_float shared[];

static __device__ RT_IntegralInvolatilesT copyInvolatilesToSM(RT_IntegralInvolatilesT const Involatiles) {
  RT_IntegralInvolatilesT Involatiles_SM;
  Involatiles_SM.nFrequencies = Involatiles.nFrequencies;
  Involatiles_SM.nSpecies = Involatiles.nSpecies;
  Involatiles_SM.Frequencies = &shared[0 * Involatiles.nFrequencies];
  Involatiles_SM.Weights = &shared[1 * Involatiles.nFrequencies];
  Involatiles_SM.Luminosities = &shared[2 * Involatiles.nFrequencies];
  // NB: Alphas is nSpecies * nFrequencies, so we use (3+nSpecies) *
  // nFrequencies total SM
  Involatiles_SM.Alphas = &shared[3 * Involatiles.nFrequencies];
  for (int i = threadIdx.x; i < Involatiles.nFrequencies; i += blockDim.x) {
    Involatiles_SM.Frequencies[i] = Involatiles.Frequencies[i];
    Involatiles_SM.Weights[i] = Involatiles.Weights[i];
    Involatiles_SM.Luminosities[i] = Involatiles.Luminosities[i];
    for (int j = 0; j < Involatiles.nSpecies; ++j) {
      const auto idx = i * Involatiles.nSpecies + j;
      Involatiles_SM.Alphas[idx] = Involatiles.Alphas[idx];
    }
  }
  __syncthreads();
  return Involatiles_SM;
}

__global__ void cSppR_kernel(size_t const nSourceParticlePairs,
                             float const *const column_H1,
                             float const *const column_He1,
                             float const *const column_He2,
                             rt_float const ExpansionFactor,
                             RT_SourceT const Source,
                             RT_ConstantsT const Constants,
                             RT_IntegralInvolatilesT Involatiles,
                             unsigned int const *const particle_indices,
                             RT_Data const Data,
                             /* out */
                             RT_RatesBlockT Rates) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock
  RT_IntegralInvolatilesT const Involatiles_SM = copyInvolatilesToSM(Involatiles);

  /* nSourceParticlePairs - 1 so we skip the final dummy particle.
   * This ensures that column_Hx[iSPP + 1] is always a valid address.
   */
  for (size_t iSPP = istart; iSPP < (nSourceParticlePairs - 1); iSPP += span) {
    rt_float N_H1_local, N_He1_local, N_He2_local;
    rt_float DistanceFromSourceMpc, ParticleRadiusMpc, DistanceThroughElementMpc;
    size_t const particleIndex = particle_indices[iSPP];

    if (particleIndex >= Rates.NumCells) {
      continue;
    }

    /* This sets the column density through each particle (N_*_local)
     * by differencing column_*
     * Note that the end of every ray has
     * column_H1[NcellsInRay+1]= column_H1[NcellsInRay]+CellContribution
     */
    N_H1_local = (column_H1[iSPP + 1] - column_H1[iSPP]);
    N_He1_local = (column_He1[iSPP + 1] - column_He1[iSPP]);
    N_He2_local = (column_He2[iSPP + 1] - column_He2[iSPP]);
    /* If N_H1_local == 0, then they are ends of rays*/
    if (N_H1_local <= NPP_MINABS_32F) {
      continue;
    }

    /* Correct negatives due to the wrapping of rays */
    if (N_H1_local < 0.0) N_H1_local = 0.0;
    if (N_He1_local < 0.0) N_He1_local = 0.0;
    if (N_He2_local < 0.0) N_He2_local = 0.0;

    DistanceFromSourceMpc = Data.R[particleIndex] / Constants.Mpc;
#ifdef DEBUG
    if ((DistanceFromSourceMpc <= NPP_MINABS_32F) || !isfinite(DistanceFromSourceMpc)) {
      printf(
          "%s: %i: ERROR: DistanceFromSourceMpc=%5.3e; iSPP=%lu; "
          "Data.R[%lu]=%5.3e\n",
          __FILE__,
          __LINE__,
          DistanceFromSourceMpc,
          iSPP,
          particleIndex,
          Data.R[particleIndex]);
      return;
    }
#endif

    ParticleRadiusMpc = Data.dR[particleIndex] / Constants.Mpc;

    /* The DistanceThroughElementMpc calculation is flawed for the case that
     * f_H1 == 0.0 If this is true, then N_H1_local should also be 0.0 (though
     * it is found to not be). All the Rates should be zero (except Gamma_HI)
     * since they depend on tau_local which will be zero. For Gamma_HI,
     * DistanceThroughElementMpc is a factor in Attenuation that is
     * subsequently divided out.Hence, a value of unity (1Mpc) for
     * DistanceThroughElement should leave the Rates=0.0 and Gamma_HI set to
     * the appropriate value.
     */
    DistanceThroughElementMpc = N_H1_local / (Data.f_H1[particleIndex] * Data.n_H[particleIndex]);
    DistanceThroughElementMpc /= Constants.Mpc;
    if (Data.f_H1[particleIndex] < NPP_MINABS_32F) {
      DistanceThroughElementMpc = 1.0f;
    }

    auto Rates_local = RT_CUDA_PhotoionizationRates(N_H1_local,
                                                    N_He1_local,
                                                    N_He2_local,
                                                    column_H1[iSPP],
                                                    column_He1[iSPP],
                                                    column_He2[iSPP],
                                                    ExpansionFactor,
                                                    &Constants,
                                                    &Source,
                                                    DistanceFromSourceMpc,
                                                    ParticleRadiusMpc,
                                                    DistanceThroughElementMpc,
                                                    &Involatiles_SM);
    atomicAdd(&(Rates.I_H1[particleIndex]), Rates_local.I_H1);
    atomicAdd(&(Rates.I_He1[particleIndex]), Rates_local.I_He1);
    atomicAdd(&(Rates.I_He2[particleIndex]), Rates_local.I_He2);
    atomicAdd(&(Rates.G_H1[particleIndex]), Rates_local.G_H1);
    atomicAdd(&(Rates.G_He1[particleIndex]), Rates_local.G_He1);
    atomicAdd(&(Rates.G_He2[particleIndex]), Rates_local.G_He2);
    atomicAdd(&(Rates.Gamma_HI[particleIndex]), Rates_local.Gamma_HI);
  } /* end loop over spans */
}

void calculateSppRates(RT_Data const Data_dev,
                       const size_t nSourceParticlePairs,
                       thrust::device_vector<float>::iterator column_H1,
                       thrust::device_vector<float>::iterator column_He1,
                       thrust::device_vector<float>::iterator column_He2,
                       rt_float const sourceRedshift,
                       RT_SourceT Source,
                       RT_ConstantsT const Constants,
                       thrust::device_vector<unsigned int>::iterator particle_indices_dev,
                       RT_IntegralInvolatilesT const Involatiles_dev,
                       /* out */
                       RT_RatesBlockT Rates_dev) {
  rt_float const ExpansionFactor = 1 / (1 + sourceRedshift);

  V_LOG("Calling cSppR_Rates() ...");
  int gridSize;   // The actual grid size needed, based on input size
  int blockSize;  // The launch configurator returned block size
  setGridAndBlockSize(Data_dev.NumCells, (void *)cSppR_kernel, &gridSize, &blockSize);

  const auto memSize = (3 + Involatiles_dev.nSpecies) * Involatiles_dev.nFrequencies * sizeof(rt_float);
  auto col_H1_ptr = thrust::raw_pointer_cast(&column_H1[0]);
  auto col_He1_ptr = thrust::raw_pointer_cast(&column_He1[0]);
  auto col_He2_ptr = thrust::raw_pointer_cast(&column_He2[0]);
  auto particle_indices_ptr = thrust::raw_pointer_cast(&particle_indices_dev[0]);

  cSppR_kernel<<<gridSize, blockSize, memSize>>>(nSourceParticlePairs,
                                                 col_H1_ptr,
                                                 col_He1_ptr,
                                                 col_He2_ptr,
                                                 ExpansionFactor,
                                                 Source,
                                                 Constants,
                                                 Involatiles_dev,
                                                 particle_indices_ptr,
                                                 Data_dev,
                                                 /* out */
                                                 Rates_dev);

  checkCudaKernelLaunch(__FILE__, __LINE__);

  V_LOG("done\n");
}
