#include "Taranis.cuh"
#include "grace/cuda/scan.cuh"
#include "grace/cuda/sort.cuh"
#include "grace/cuda/trace_sph.cuh"

int trace_rays(thrust::device_vector<SphereType> const &d_particles,
               grace::CudaBvh const &d_tree,
               thrust::device_vector<grace::Ray> d_rays,
               /* Output, used by grace */
               thrust::device_vector<float> &d_kernel_integrals,
               thrust::device_vector<unsigned int> &d_hit_indices,
               thrust::device_vector<int> &d_ray_offsets,
               /* Output, made available to host code */
               thrust::host_vector<unsigned int> &particle_indices,
               thrust::host_vector<float> &kernel_integrals) {
  /* Perform a full trace. */
  size_t nParticles = d_particles.size();

  // The distance, along the ray, to each ray-particle intersection.
  thrust::device_vector<float> d_hit_distances;
  // The template parameter sets the precision of the kernel integral lookup
  // table and hence the kernel integral interpolation.  This may be
  // different from the precision of the output, d_kernel_integrals.
  //
  // d_kernel_integrals contains ray-end markers, or sentinels.  At these
  // locations, a value of 0 is given for the integral.
  // d_hit_indices also contains ray-end markers.  At these locations, the
  // dummy particle (with index N) is referenced.
  // d_hit_distances also contains ray-end markers.  At these locations, the
  // dummmy distance of positive infinity is provided.
  grace::trace_with_sentinels_sph(d_rays,
                                  d_particles,
                                  d_tree,
                                  d_ray_offsets,
                                  d_hit_indices,
                                  // Index for dummy particle.
                                  (int)nParticles,
                                  d_kernel_integrals,
                                  // Integral for dummy particle.
                                  0.0f,
                                  d_hit_distances,
                                  // Hit distance for dummy particle.
                                  std::numeric_limits<float>::infinity());
  // The per-intersection output.  That is, each ray integrated through
  // each intersected particle's smoothing kernel.
  // thrust::device_vector<float> d_kernel_integrals;
  // The index into d_particles to the corresponding particle for
  // each ray-particle intersection.  It has the same order as
  // d_kernel_integrals and d_hit_distances.
  // thrust::device_vector<unsigned int> d_hit_indices;

  grace::sort_by_distance(d_hit_distances, d_ray_offsets, d_hit_indices, d_kernel_integrals);

  d_hit_distances.clear();
  d_hit_distances.shrink_to_fit();
  /* Copy to host memory at leisure */
  // particle_indices = d_hit_indices;
  // kernel_integrals = d_kernel_integrals;
  return EXIT_SUCCESS;
}

int columnDensities(/* Input, not modified. Supplied by host code. */
                    const double N_H_per_particle,
                    const double N_He_per_particle,
                    const rt_float UnitLength,
                    /* Input, not modified. Generated by grace functions */
                    float *d_X_HI,
                    float *d_X_HeI,
                    float *d_X_HeII,
                    thrust::device_vector<float> &d_kernel_integrals,
                    thrust::device_vector<unsigned int> &d_hit_indices,
                    thrust::device_vector<int> &d_ray_offsets,
                    /* Output, made available to host code */
                    thrust::device_vector<float> &Ncol_HI,
                    thrust::device_vector<float> &Ncol_HeI,
                    thrust::device_vector<float> &Ncol_HeII) {
  /* NOTE: d_kernel_integrals.size() !=  d_X_HI.size()
   * d_X_HI.size() = Number of Particles
   * d_kernel_integrals.size() is the sum of the particles along
   * each ray + N_rays for the End-Of-Ray marker.
   * I like to call d_kernel_integrals.size() nSourceParticlePairs. */

  /* Calculate cumulative and (implicitly) per-particle column densities
   * for each species.
   */

  // We want cumulative column density *up to* a particle.
  // The ith particle's Ncol is then Ncol[i+1] - Ncol[i].
  // Because of the dummy particles marking a ray's end, this will also work
  // for the final particle intersected by each ray.

  // We do each species one-by-one to save on GPU memory.
  thrust::device_vector<float> d_Ncol(d_kernel_integrals.size());

  /* This is essentially:
   *  cumsum(d_X_H1.*d_kernel_integrals);
   */
  grace::weighted_exclusive_segmented_scan(d_kernel_integrals, d_X_HI, d_hit_indices, d_ray_offsets, d_Ncol);
  /* Convert the unscaled column densities into physical units.
   * N_H(e)_per_particle is too big for a float, but divided by UnitLength^2
   * makes it float-friendly and puts it in the units we want: m^-2.
   * _1 is a placeholder aka lambda expression */
  using namespace thrust::placeholders;
  float Scale = (float)(N_H_per_particle / (double)UnitLength / (double)UnitLength);
  thrust::transform(d_Ncol.begin(), d_Ncol.end(), d_Ncol.begin(), _1 * Scale);
  Ncol_HI = d_Ncol; /* Copy to host array */

  grace::weighted_exclusive_segmented_scan(d_kernel_integrals, d_X_HeI, d_hit_indices, d_ray_offsets, d_Ncol);
  Scale = (float)(N_He_per_particle / (double)UnitLength / (double)UnitLength);
  thrust::transform(d_Ncol.begin(), d_Ncol.end(), d_Ncol.begin(), _1 * Scale);
  Ncol_HeI = d_Ncol; /* Copy to host array */

  grace::weighted_exclusive_segmented_scan(d_kernel_integrals, d_X_HeII, d_hit_indices, d_ray_offsets, d_Ncol);
  thrust::transform(d_Ncol.begin(), d_Ncol.end(), d_Ncol.begin(), _1 * Scale);
  Ncol_HeII = d_Ncol; /* Copy to host array */
  return EXIT_SUCCESS;
}
