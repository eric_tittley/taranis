#include "Taranis.h"
#include "Taranis.cuh"

#include "RT_CUDA.h"

__global__ void
rEAT_kernel(rt_float Temperature,
            RT_ConstantsT const Constants_dev_RT,
            RT_Data const Data_dev_RT) { 

 size_t const nParticles = Data_dev_RT.NumCells;

 size_t const istart = blockIdx.x*blockDim.x+threadIdx.x;
 size_t const span   = gridDim.x * blockDim.x; // nBlocks * nThreadsPerBlock

 for( size_t cellindex=istart; cellindex < nParticles; cellindex+=span ) {
  rt_float const Entropy = RT_CUDA_EntropyFromTemperature(Temperature,
                                                          cellindex,
                                                         &Data_dev_RT,
                                                         &Constants_dev_RT);
  Data_dev_RT.Entropy[cellindex]=Entropy;
 }
 for( size_t cellindex=istart; cellindex < nParticles; cellindex+=span ) {
  Data_dev_RT.T[cellindex]=Temperature;
 }
}

void
resetEntropyAndTemperature(rt_float Temperature,
                           RT_ConstantsT const Constants_dev_RT,
                           RT_Data const Data_dev_RT
		 ) {
 size_t const nParticles = Data_dev_RT.NumCells;

 int gridSize;       // The actual grid size needed, based on input
 		     // size
 int blockSize;      // The launch configurator returned block size
 setGridAndBlockSize(nParticles, (void *)rEAT_kernel,
                     &gridSize, &blockSize);

 rEAT_kernel<<<gridSize, blockSize>>>
            (Temperature,
             Constants_dev_RT,
             Data_dev_RT
            );
 checkCudaKernelLaunch(__FILE__, __LINE__);
}
