#include "Manidoowag.h"
#include "RT.h"
#include "RT_CUDA.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"
#include "Taranis.cuh"
#include "Taranis.h"
#include "ERT_gizmo.h"
#include "allvars.h"
#include "cudasupport.h"
#include "taranis_vars.h"

/*
 * Here we will perfom everything that needs to be done once.
 * Load constants file etc...
 */

RT_ConstantsT _Constants;
RT_ConstantsT _Constants_dev;

ParametersT _Parameters;

rt_float *_source_table;

size_t _num_sources;
size_t _num_sources_max;

#ifdef HEALPIX_RAYS
thrust::device_vector<float> _ray_directions;
#endif

RT_SourceT *_Source;
float _current_time;

void ert_startup_routine() {
  A_LOG("Entering ert_startup_routine [Taranis]...\n");
  cudaSetDevice(0);
  /* Initialize run parameters. */
  A_LOG("Reading Parameter File...");
  char const *const ParametersFile = "Taranis.params";
  int ierr = readParameters(ParametersFile, &_Parameters);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Parameters file.";
    std::cout << std::endl;
    exit(-1);
  }
  A_LOG("done.\n");

  // Load Constants File
  A_LOG("Reading Constants file...");
  char const *const ConstantsFile = "Constants.params";
  ierr = RT_ReadConstantsFile(ConstantsFile, &_Constants);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Constants file.";
    std::cout << std::endl;
    exit(-1);
  }
  A_LOG("done.\n");

  switch (All.ComovingIntegrationOn) {
    case 0:
      _current_time = All.Time * UNIT_TIME_IN_CGS;  // current gizmo time in seconds
      break;
    case 1:
      _current_time = cosmological_time(All.Time, _Constants);
      break;
    default:
      std::cerr << __FILE__ ": " << __LINE__ << ": "
                << "ERROR: unknown value of ComovingIntegrationOn:" << All.ComovingIntegrationOn << std::endl;
      return;
  }
  A_LOG("Starting simulation at t=%e (Ma)\n", _current_time / _Constants.Ma);

  /* Switch to and report on GPU(s) being used. */
  int nDev = 0;
  cudaGetDeviceCount(&nDev);
  while (_num_sources / nDev < MIN_SOURCES_PER_GPU && nDev > 1) {
    --nDev;
  }
  if (nDev < 1) {
    std::cerr << __FILE__ ": " << __LINE__ << ": "
              << "ERROR: nDev=" << nDev << std::endl
              << "       Check environment variable CUDA_VISIBLE_DEVICES is set correctly." << std::endl
              << "_num_sources=" << _num_sources << "; MIN_SOURCES_PER_GPU=" << MIN_SOURCES_PER_GPU << std::endl;
    return;
  }
  std::cout << "Using " << nDev << " device(s)" << std::endl;
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(i);
    std::cout << "Properties of CUDA device " << i << std::endl;
    RT_CUDA_Util_PrintDeviceProperties(i);
  }

  /* increase stack size */
  size_t pValue;
  bool const TerminateOnCudaError = true;
  cudaError_t cudaError = cudaDeviceGetLimit(&pValue, cudaLimitStackSize);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  A_LOG("Current LimitStackSize=%lu. Setting to 2048 bytes\n", pValue);
  cudaError = cudaDeviceSetLimit(cudaLimitStackSize, 2048);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);

  /* Copy constants */
  RT_CUDA_CopyConstantsToDevice(_Constants, &_Constants_dev);
  MyFloat max_HII = -1.0; // MyFloat defince in gizmo's allvars.h
  /* Set initial fractions */
  if (RestartFlag == 0) {  // Not restarting
                           //#pragma omp parallel for shared(none)
    for (size_t i = 0; i < N_gas; i++) {
      SphP[i].HI = _Parameters.InitialFractions[0];
      SphP[i].HII = _Parameters.InitialFractions[1];
      SphP[i].HeI = _Parameters.InitialFractions[2];
      SphP[i].HeII = _Parameters.InitialFractions[3];
      SphP[i].HeIII = _Parameters.InitialFractions[4];
    }
  } else {
    //#pragma omp parallel for shared(none)
    for (size_t i = 0; i < N_gas; ++i) {
      if (P[i].Type != 0) continue;

      SphP[i].HII = 1 - SphP[i].HI;
      SphP[i].HeIII = 1. - SphP[i].HeI - SphP[i].HeII;
      if (SphP[i].HII > max_HII) {
        max_HII = SphP[i].HII;
      }
    }
  }
  printf("Max HII found = %5.3e\n", max_HII);

/* Generate Healpix Directions */
#ifdef HEALPIX_RAYS
  A_LOG("Generating Healpix Rays...");
  thrust::device_vector<float> ray_directions(3 * _Parameters.N_rays);
  ierr = generateHealpixRayDirections(_Parameters.N_rays, ray_directions);
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: Failed to generate Healpix Ray Directions.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    exit(-1);
  }
  A_LOG("done.\n");
  _ray_directions = ray_directions;
#endif  // HEALPIX_RAYS

  A_LOG("Setting up sources...\n");
  /* Load source file */
#if defined(FOF_SOURCES) || defined(STELLAR_SOURCES)
  _num_sources_max = _Parameters.nSources;
  char fname[] = "Geneva_0.002_burst_1e6msol";
  size_t count = 7 * 100;
  FILE *fp = fopen(fname, "rb");
  if (!fp) {
    perror("Taranis: could not open starburst file");
    gizmo_terminate("Failed to open starburst file");
  }
  _source_table = new float[count];
  size_t nread = fread(_source_table, sizeof(float), count, fp);
  if (nread != count) {
    if (feof(fp)) {
      printf(
          "Taranis: starburst file invalid, expected %zu floats, hit EOF "
          "after %zu",
          count,
          nread);
    } else {
      perror("Taranis: error reading starburst file");
    }
    fclose(fp);
    gizmo_terminate("Failed to read starburst file");
  }
  fclose(fp);
#endif

  _Source = (RT_SourceT *)malloc(_Parameters.nSources * sizeof(RT_SourceT));

#if !defined(FOF_SOURCES) && !defined(STELLAR_SOURCES) && !defined(MASON_2015_SOURCES)
  Taranis_SetSources(_Parameters, All.BoxSize, _Constants, _Source, UNIT_LENGTH_IN_CGS / 100);

  /* Initialize the source functions */
  _num_sources = _Parameters.nSources;
  for (size_t i = 0; i < _Parameters.nSources; ++i) {
    RT_SetSourceFunction(_Source + i); /* Pointer Arithmetic */
  }
  RT_InitialiseSpectrum(0, _Constants, &(_Source[0]));
#endif
  A_LOG("done\n");
  A_LOG("Leaving ert_startup_routine [Taranis].\n");
}
