#include <cuda_runtime.h>
#include <thrust/device_vector.h>

#include "Taranis.h"
#include "Taranis.cuh"

#ifdef RT_DOUBLE
/* Overload atomicAdd */
__device__ double atomicAdd(double* address, float const fval)
{
    unsigned long long int* address_as_ull =
                              (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    double val = (double)fval;

    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                               __longlong_as_double(assumed)));

    // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
    } while (assumed != old);

    return __longlong_as_double(old);
}
#endif

__global__ void
aSCTR_kernel(size_t const nSourceParticlePairs,
             size_t const nParticles,
             float const * const I_H1_SPP_dev_RT,
             float const * const I_He1_SPP_dev_RT,
             float const * const I_He2_SPP_dev_RT,
             float const * const G_H1_SPP_dev_RT,
             float const * const G_He1_SPP_dev_RT,
             float const * const G_He2_SPP_dev_RT,
             float const * const Gamma_HI_SPP_dev_RT,
             unsigned int const * const gadget_map_dev_RT,
             unsigned int const * const particle_indices_dev_RT,
             /* updated */
             RT_RatesBlockT Rates_dev_RT ) {

 size_t const istart = blockIdx.x*blockDim.x+threadIdx.x;
 size_t const span   = gridDim.x * blockDim.x; // nBlocks * nThreadsPerBlock

 for( size_t iSPP = istart; iSPP < nSourceParticlePairs; iSPP+=span ) {
  /* WARNING: There is a race here, as more than one iSPP can point to iHost.
   * Hence the atomicAdd() instead of a += */
  size_t const particleIndex = particle_indices_dev_RT[iSPP];
  /* particleIndex == nParticles is an End-Of-Ray marker */
  if( particleIndex < nParticles ) {
   size_t const iHost = gadget_map_dev_RT[particleIndex];
   atomicAdd(&(Rates_dev_RT.I_H1[iHost] ), I_H1_SPP_dev_RT[iSPP] );
   atomicAdd(&(Rates_dev_RT.I_He1[iHost]), I_He1_SPP_dev_RT[iSPP]);
   atomicAdd(&(Rates_dev_RT.I_He2[iHost]), I_He2_SPP_dev_RT[iSPP]);
   atomicAdd(&(Rates_dev_RT.G_H1[iHost] ), G_H1_SPP_dev_RT[iSPP] );
   atomicAdd(&(Rates_dev_RT.G_He1[iHost]), G_He1_SPP_dev_RT[iSPP]);
   atomicAdd(&(Rates_dev_RT.G_He2[iHost]), G_He2_SPP_dev_RT[iSPP]);
   atomicAdd(&(Rates_dev_RT.Gamma_HI[iHost]), Gamma_HI_SPP_dev_RT[iSPP]);
  }
 }
}

void
accumulateSourceContributionsToRates(thrust::device_vector<float> &I_H1_SPP_dev_RT,
                                     thrust::device_vector<float> &I_He1_SPP_dev_RT,
                                     thrust::device_vector<float> &I_He2_SPP_dev_RT,
                                     thrust::device_vector<float> &G_H1_SPP_dev_RT,
                                     thrust::device_vector<float> &G_He1_SPP_dev_RT,
                                     thrust::device_vector<float> &G_He2_SPP_dev_RT,
                                     thrust::device_vector<float> &Gamma_HI_SPP_dev_RT,
                                     thrust::device_vector<unsigned int> &gadget_map_dev_RT,
                                     thrust::device_vector<unsigned int> &particle_indices_dev_RT,
                                     /* updated */
                                     RT_RatesBlockT Rates_dev_RT ) {
 size_t const nSourceParticlePairs = particle_indices_dev_RT.size();
 size_t const nParticles           = gadget_map_dev_RT.size();

 int gridSize;       // The actual grid size needed, based on input
                     // size
 int blockSize;      // The launch configurator returned block size
 setGridAndBlockSize(nSourceParticlePairs, (void *)aSCTR_kernel,
                     &gridSize, &blockSize);

 aSCTR_kernel<<<gridSize, blockSize>>>
             (nSourceParticlePairs,
              nParticles,
              I_H1_SPP_dev_RT.data().get(),
              I_He1_SPP_dev_RT.data().get(),
              I_He2_SPP_dev_RT.data().get(),
              G_H1_SPP_dev_RT.data().get(),
              G_He1_SPP_dev_RT.data().get(),
              G_He2_SPP_dev_RT.data().get(),
              Gamma_HI_SPP_dev_RT.data().get(),
              gadget_map_dev_RT.data().get(),
              particle_indices_dev_RT.data().get(),
              /* updated */
              Rates_dev_RT );
 checkCudaKernelLaunch(__FILE__, __LINE__);
}
