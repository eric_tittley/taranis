#include <allvars.h>

#include "config.h"

#ifdef FOF_SOURCES
extern "C" {
#  include <proto.h>
#  include <structure/fof.h>

#  include "RT_Luminosity.h"
}

#  include <algorithm>
#  include <cstdio>
#  include <numeric>
#  include <vector>

#  include "ERT_gizmo.h"
#  include "taranis_vars.h"

constexpr int SOURCE_AGE_BINS = 30;
constexpr double STELLAR_AGE_CUTOFF_MA = 30.;
constexpr double ESCAPE_FRACTION = 0.01;
constexpr double BIN_WIDTH_MA = STELLAR_AGE_CUTOFF_MA / SOURCE_AGE_BINS;

// table - list of (currently) 700 floats. age alpha L0 alpha L0 alpha L0
/* table is in column order */

struct starGroup {
  rt_float CM[3];
  rt_float Vel[3];
  rt_float stellarMassInGroup[SOURCE_AGE_BINS];
  rt_float t0;  // time of the last fof
  size_t nStars;
};
std::vector<starGroup> starGroupList;
/*
static float time_diff(integertime time0, integertime time1) {
  if (time1 < time0) {
    printf(
        "ERROR: predicting source movement back in time. time0=%lld, time1=%lld\n", (long long)time0, (long long)time1);
    return 0.f;
  }

  if (time1 == time0) {
    return 0.f;
  }

  if (All.ComovingIntegrationOn) {
    return get_drift_factor(time0, time1);
  }

  return (time1 - time0) * All.Timebase_interval;
}

void updateSourcePositions() {
  for (size_t i = 0; i < _num_sources; ++i) {
    const float dt = time_diff(starGroupList[i].t0, All.time);
    // need to add cosmology
    _Source[i].r[0] += starGroupList.Vel[0] * dt;
    _Source[i].r[1] += starGroupList.Vel[1] * dt;
    _Source[i].r[2] += starGroupList.Vel[2] * dt;
  }
}
*/
static float get_table_element(int table_idx, int k) { return _source_table[k * 100 + table_idx]; }

static float get_table_alpha(int table_idx, int frequency_bin) {
  return get_table_element(table_idx, 1 + 2 * frequency_bin);
}

static float get_table_L_0(int table_idx, int frequency_bin) {
  return get_table_element(table_idx, 2 + 2 * frequency_bin);
}

static rt_float RT_Luminosity_starburst(RT_LUMINOSITY_ARGS) {
  int frequency_bin;
  if (nu < Constants->nu_1) {
    frequency_bin = 0;
  } else if (nu < Constants->nu_2) {
    frequency_bin = 1;
  } else {
    frequency_bin = 2;
  }

  const rt_float mass_unit_1e6msol = (All.UnitMass_in_g * 1e-6) / (All.HubbleParam * SOLAR_MASS);

  rt_float L_nu = 0;
  for (int i = 0; i < SOURCE_AGE_BINS; ++i) {
    rt_float mass = Source->source_table[i] * mass_unit_1e6msol;
    rt_float alpha = get_table_alpha(i * 2, frequency_bin);
    rt_float L_0 = get_table_L_0(i * 2, frequency_bin);
    if (L_0 <= 0) {
      continue;
    }
    rt_float lum = L_0 * std::pow(nu / Constants->nu_0, alpha);
    L_nu += lum * (mass * ESCAPE_FRACTION);
  }
  return L_nu;
}

void ert_set_sources_from_fof(fof_particle_list *particleList) {
  // Ngroups and Group are available from allvars.h
  // sort by stellar mass
  printf("Setting sources [FOF_SOURCES]...\n");
  starGroupList.clear();
  starGroupList.resize(Ngroups);
  size_t start = 0;
  for (size_t i = 0; i < Ngroups; i++) {
    const group_properties &group = Group[i];
    for (size_t j = 0; j < 3; j++) starGroupList[i].CM[j] = group.CM[j];
    for (size_t j = 0; j < 3; j++) starGroupList[i].Vel[j] = group.Vel[j];
    starGroupList[i].t0 = All.Time;
    size_t len = 0;
    while (particleList[start].MinID < group.MinID) start++;
    for (len = 0; start + len < NumPart;)
      if (particleList[start + len].MinID == group.MinID)
        len++;
      else
        break;
    for (size_t k = 0; k < len; k++) {
      int p_index = particleList[start + k].Pindex;
      if (P[p_index].Type != 4) continue;  // only consider stars
      double star_age = evaluate_stellar_age_Gyr(P[p_index].StellarAge) * 1000;
      if (star_age > STELLAR_AGE_CUTOFF_MA) continue;  // old stars don't contribute
      int index = round(star_age / BIN_WIDTH_MA);
      starGroupList[i].stellarMassInGroup[index] += P[p_index].Mass;
      starGroupList[i].nStars++;
    }
  }

  // remove groups with no young stars
  starGroupList.erase(
      std::remove_if(starGroupList.begin(), starGroupList.end(), [](const starGroup &a) { return a.nStars <= 0; }),
      starGroupList.end());

  int num_sources = std::min((int)starGroupList.size(), (int)_num_sources_max);

  std::nth_element(starGroupList.begin(),
                   starGroupList.begin() + num_sources - 1,
                   starGroupList.end(),
                   [](const starGroup &a, const starGroup &b) { return a.nStars > b.nStars; });

  starGroupList.resize(num_sources);
  printf("%d sources \n", num_sources);
  _num_sources = num_sources;
  // Set source positions
  for (size_t i = 0; i < num_sources; ++i) {
    for (size_t j = 0; j < 3; j++) _Source[i].r[j] = starGroupList[i].CM[j];
    _Source[i].AttenuationGeometry = RaySphericalWave;
    _Source[i].SourceType = SourceUser;
    _Source[i].LuminosityFunction = (SourceFunctionT)&RT_Luminosity_starburst;
    _Source[i].source_table = &starGroupList[i].stellarMassInGroup[0];
    float totalMass = 0.;
    for (size_t i = 0; i < SOURCE_AGE_BINS; i++) totalMass += starGroupList[i].stellarMassInGroup[i];
    printf("Source at %e, %e, %e with total mass %e",
           starGroupList[i].CM[0],
           starGroupList[i].CM[1],
           starGroupList[i].CM[2],
           totalMass);
  }
}

#endif  // FOF_SOURCES