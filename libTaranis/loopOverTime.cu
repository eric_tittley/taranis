/*
 * Loops over the simulation time and updates Data_dev
 *
 *
 *
 */
#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <thrust/sort.h>
#include <thrust/system_error.h>

#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "Taranis.cuh"
#include "Taranis.h"
#include "cudasupport.h"
#include "grace/ray.h"
#include "nccl.h"
#include "omp.h"

#define gpuErrchk(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true) {
  if (code != cudaSuccess) {
    fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}

#define NCCLCHECK(cmd)                                                                      \
  do {                                                                                      \
    ncclResult_t r = cmd;                                                                   \
    if (r != ncclSuccess) {                                                                 \
      printf("Failed, NCCL error %s:%d '%s'\n", __FILE__, __LINE__, ncclGetErrorString(r)); \
      exit(EXIT_FAILURE);                                                                   \
    }                                                                                       \
  } while (0)

int loopOverTime(RT_SourceT *const source,
                 size_t const nSource,
                 rt_float const t_start,
                 rt_float const t_stop,
                 rt_float const unitLength,
                 rt_float const redshift,
                 rt_float const boxSize,
                 float rayLength,
                 size_t const N_rays,
                 double const N_H_per_particle,
                 double const N_He_per_particle,
                 RT_ConstantsT Constants,
                 int const nDev,
                 const int *devs,
                 size_t const nParticles,
                 lists_t *lists,
/* Optional, not modified */
#ifdef DUMP_RT_DATA
                 rt_float const *const OutputTimes,
                 rt_float const outputTimesUnit,
                 size_t const nOutputTimes,
                 size_t OutputTimeIndex,
                 size_t const DumpIterationCount,
                 char *Dir,
#endif
                 rt_float InitialTemperature // Only used for monochromatic radiation (i.e. Test 1)
) {

  double CodeTimeArray[nCodeTimes];
  double const StartTime = getTimeInSeconds();
  float CheckPointTime;
  double TotalTime = 0.0;
  size_t IterationCount = 0;

  for (size_t i = 0; i < nCodeTimes; ++i) {
    CodeTimeArray[i] = 0.0;
  }

  rt_time_t time;
  time.current_time = t_start;
  time.t_end = t_stop;
  time.t_nextIonizationRate = t_start;
  time.updateIonizationTimescale = true;
  time.LastIterationFlag = false;
  time.reportStatistics = false;
  time.OutputFlag = false;
  time.OutputTimeIndex = 0;
  time.iterSinceIonizationRateUpdate = 0;

#ifdef DUMP_RT_DATA
  time.nextOutputTime = OutputTimes[0] * outputTimesUnit;
#else
  time.nextOutputTime = 1e20;
#endif

  /* Initialize NCCL communicator and individual device streams */
  ncclComm_t comms[nDev];
  cudaStream_t *s = (cudaStream_t *)malloc(sizeof(cudaStream_t) * nDev);

    if(nDev>1) {
      for (int i = 0; i < nDev; i++) {
      cudaSetDevice(devs[i]);
      cudaStreamCreate(s + i);
    }

    // initializing NCCL
    printf("ncclCommInitAll\n");
    NCCLCHECK(ncclCommInitAll(comms, nDev, devs));

    // synchronizing on CUDA streams to wait for completion of NCCL operation
    for (int i = 0; i < nDev; ++i) {
      cudaSetDevice(devs[i]);
      cudaStreamSynchronize(s[i]);
    }
  }

  /* Cuda events to time kernel calls */
  cudaSetDevice(devs[0]);
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  rt_float const ExpansionFactor = 1. / (redshift + 1.);

  int ierr;

  /* Initialize involatile (mostly) variables of the photoionization integral */
  V_LOG("Allocating involatiles...");
  lists->involatiles.resize(nDev);
  for (int i = 0; i < nDev; i++) {
    if (source[0].SourceType != SourceMonochromatic) {
      ierr = RT_IntegralInvolatiles_allocate(Constants, &lists->involatiles[i]);
      assert(ierr == EXIT_SUCCESS);

      ierr = RT_IntegralInvolatiles_setFrequenciesWeightsAlphas(Constants, lists->involatiles[i]);
      assert(ierr == EXIT_SUCCESS);

    } else { /* Monochromatic (Test 1) */
      lists->involatiles[i].nFrequencies = 1;
      lists->involatiles[i].nSpecies = 3;
      lists->involatiles[i].Frequencies = (rt_float *)malloc(sizeof(rt_float));
      lists->involatiles[i].Frequencies[0] = source[0].nu;
      lists->involatiles[i].Weights = (rt_float *)malloc(sizeof(rt_float));
      lists->involatiles[i].Weights[0] = 1.0;
      lists->involatiles[i].Alphas = (rt_float *)malloc(3 * sizeof(rt_float));
      lists->involatiles[i].Alphas[0] = H1_H2(source[0].nu, Constants);
      lists->involatiles[i].Alphas[1] = He1_He2(source[0].nu, Constants);
      lists->involatiles[i].Alphas[2] = He2_He3(source[0].nu, Constants);
      lists->involatiles[i].Luminosities = (rt_float *)malloc(sizeof(rt_float));
    }
  }
  /* Luminosity has yet to be set */
  /* Allocate space on device */
  lists->d_involatiles.resize(nDev);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    ierr = RT_CUDA_allocateIntegralInvolatilesOnDevice(lists->involatiles[i], &lists->d_involatiles[i]);
    assert(ierr == EXIT_SUCCESS);
  }
  V_LOG("done\n");

  /* Main loop */
  /* Use threads, one for each CUDA device, to loop over separate spans of sources */
  /* The time_t struct is global but only master should update values in the struct.
     Other threads should treat it as read only. */
#pragma omp parallel num_threads(nDev) default(shared)
  {
#pragma omp barrier
    int tid = omp_get_thread_num();
    cudaSetDevice(devs[tid]);
    while (!time.LastIterationFlag) {
      /* Check if we need to update the Ionization Rates */
      if (time.updateIonizationTimescale) {
        time.reportStatistics = true;  // only report every RT timestep
        // Broadcast RT_Data from root to all ranks
        if (nDev > 1) {
          NCCLCHECK(ncclBcast((void *)lists->data[tid]->MemSpace,
                              nParticles * 12,
                              ncclFloat,
                              0,  // root
                              comms[tid],
                              s[tid]));
        }
        bool sync = true;
        /* Initialize Rates */
        if (tid == 0) {
          V_LOG("Zeroing Rates...");
          cudaEventRecord(start);
        }
        zeroRates(lists->rates[tid]);
        if (tid == 0) {
          cudaEventRecord(stop);
          cudaEventSynchronize(stop);
          cudaEventElapsedTime(&CheckPointTime, start, stop);
          CodeTimeArray[ZeroingRates] += CheckPointTime;
          V_LOG(" done\n");
        }
#pragma omp barrier
        const int max_sources_at_once = MAX_SOURCES_PER_GPU;

        int n_sources_local = (nSource + nDev - 1) / nDev;  // round up
        int num_sources_at_once, source_start_id, num_traces;
        if (n_sources_local != 0) {
          num_sources_at_once = std::min(n_sources_local, max_sources_at_once);
          source_start_id = n_sources_local * tid;
          if (tid + 1 == nDev) n_sources_local -= nSource % nDev;
          num_traces = (n_sources_local + num_sources_at_once - 1) / num_sources_at_once;  // round up
        } else {
          num_sources_at_once = 0;
          source_start_id = 0;
          num_traces = 0;
        }
        // last device does nSource%nDev sources less

        for (int i_source = source_start_id; i_source < source_start_id + n_sources_local;
             i_source += num_sources_at_once) {
          V_LOG("Creating Rays...");
          if (tid == 0) cudaEventRecord(start);
          // Create rays from the set of sources i_source_start to i_source_end
          int i_source_start = i_source;
          int i_source_end = i_source_start + num_sources_at_once;
          if (i_source_end > nSource) i_source_end = nSource;  // hit the end of the source list
          if (i_source_end > source_start_id + n_sources_local) i_source_end = source_start_id + n_sources_local;
          int num_sources_this_step = i_source_end - i_source_start;
          int n_rays_this_step = num_sources_this_step * N_rays;
          V_LOG("Thread %d on dev %d tracing sources %d to %d\n", tid, devs[tid], i_source_start, i_source_end);

          thrust::device_vector<grace::Ray> d_rays(n_rays_this_step);
#ifdef HEALPIX_RAYS
          generateHealpixRaysFromMultipleSources(d_rays,
                                                 N_rays,  // rays per source
                                                 num_sources_this_step,
                                                 i_source_start,
                                                 source,                 // source list
                                                 rayLength,              // TODO: make this a vector
                                                 *lists->ray_dir[tid]);  // healpix directions
#else
          if (source[i_source_start].AttenuationGeometry == RaySphericalWave) {
            ierr = generateRandomRays(d_rays, source[i_source_start], rayLength);
          } else if (source[i_source_start].AttenuationGeometry == RayPlaneWave) {
            ierr = generatePlaneParallelRays(d_rays, source[i_source_start], rayLength);
          }
#endif
          assert(ierr == EXIT_SUCCESS);
          if (tid == 0) {
            cudaEventRecord(stop);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&CheckPointTime, start, stop);
            CodeTimeArray[RayCreation] += CheckPointTime;
          }
          V_LOG("done\n");

          V_LOG("Tracing Rays...");
          if (tid == 0) cudaEventRecord(start);
          thrust::device_vector<float> d_kernel_integrals;
          thrust::device_vector<unsigned int> d_hit_indices;
          thrust::device_vector<int> d_ray_offsets(n_rays_this_step);
          thrust::host_vector<unsigned int> particle_indices;
          thrust::host_vector<float> kernel_integrals;
          thrust::host_vector<int> ray_offsets;

          ierr = trace_rays(/* Input, not modified */
                            *lists->particles[tid],
                            *lists->tree[tid],
                            d_rays,
                            /* Output, used by grace */
                            d_kernel_integrals,
                            d_hit_indices,
                            d_ray_offsets,
                            /* Output, made available to host code */
                            particle_indices,   // unused - edit columnDensities.cu
                            kernel_integrals);  // if you need to use them
          assert(ierr == EXIT_SUCCESS);
          if (tid == 0) {
            cudaEventRecord(stop);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&CheckPointTime, start, stop);
            CodeTimeArray[RayTracing] += CheckPointTime;
          }
          ray_offsets = d_ray_offsets;
          V_LOG(" done\n");

          /* particle_indices is ...
           *  Ncol_HeII[i] is the cumulative column density to the particle
           *  with index particle_indices[i] into the sorted list of particles.
           *  The index into the Gadget list is gadget_map[particle_indices[i]]
           *  So,
           *   particle at r[1,gadget_map[particle_indices[i]]], r[2,...], r[3,...]
           *   has total cumulative column density Ncol_HeII[i], but noting that
           * there will be one or more total cumulative column densities to each
           * particle within Ncol_HeII Recall that particle_indices[i] == nParticles
           * (length of Gadget list), indicates the end of a sight line.
           */

          /* Wait for the RT_Data bcast to complete */
          if (nDev > 1) {
            if (sync) {
              double t_start = omp_get_wtime();
              cudaStreamSynchronize(s[tid]);
              sync = false;
              double t_end = omp_get_wtime();
              if (tid == 0) CodeTimeArray[RTDataBcast] += t_end - t_start;
            }
          }
          //    cudaDeviceSynchronize();  // shouldn't be needed but testing to see if it fixed a rare crash
          /* Find column densities to each particle from this source */
          V_LOG("Assigning column densities...");
          if (tid == 0) cudaEventRecord(start);
          /*  The column densities to be filled in */
          thrust::device_vector<float> column_H1_SPP_dev(d_kernel_integrals.size());
          thrust::device_vector<float> column_He1_SPP_dev(d_kernel_integrals.size());
          thrust::device_vector<float> column_He2_SPP_dev(d_kernel_integrals.size());
          V_LOG("Going into ColumDensities.cu\n");
          ierr = columnDensities(/* Input, not modified. Supplied by host code. */
                                 N_H_per_particle,
                                 N_He_per_particle,
                                 unitLength,
                                 /* Input, not modified. */
                                 (*lists->data[tid]).f_H1,   // X_HI_dev,
                                 (*lists->data[tid]).f_He1,  // X_HeI_dev,
                                 (*lists->data[tid]).f_He2,  // X_HeII_dev,
                                 d_kernel_integrals,
                                 d_hit_indices,
                                 d_ray_offsets,
                                 /* Output, made available to host code */
                                 column_H1_SPP_dev,
                                 column_He1_SPP_dev,
                                 column_He2_SPP_dev);
          V_LOG("Returned from ColumnDensities.cu\n");
          assert(ierr == EXIT_SUCCESS);
          if (tid == 0) {
            cudaEventRecord(stop);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&CheckPointTime, start, stop);
            CodeTimeArray[ColumnDensities] += CheckPointTime;
          }
          cudaDeviceSynchronize();
          setMeanColumnDensity(
              column_H1_SPP_dev, column_He1_SPP_dev, column_He2_SPP_dev, d_hit_indices, (*lists->data[tid]));
          cudaDeviceSynchronize();
          V_LOG(" done\n");
          V_LOG("Loop over %d sources...\n", num_sources_this_step);
          for (size_t iSource = i_source_start; iSource < i_source_end; ++iSource) {
            /* Loop over each source */
            V_LOG("Setting Involatiles Luminosity..");
            if (tid == 0) cudaEventRecord(start);
            ierr = RT_IntegralInvolatiles_setLuminosity(source[iSource],
                                                        Constants,
                                                        redshift, /* SourceRedshift */
                                                        lists->involatiles[tid]);
            assert(ierr == EXIT_SUCCESS);
            ierr = RT_CUDA_copyIntegralInvolatilesToDevice(lists->involatiles[tid], lists->d_involatiles[tid]);
            assert(ierr == EXIT_SUCCESS);

            if (tid == 0) {
              cudaEventRecord(stop);
              cudaEventSynchronize(stop);
              cudaEventElapsedTime(&CheckPointTime, start, stop);
              CodeTimeArray[InvolatilesLuminosity] += CheckPointTime;
            }

            V_LOG("done\n");
            // Set the distance from the source to each particle
            V_LOG("Calculating particle-source distances...");

            if (tid == 0) cudaEventRecord(start);
            setSourceToParticleDistance(
                *lists->particles[tid], source[iSource], boxSize, unitLength, (*lists->data[tid]));
            if (tid == 0) {
              cudaEventRecord(stop);
              cudaEventSynchronize(stop);
              cudaEventElapsedTime(&CheckPointTime, start, stop);
              CodeTimeArray[Distances] += CheckPointTime;
            }
            V_LOG(" done\n");

            /* Calculate the source-Particle pair rates. */
            if (tid == 0) cudaEventRecord(start);
            int ray_start = (iSource - i_source_start) * N_rays;  // id of first ray to this source
            int ray_end = ray_start + N_rays;                     // id of last ray to this source
            int start_offset = ray_offsets[ray_start];
            int end_offset = ray_offsets[ray_end] - 1;  // need to copy up to but not including the next set of rays
            if (ray_end == n_rays_this_step) end_offset = column_H1_SPP_dev.size() - 1;
            const size_t nSourceParticlePairs = end_offset - start_offset;

            /*
             * Create Iterators that point to the start of the current source's column densities and associated particle
             * indices.
             */
            thrust::device_vector<float>::iterator column_H1_SPP_dev_local = column_H1_SPP_dev.begin() + start_offset;
            thrust::device_vector<float>::iterator column_He1_SPP_dev_local = column_He1_SPP_dev.begin() + start_offset;
            thrust::device_vector<float>::iterator column_He2_SPP_dev_local = column_He2_SPP_dev.begin() + start_offset;
            thrust::device_vector<unsigned int>::iterator particle_indices_local = d_hit_indices.begin() + start_offset;

            V_LOG("Calculating %d source-Particle pair rates ...", end_offset - start_offset);
            calculateSppRates((*lists->data[tid]),
                              nSourceParticlePairs,
                              column_H1_SPP_dev_local,
                              column_He1_SPP_dev_local,
                              column_He2_SPP_dev_local,
                              /* This should be the source redshift as seen from the cell/particle.
                               * Hence it will be different for each cell/particle.  This should be
                               * a vector with an element for each source-Particle Pair. */
                              redshift,
                              source[iSource],
                              Constants,
                              particle_indices_local,
                              lists->d_involatiles[tid],
                              /* out */
                              lists->rates[tid]);

            if (tid == 0) {
              cudaEventRecord(stop);
              cudaEventSynchronize(stop);
              cudaEventElapsedTime(&CheckPointTime, start, stop);
              CodeTimeArray[SPPRates] += CheckPointTime;
            }
            V_LOG(" done\n");

            /* Accumulate for diagnosis the mean column densities to each particle.
             * For multi-source systems, the column densities will be from the last
             * source.
             */
            // setMeanColumnDensity(column_H1_SPP_dev, column_He1_SPP_dev, column_He2_SPP_dev, d_hit_indices, Data_dev);

          } /* End of loop over all sources */
          V_LOG("...done loop over sources\n");
          // sum components into G
          V_LOG("Updating heating rates ...");
          if (tid == 0) cudaEventRecord(start);
          updateCumulativeHeatingRate(lists->rates[tid]);
          if (tid == 0) {
            cudaEventRecord(stop);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&CheckPointTime, start, stop);
            CodeTimeArray[UpdateHeatingRates] += CheckPointTime;
          }
          cudaDeviceSynchronize();

          V_LOG(" done\n");
        }

        double t_start = omp_get_wtime();
        if (nDev > 1) {
          NCCLCHECK(ncclReduce((const void *)lists->rates[tid].I_H1,
                               (void *)lists->rates[tid].I_H1,
                               nParticles,
                               ncclFloat,
                               ncclSum,
                               0,  // root
                               comms[tid],
                               s[tid]));
          NCCLCHECK(ncclReduce((const void *)lists->rates[tid].I_He1,
                               (void *)lists->rates[tid].I_He1,
                               nParticles,
                               ncclFloat,
                               ncclSum,
                               0,  // root
                               comms[tid],
                               s[tid]));
          NCCLCHECK(ncclReduce((const void *)lists->rates[tid].I_He2,
                               (void *)lists->rates[tid].I_He2,
                               nParticles,
                               ncclFloat,
                               ncclSum,
                               0,  // root
                               comms[tid],
                               s[tid]));
          NCCLCHECK(ncclReduce((const void *)lists->rates[tid].Gamma_HI,
                               (void *)lists->rates[tid].Gamma_HI,
                               nParticles,
                               ncclFloat,
                               ncclSum,
                               0,  // root
                               comms[tid],
                               s[tid]));
          NCCLCHECK(ncclReduce((const void *)lists->rates[tid].G,
                               (void *)lists->rates[tid].G,
                               nParticles,
                               ncclFloat,
                               ncclSum,
                               0,  // root
                               comms[tid],
                               s[tid]));
        }

        if(nDev > 1) {
          // synchronizing on CUDA streams to wait for completion of NCCL operation
          cudaStreamSynchronize(s[tid]);
        }

        double t_end = omp_get_wtime();
#pragma omp master
        { CodeTimeArray[RTRatesReduction] += t_end - t_start; }
      }  // if(recalculate_ionization_rates)
#pragma omp master
      {
        /* If we don't update the ioniztion rates we need to zero the cooling rates here */
        if (!time.updateIonizationTimescale) {
          cudaEventRecord(start);
          zeroCoolingRates(lists->rates[0]);
          cudaDeviceSynchronize();
          cudaEventRecord(stop);
          cudaEventSynchronize(stop);
          cudaEventElapsedTime(&CheckPointTime, start, stop);
          CodeTimeArray[ZeroingRates] += CheckPointTime;
          V_LOG(" done\n");
        }

        V_LOG("Recombination rates ...");
        cudaEventRecord(start);
        calculateRecombinationRatesInclCooling(ExpansionFactor, Constants, *lists->data[0], lists->rates[0]);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[RecombinationRates] += CheckPointTime;

        V_LOG(" done\n");

#ifndef NO_COOLING
        /* Cooling rates */
        V_LOG("Cooling rates ...");
        cudaEventRecord(start);
        RT_CUDA_CoolingRates(ExpansionFactor, Constants, *lists->data[0], lists->rates[0]);
        /* RT_CUDA does not currently perform error checking */
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[CoolingRates] += CheckPointTime;
        V_LOG(" done\n");
#endif

#ifdef COLLISIONAL_IONIZATION
        if (time.updateIonizationTimescale) {
          collisionalIonizationRate(Constants, *lists->data[0], lists->rates[0]);
        }
#endif

        /* Calculate hydrogen equilibrium values */
        V_LOG("Setting equilibrium fractions ...");
        cudaEventRecord(start);
        RT_CUDA_setHydrogenEquilibriumValues(Constants, *lists->data[0], lists->rates[0]);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[SetHydrogenEquilibriumValues] += CheckPointTime;
        V_LOG(" done\n");
        /* Find the largest timestep that allows us to safely integrate through dt
         */
        V_LOG("Finding the timestep...");
        cudaEventRecord(start);
        findNextTimestep_CUDA(nParticles, *lists->data[0], lists->rates[0], Constants, &time);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[FindingTimestep] += CheckPointTime;

        V_LOG(" done\n");
        /*
        std::cout << "t= " << time.current_time << " (" << time.current_time / Constants.Ma << " Ma)"
                  << " dt_RT= " << time.dt_RT << " (" << time.dt_RT / Constants.Ma << " Ma)" << std::endl;
                  */
#ifdef MONITOR_GPU_MEMORY
        printMemory(CudaDeviceTaranis);
#endif

        // Update the ionization fractions and entropies of the cells/particles
        V_LOG("Updating particles...");
#ifdef DEBUG
        RT_CUDA_RT_Data_CheckData(*lists->data[0], __FILE__, __LINE__);
#endif
        cudaEventRecord(start);
        updateParticles(time.dt_RT,
                        redshift,
                        Constants,
                        lists->rates[0],
                        /* Updated */ *lists->data[0]);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
#ifdef DEBUG
        RT_CUDA_RT_Data_CheckData(*lists->data[0], __FILE__, __LINE__);
#endif
        CodeTimeArray[UpdatingParticles] += CheckPointTime;
        V_LOG(" done\n");

        if (source->SourceType == SourceMonochromatic) {
          /* Test 1 is ISOTHERMAL
           * Reset Entropy and Temperature
           * This is a horrible test making a horrible assumption. */
          resetEntropyAndTemperature(InitialTemperature, Constants, *lists->data[0]);
        }

        time.current_time += time.dt_RT;
#ifdef DUMP_RT_DATA
        if (time.OutputFlag) {
          V_LOG("Outputing RT_Data...");
          cudaEventRecord(start);
          cudaDeviceSynchronize();
          CreateSnapshot(
              *lists->data[0], lists->rates[0], *lists->map[0], redshift, time.current_time, outputTimesUnit, Dir);
          time.OutputFlag = false;
          time.OutputTimeIndex++;
          /* If this was the last output end the run. */
          if (!time.LastIterationFlag) time.nextOutputTime = OutputTimes[time.OutputTimeIndex] * outputTimesUnit;
          cudaDeviceSynchronize();
          cudaEventRecord(stop);
          cudaEventSynchronize(stop);
          cudaEventElapsedTime(&CheckPointTime, start, stop);
          CodeTimeArray[IO] += CheckPointTime;
          V_LOG(" done\n");
        }
#endif

        cudaDeviceSynchronize();
        if (time.reportStatistics) {
          IterationCount++;
          double LastIterationTime = getTimeInSeconds() - StartTime - TotalTime;
          TotalTime = getTimeInSeconds() - StartTime;
          std::cout << "Iteration walltime = " << LastIterationTime << " s" << std::endl;
          std::cout << "Cooling Iterations = " << time.iterSinceIonizationRateUpdate << std::endl;
          time.reportStatistics = false;
          time.iterSinceIonizationRateUpdate = 0;
        }
      }  // omp master
#pragma omp barrier
    }                 /* end of loop over time */
  }                   // omp parallel
#pragma omp taskwait  // wait for last snapshot to finish writing

  for (size_t i = 0; i < nCodeTimes; ++i) {
    if (i == RTDataBcast || i == RTRatesReduction) continue;  // use omp_get_w_time so already in seconds
    CodeTimeArray[i] *= 1e-3;                                 // convert to seconds
  }
  TotalTime = getTimeInSeconds() - StartTime;
  std::cout << "Walltime                 = " << TotalTime << "s" << std::endl;
  std::cout << "Iteration count          = " << IterationCount << std::endl;
  std::cout << "Time per iteration       = " << TotalTime / (double)IterationCount << "s" << std::endl;
  std::cout << "Zeroing Rates            = " << 100.0 * CodeTimeArray[ZeroingRates] / TotalTime << " %" << std::endl;
  std::cout << "Ray Creation             = " << 100.0 * CodeTimeArray[RayCreation] / TotalTime << " %" << std::endl;
  std::cout << "Ray Tracing              = " << 100.0 * CodeTimeArray[RayTracing] / TotalTime << " %" << std::endl;
  std::cout << "Column Densities         = " << 100.0 * CodeTimeArray[ColumnDensities] / TotalTime << " %" << std::endl;
  std::cout << "Setting Distances        = " << 100.0 * CodeTimeArray[Distances] / TotalTime << " %" << std::endl;
  std::cout << "Involatiles Luminosity   = " << 100.0 * CodeTimeArray[InvolatilesLuminosity] / TotalTime << " %"
            << std::endl;
  std::cout << "source-Particle Rates    = " << 100.0 * CodeTimeArray[SPPRates] / TotalTime << " %" << std::endl;
  std::cout << "Recombination Rates      = " << 100.0 * CodeTimeArray[RecombinationRates] / TotalTime << " %"
            << std::endl;
  std::cout << "Updating Heating Rates   = " << 100.0 * CodeTimeArray[UpdateHeatingRates] / TotalTime << " %"
            << std::endl;
  std::cout << "Cooling Rates            = " << 100.0 * CodeTimeArray[CoolingRates] / TotalTime << " %" << std::endl;
  std::cout << "Set Hydrogen Equilibrium = " << 100.0 * CodeTimeArray[SetHydrogenEquilibriumValues] / TotalTime << " %"
            << std::endl;
  std::cout << "Finding Timestep         = " << 100.0 * CodeTimeArray[FindingTimestep] / TotalTime << " %" << std::endl;
  std::cout << "Updating Particles       = " << 100.0 * CodeTimeArray[UpdatingParticles] / TotalTime << " %"
            << std::endl;
  std::cout << "IO time                  = " << 100.0 * CodeTimeArray[IO] / TotalTime << " %" << std::endl;
  if (nDev > 1) {
    std::cout << "RTData Broadcast wait    = " << 100.0 * CodeTimeArray[RTDataBcast] / TotalTime << " %" << std::endl;
    std::cout << "RTRates Reduction wait   = " << 100.0 * CodeTimeArray[RTRatesReduction] / TotalTime << " %"
              << std::endl;
  }

  double SumKnownTimes = 0.0;
  for (size_t i = 0; i < nCodeTimes; ++i) {
    SumKnownTimes += CodeTimeArray[i];
  }
  std::cout << "Other                    = " << 100.0 * (TotalTime - SumKnownTimes) / TotalTime << " %" << std::endl;

  for (int i = 0; i < nDev; i++) {
    RT_CUDA_freeIntegralInvolatilesOnDevice(lists->d_involatiles[i]);
  }

  free(s);

  return EXIT_SUCCESS;
}
