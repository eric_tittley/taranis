#ifndef _TARANIS_VARS_
#define _TARANIS_VARS_
#include "Taranis.h"
#include "thrust/device_vector.h"

extern RT_ConstantsT _Constants;
extern RT_ConstantsT _Constants_dev;

extern ParametersT _Parameters;

#ifdef HEALPIX_RAYS
extern thrust::device_vector<float> _ray_directions;
#endif

extern RT_SourceT* _Source;

extern float _current_time;

extern rt_float* _source_table;

extern size_t _num_sources;
extern size_t _num_sources_max;
#endif
