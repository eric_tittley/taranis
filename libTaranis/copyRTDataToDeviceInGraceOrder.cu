#include "Taranis.cuh"

#include "cudasupport.h"

//RT_Data_dest will be in grace order, the copy left on the host will remain in gadget order
void copyRTDataToDeviceInGraceOrder(RT_Data const Data_src,
                                    RT_Data *Data_dest,
                                    thrust::host_vector<unsigned int> &gadget_map) {
 size_t nParticles = Data_src.NumCells;
 //use tmp copy of data to rearrange 
 RT_Data * Data_tmp_p = RT_Data_Allocate(nParticles);
 RT_Data Data_tmp = *Data_tmp_p;

 //set values
 Data_tmp.NumCells = Data_src.NumCells;
 Data_tmp.Cell     = Data_src.Cell;
 Data_tmp.NBytes   = Data_src.NBytes;
 #pragma omp parallel for
 for(size_t i=0; i<nParticles; i++){
  size_t index = gadget_map[i]; //old index
 
  Data_tmp.R[i]       = Data_src.R[index];
  Data_tmp.dR[i]      = Data_src.dR[index];
  Data_tmp.Density[i] = Data_src.Density[index];
  Data_tmp.Entropy[i] = Data_src.Entropy[index];
  Data_tmp.T[i]       = Data_src.T[index];

  Data_tmp.n_H[i]   = Data_src.n_H[index];
  Data_tmp.f_H1[i]  = Data_src.f_H1[index];
  Data_tmp.f_H2[i]  = Data_src.f_H2[index];
  Data_tmp.n_He[i]  = Data_src.n_He[index];
  Data_tmp.f_He1[i] = Data_src.f_He1[index];
  Data_tmp.f_He2[i] = Data_src.f_He2[index];
  Data_tmp.f_He3[i] = Data_src.f_He3[index];

  Data_tmp.column_H1[i]  = 0.0;
  Data_tmp.column_He1[i] = 0.0;
  Data_tmp.column_He2[i] = 0.0;
 }
 cudaError_t cudaError;
 size_t const sizeOfHostMemSpace = Data_src.NBytes; /* bytes */
 cudaError = cudaMalloc( &(Data_dest->MemSpace), sizeOfHostMemSpace );
 checkCudaAPICall(cudaError, __FILE__, __LINE__, true);

 cudaError = cudaMemcpy( Data_dest->MemSpace,
                         Data_tmp.MemSpace,
                         sizeOfHostMemSpace,
                         cudaMemcpyHostToDevice );
 cudaDeviceSynchronize();
 checkCudaAPICall(cudaError, __FILE__, __LINE__, true);

 Data_dest->NBytes   = Data_src.NBytes;
 Data_dest->NumCells = Data_src.NumCells; /* == nParticles */
 /* Set the pointers in Data_dev_RT */
 RT_Data_Assign ( Data_dest );
 RT_Data_Free(Data_tmp_p);
}