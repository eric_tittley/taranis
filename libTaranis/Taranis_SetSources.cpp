#include "Manidoowag.h"
#include "Taranis.h"

/* Forward declaration. Not used elsewhere, so not in header. */
void setSourcePositions(ParametersT const Parameters, rt_float const BoxSize, RT_SourceT* const Source);

void Taranis_SetSources(ParametersT const Parameters,
                        rt_float const BoxSize,
                        RT_ConstantsT const Constants,
                        RT_SourceT* const Source,
                        rt_float const UnitLength) {
  /* T = 1E5 K blackbody flux in units of _ionizing_ photons s^-1 m^-2.
   * (Derive by integrating planck's law / (h_p * nu) * cos(theta) over
   * frequency and solid angle. cos(theta) term due to Lambert's cosine
   * law. Frequency integral is from nu = nu_0 -> infinity, where n_0 is
   * the ionization threshold of hydrogen; solid angle integral over
   * hemipsphere.
   * The frequency integral from nu_0 -> infinity can be found by
   * computing the integral from 0 -> infinity, which has an analytic
   * solution (~ zeta(3)) and subtracting the integral from 0 -> nu_0,
   * which can be expressed in terms of polylogarithms, but must
   * ultimately be computed numerically.)
   */
  const double Flux1E5BB = 1.067678205e+30;

  switch (Parameters.ProblemType) {
    case 100:
      for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
        Source[iSource].r[0] = 0.5 * BoxSize; /* Gadget internal units */
        Source[iSource].r[1] = 0.5 * BoxSize; /* Gadget internal units */
        Source[iSource].r[2] = 0.5 * BoxSize; /* Gadget internal units */
        Source[iSource].SourceType = SourceBlackBody;
        /* Source always on! */
        Source[iSource].z_on = 1e30;
        Source[iSource].z_off = 0.;
        Source[iSource].nu_end = 0.0; /* Needs to be initialized */
        /* 5E48 photons/sec, 1E5 K black body */
        Source[iSource].T_BB = 1E5; /* K */
        /* Ndot = 5E48 ionizing photons/sec equivalent */
        Source[iSource].SurfaceArea =
            (rt_float)(5E48 / Flux1E5BB / (double)Parameters.N_rays / Parameters.nSources); /* m^2 per ray */
        Source[iSource].AttenuationGeometry = RaySphericalWave;
      }
      break;

    case 0: { /* N sources placed in halos in mass order */
      rt_float luminosityPerSource = Parameters.totalSourceLuminosity / ((rt_float)Parameters.nSources);

      /* Read in the halo centres from Parameters.HaloCatalogueFile.
       * The file should be a list of halo centres, with the first line being the
       * number of halos and the following lines the x y z sets normalized [0,1)
       */
      setSourcePositions(Parameters, BoxSize, Source);

      for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
        /*  Power law: nu^-1, at the centre. */
        Source[iSource].SourceType = SourcePowerLawMinusOne;
        Source[iSource].z_on = 8000.0;
        Source[iSource].z_off = 3.0;
        Source[iSource].L_0 = luminosityPerSource / (rt_float)Parameters.N_rays; /* W / Hz @ nu_0 */
        Source[iSource].nu_end = 0.0;                                            /* Needs to be initialized */
        Source[iSource].AttenuationGeometry = RaySphericalWave;
      }
    } break;

    case 1:                           /* Test 1 */
      Source[0].r[0] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[1] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[2] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].SourceType = SourceMonochromatic;
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.0;
      Source[0].nu = Constants.nu_0;
      Source[0].nu_end = 0.0;                         /* Needs to be initialized */
      Source[0].L_0 = 1.0895e+31 / Parameters.N_rays; /* J/s / ray */
      Source[0].AttenuationGeometry = RaySphericalWave;
      break;

    case 2:                           /* Test 2 - Constant density medium*/
    case 5:                           /* Test 5 - Dynamical expansion of an HII region into a constant density medium.*/
      Source[0].r[0] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[1] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[2] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].SourceType = SourceBlackBody;
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      Source[0].nu_end = 0.0; /* Needs to be initialized */
      /* 5E48 photons/sec, 1E5 K black body */
      Source[0].T_BB = 1E5; /* K */
      /* Ndot = 5E48 ionizing photons/sec equivalent */
      Source[0].SurfaceArea = (rt_float)(5E48 / Flux1E5BB / (double)Parameters.N_rays); /* m^2 per ray */
      Source[0].AttenuationGeometry = RaySphericalWave;
      break;

    case 3: /* Test 3 */
      /* Rays enter plane-parallel, from x = 0 plane. */
      Source[0].r[0] = 1;
      Source[0].r[1] = 0;
      Source[0].r[2] = 0;

      Source[0].SourceType = SourceBlackBody;
      Source[0].PlaneSide = BoxSize; /* Gadget internal units */
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      Source[0].nu_end = 0.0; /* Needs to be initialized */
      /* 1E6 ionizing photons/sec/cm^2, 1E5 K black body */
      Source[0].T_BB = 1E5;
      /* F = 1E6 ionizing photons/sec/cm^2 = 1E10 photons/sec/m^2 is required. */
      {
        double TotalArea =
            1E10 * (double)BoxSize * (double)BoxSize * (double)UnitLength * (double)UnitLength / Flux1E5BB; /* m^2 */
        Source[0].SurfaceArea = TotalArea / Parameters.N_rays; /* m^2 per ray */
      }
      Source[0].AttenuationGeometry = RayPlaneWave;
      break;

    case 35: /*similar to test 3, clump in center. 2 sources to cast a diagonal shadow*/
      Source[0].r[0] = 0.25 * BoxSize;
      Source[0].r[1] = 0.5 * BoxSize;
      Source[0].r[2] = 0.5 * BoxSize;

      Source[0].SourceType = SourceBlackBody;
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      Source[0].nu_end = 0.0; /* Needs to be initialized */
      /* 1E6 ionizing photons/sec/cm^2, 1E5 K black body */
      Source[0].T_BB = 1E5;
      /* F = 1E6 ionizing photons/sec/cm^2 = 1E10 photons/sec/m^2 is required. */
      Source[0].SurfaceArea = (rt_float)(1E48 / Flux1E5BB / (double)Parameters.N_rays); /* m^2 per ray */
      Source[0].AttenuationGeometry = RaySphericalWave;

      Source[1].r[0] = 0.5 * BoxSize;
      Source[1].r[1] = 0.25 * BoxSize;
      Source[1].r[2] = 0.5 * BoxSize;

      Source[1].SourceType = SourceBlackBody;
      /* Source always on! */
      Source[1].z_on = 1e30;
      Source[1].z_off = 0.;
      Source[1].nu_end = 0.0; /* Needs to be initialized */
      /* 1E6 ionizing photons/sec/cm^2, 1E5 K black body */
      Source[1].T_BB = 1E5;
      /* F = 1E6 ionizing photons/sec/cm^2 = 1E10 photons/sec/m^2 is required. */
      Source[1].SurfaceArea = (rt_float)(1E48 / Flux1E5BB / (double)Parameters.N_rays); /* m^2 per ray */
      Source[1].AttenuationGeometry = RaySphericalWave;
      break;
    case 4: /* Test 4 */
    {
      if (Parameters.nSources != 16) {
        std::cerr << "ERROR: " << __FILE__ << ": " << __LINE__ << " Test 4 has 16 sources. Parameters file states "
                  << Parameters.nSources << " sources." << std::endl;
      }

      /* Read source positions and unscaled luminosities from halo catalogue */
      setSourcePositions(Parameters, BoxSize, Source);

      for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
        Source[iSource].SourceType = SourceBlackBody;
        /* Source always on! */
        Source[iSource].z_on = 1e30;
        Source[iSource].z_off = 0.;
        Source[iSource].nu_end = 0.0; /* Needs to be initialized */
        Source[iSource].T_BB = 1E5;   /* HARD SPECTRA, K */
        /* Ndot = L[iSource] * 1E52 ionizing photons / sec */
        /* Source.L_0 will have source luminosities in units of 1E52 ionizing photons/sec. */
        Source[iSource].SurfaceArea = (rt_float)(((double)Source[iSource].L_0 * (double)1E52) /
                                                 (Flux1E5BB * (double)Parameters.N_rays)); /* m^2 per ray */
        Source[iSource].AttenuationGeometry = RaySphericalWave;
      }
    } break;

      /* case 5: See case 2 */

    case 6: /* Test 6 - Dynamical expansion of an HII region into a flat-topped r^-2 density density field. */
      Source[0].r[0] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[1] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[2] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].SourceType = SourceBlackBody;
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      Source[0].nu_end = 0.0; /* Needs to be initialized */
      /* 1E50 photons/sec, 1E5 K black body */
      Source[0].T_BB = 1E5; /* K */
      /* Ndot = 1E50 ionizing photons/sec equivalent */
      Source[0].SurfaceArea = (rt_float)(1E50 / Flux1E5BB / (double)Parameters.N_rays); /* m^2 per ray */
      Source[0].AttenuationGeometry = RaySphericalWave;
      break;

    case 20:
      for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
        Source[iSource].r[0] = 0.5 * BoxSize; /* Gadget internal units */
        Source[iSource].r[1] = 0.5 * BoxSize; /* Gadget internal units */
        Source[iSource].r[2] = 0.5 * BoxSize; /* Gadget internal units */
        Source[iSource].SourceType = SourceBlackBody;
        /* Source always on! */
        Source[iSource].z_on = 1e30;
        Source[iSource].z_off = 0.;
        Source[iSource].nu_end = 0.0; /* Needs to be initialized */
        /* 5E48 photons/sec, 1E5 K black body */
        Source[iSource].T_BB = 1E5; /* K */
        /* Ndot = 5E48 ionizing photons/sec equivalent */
        Source[iSource].SurfaceArea =
            (rt_float)(5E48 / Flux1E5BB / (double)Parameters.N_rays / Parameters.nSources); /* m^2 per ray */
        Source[iSource].AttenuationGeometry = RaySphericalWave;
      }
      break;

    default:
      printf("%s: %i: ERROR: Unknown ProblemType %i\n", __FILE__, __LINE__, (int)Parameters.ProblemType);
  }
}

void setSourcePositions(ParametersT const Parameters, rt_float const BoxSize, RT_SourceT* const Source) {
  std::cout << "Reading halo catalogue " << Parameters.HaloCatalogueFile << " ...";
  /* Read in the halo centres.
   * The file should be a list of halo centres, in order of most
   * massive/dense/whatever first, the first line being the number of centres
   * and the following lines the x y z sets normalized [0,1) */
  rt_float* haloCentres;
  rt_float* Ls;
  size_t const nHalos = readHaloCatalogue(Parameters.HaloCatalogueFile, haloCentres, Ls);
  if (nHalos == 0) {
    std::cerr << std::endl
              << "ERROR: " << __FILE__ << ": " << __LINE__ << " Trouble reading Halo Catalogue." << std::endl;
  }
  if (nHalos < Parameters.nSources) {
    std::cerr << std::endl
              << "ERROR: " << __FILE__ << ": " << __LINE__ << " Fewer halos (" << nHalos << ") than sources ("
              << Parameters.nSources << ")" << std::endl;
  }
  std::cout << "...done" << std::endl;

  /* Set up positions */
  for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
    Source[iSource].r[0] = haloCentres[3 * iSource + 0] * BoxSize; /* Gadget internal units */
    Source[iSource].r[1] = haloCentres[3 * iSource + 1] * BoxSize; /* Gadget internal units */
    Source[iSource].r[2] = haloCentres[3 * iSource + 2] * BoxSize; /* Gadget internal units */
    Source[iSource].L_0 = Ls[iSource];
  }
  free(haloCentres);
  free(Ls);
}
