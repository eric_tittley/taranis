#include <allvars.h>

#include "config.h"

#ifdef STELLAR_SOURCES
extern "C" {
#  include <proto.h>

#  include "RT_Luminosity.h"
}

#  include <algorithm>
#  include <cstdio>
#  include <numeric>
#  include <vector>

#  include "ERT_gizmo.h"
#  include "taranis_vars.h"

constexpr double STELLAR_AGE_CUTOFF_MA = 30.;
constexpr double ESCAPE_FRACTION = 0.000001;

// table - list of (currently) 700 floats. age alpha L0 alpha L0 alpha L0
/* table is in column order */

static float get_table_element(int table_idx, int k) { return _source_table[k * 100 + table_idx]; }

static float get_table_alpha(int table_idx, int frequency_bin) {
  return get_table_element(table_idx, 1 + 2 * frequency_bin);
}

static float get_table_L_0(int table_idx, int frequency_bin) {
  return get_table_element(table_idx, 2 + 2 * frequency_bin);
}

static rt_float RT_Luminosity_single_starburst(RT_LUMINOSITY_ARGS) {
  int frequency_bin;
  if (nu < Constants->nu_1) {
    frequency_bin = 0;
  } else if (nu < Constants->nu_2) {
    frequency_bin = 1;
  } else {
    frequency_bin = 2;
  }

  size_t index = Source->particle_id;
  const rt_float mass_unit_1e6msol = (All.UnitMass_in_g * 1e-6) / (All.HubbleParam * SOLAR_MASS);

  rt_float mass = P[index].Mass * mass_unit_1e6msol;
  int age_bin = round(P[index].StellarAge);
  rt_float alpha = get_table_alpha(age_bin * 2, frequency_bin);
  rt_float L_0 = get_table_L_0(age_bin * 2, frequency_bin);
  if (L_0 <= 0) {
    L_0 = 0.;
  }
  rt_float L_nu = L_0 * std::pow(nu / Constants->nu_0, alpha) * mass * ESCAPE_FRACTION;
  return L_nu;
}

void ert_set_sources_from_stars() {
  // Ngroups and Group are available from allvars.h
  // sort by stellar mass
  std::vector<size_t> star_indices;
  printf("Setting sources [STELLAR_SOURCES]...\n");
  for (size_t iParticle = 0; iParticle < All.TotNumPart; iParticle++) {
    if (P[iParticle].Type != 4) {
      continue;
    }
    double star_age = evaluate_stellar_age_Gyr(P[iParticle].StellarAge) * 1000;
    if (star_age < STELLAR_AGE_CUTOFF_MA) {
      star_indices.push_back(iParticle);
    }
  }

  int num_sources = star_indices.size();

  printf("%d sources \n", num_sources);
  _num_sources = num_sources;
  //allocate memory for sources
  free(_Source);
  _Source = (RT_SourceT *)malloc(num_sources * sizeof(RT_SourceT));
  // Set source positions
  for (size_t i = 0; i < num_sources; ++i) {
    size_t index = star_indices[i];
    for (size_t j = 0; j < 3; j++) _Source[i].r[j] = P[index].Pos[j];
    _Source[i].AttenuationGeometry = RaySphericalWave;
    _Source[i].SourceType = SourceUser;
    _Source[i].LuminosityFunction = (SourceFunctionT)&RT_Luminosity_single_starburst;
    _Source[i].particle_id = index;
  }
}

#endif  // STELLAR_SOURCES
