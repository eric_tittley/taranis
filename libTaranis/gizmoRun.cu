#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "Taranis.cuh"
#include "Taranis.h"
#include "config.h"
#include "cudasupport.h"
#include "taranis_vars.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "RT_FileHeaderT.h"
#ifdef __cplusplus
}
#endif

#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <thrust/sort.h>
#include <thrust/system_error.h>

#include "grace/types.h"

/* RT includes */
#include "RT.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"
#include "allvars.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "proto.h"
#ifdef __cplusplus
}
#endif

#include <stdlib.h>

#include <vector>

void ert_parent_routine() {
  printf("Entering ert_parent_routine [Taranis]...\n");

  double tStart = getTimeInSeconds();
  cudaError_t cudaError;
  lists_t lists;

#ifdef STELLAR_SOURCES
  V_LOG("Setting sources from star particles\n");
  ert_set_sources_from_stars();
#endif

  V_LOG("Setting up GPUs\n");
  int nDev = 0;
  cudaGetDeviceCount(&nDev);
  while (_num_sources / nDev < MIN_SOURCES_PER_GPU && nDev > 1) {
    --nDev;
  }
  if (nDev < 1) {
    nDev = 1;
  }
  int *devs = (int *)malloc(sizeof(int) * nDev);
  for (int i = 0; i < nDev; i++) {
    devs[i] = i;
  }

  A_LOG("Using %d device(s)\n", nDev);
  cudaSetDevice(devs[0]);

  V_LOG("Setting up particle parameters...\n");

  size_t const nParticles = All.TotN_gas;
  double UnitLength = UNIT_LENGTH_IN_CGS / 100;
  double UnitMass = UNIT_MASS_IN_CGS / 1000;

  if (All.ComovingIntegrationOn) {
    /* Scale by the expansion factor */
    UnitLength *= All.Time;
  }
  rt_float GizmoDensityUnit =
      (rt_float)((UnitMass / (double)UnitLength) / (double)UnitLength) / (double)UnitLength; /* kg/m^3) */
  double GasParticleMass;
  double NumberOfHydrogenAtomsPerParticle;
  double NumberOfHeliumAtomsPerParticle;
  double density_to_n_H = (1.0 - _Constants.Y) / (_Constants.mass_p + _Constants.mass_e) * GizmoDensityUnit;
  rt_float n_H_to_n_He = 0.25 * _Constants.Y / (1.0 - _Constants.Y); /* Nan if Y==1 */
  printf("density_to_n_H = %e\n", density_to_n_H);
  printf("HubbleParam = %e\n", All.HubbleParam);
  V_LOG("done.\n");

  size_t iParticle = 0, iGas = 0;
  const int max_per_leaf = 32;
  thrust::host_vector<SphereType> h_particles(nParticles);
  /* Scale the incoming smoothing lengths.
   * Used for creating the tree and setting Data.dR further down.
   * Use 1.0 if no change is to be made.  This works for Test 6.
   * Use 1/2 (0.5) if the incoming h's (Hsml) are compact over 2.
   * If we also want h_particles to be packed as if Nsph = 1, not something
   * like 32 or 64, then use
   * 1 / CBRT((rt_float)_Parameters.Nsph);
   * Assumes Nsph in Parameters is set to what Nsph is in Gizmo.
   * Test 6 fails with 1/Nsph^(1/3)
   * NOTE it is important to remember that Taranis doesn't *really* care what the smoothing
   * radii are; it'll just spread the matter out over the volume.  Large h's mean 
   * softer edges, less shot noise, and slower execution.  However some of the tests fail
   * if, for example, it is set to pack them like Nsph = 1;
   */
  // const rt_float SmoothingLengthNormalization = 1.;
  const rt_float SmoothingLengthNormalization = 0.5;
  // const rt_float SmoothingLengthNormalization = (rt_float)1./CBRT((rt_float)_Parameters.Nsph);

  // copy to grace format
  for (iParticle = 0; iParticle < N_gas; iParticle++) {
    if (P[iParticle].Type != 0) {
      continue;
    }
    h_particles[iGas].x = (rt_float)P[iParticle].Pos[0];  // code units
    h_particles[iGas].y = (rt_float)P[iParticle].Pos[1];
    h_particles[iGas].z = (rt_float)P[iParticle].Pos[2];
    h_particles[iGas].r = (rt_float)P[iParticle].Hsml * SmoothingLengthNormalization;
    iGas++;
  }

  //  if (iGas != nParticles) {
  //    gizmo_terminate("Taranis: Unexpected number of gas particles, All.TotN_gas invalid");
  //  }

  /* Copy particle data to the GPU and build tree */
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    thrust::device_vector<SphereType> *d_particles = new thrust::device_vector<SphereType>(nParticles);
    thrust::host_vector<unsigned int> *host_map = new thrust::host_vector<unsigned int>(nParticles);
    grace::CudaBvh *d_tree = new grace::CudaBvh(nParticles, max_per_leaf);

    thrust::copy(h_particles.begin(), h_particles.end(), (*d_particles).begin());

    buildTree(*d_particles, *d_tree, *host_map);

    lists.particles.push_back(d_particles);
    lists.tree.push_back(d_tree);
    lists.map.push_back(host_map);
  }

  // Allocate RT_Data and Rates
  RT_Data *Data_p = RT_Data_Allocate(nParticles);
  RT_Data Data = *Data_p;
  size_t const sizeOfHostMemSpace = Data.NBytes; /* bytes */

  for (size_t iParticle = 0, iGas = 0; iParticle < N_gas; iParticle++) {
    if (P[iParticle].Type != 0) {
      continue;
    }

    size_t index = (*lists.map[0])[iGas];

    GasParticleMass = P[index].Mass * UnitMass; /* kg */
    // calculate this per particle to remove the assumption that all particles
    // have the same mass
    // TODO: Currently this doesn't work as we just pass the last value to loopOverTime which will break if particles
    // actually have different masses
    NumberOfHydrogenAtomsPerParticle =
        GasParticleMass * (double)(1.0 - _Constants.Y) / (double)(_Constants.mass_p + _Constants.mass_e);
    NumberOfHeliumAtomsPerParticle =
        GasParticleMass * (double)_Constants.Y / (double)(4.0 * (_Constants.mass_p + _Constants.mass_e));

    // update fractions first (needed for entropy calculations)
    Data.f_H1[iGas] = (rt_float)SphP[index].HI;
    Data.f_H2[iGas] = (rt_float)SphP[index].HII;
    Data.f_He1[iGas] = (rt_float)SphP[index].HeI;
    Data.f_He2[iGas] = (rt_float)SphP[index].HeII;
    Data.f_He3[iGas] = (rt_float)SphP[index].HeIII;

    Data.dR[iGas] = UnitLength * P[index].Hsml * SmoothingLengthNormalization;
    Data.Density[iGas] = GizmoDensityUnit * (rt_float)SphP[index].Density; /* kg m^-3 */
    Data.n_H[iGas] = density_to_n_H * (rt_float)SphP[index].Density;       /* m^-3 */
    if (_Constants.Y < 1.) {
      Data.n_He[iGas] = n_H_to_n_He * Data.n_H[iGas];
    } else { /* Y==1 */
      Data.n_He[iGas] = (rt_float)SphP[index].Density / (4.0 * (_Constants.mass_p + _Constants.mass_e));
    }

    /* Update the neutral & ionised number densities. */
    rt_float const n_H2 = Data.n_H[iGas] * Data.f_H2[iGas];
    rt_float const n_He2 = Data.n_He[iGas] * Data.f_He2[iGas];
    rt_float const n_He3 = Data.n_He[iGas] * Data.f_He3[iGas];

    /* Electron density */
    rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

    /* Total number density */
    rt_float const n = Data.n_H[iGas] + Data.n_He[iGas] + n_e;

    rt_float const mu = (Data.n_H[iGas] + 4. * Data.n_He[iGas]) / n;

    rt_float GadgetUtoTFactor = 1.e6 * (2. / 3.) * mu * _Constants.mass_p / _Constants.k_B;
    rt_float temperature = GadgetUtoTFactor * (rt_float)SphP[index].InternalEnergy;
    if (temperature == 0) printf("ERROR: InternalEnergy = %e; T = %e\n", SphP[index].InternalEnergy, temperature);
    Data.T[iGas] = temperature; /* Temperature [K] */
    Data.Entropy[iGas] = RT_EntropyFromTemperature(_Constants, temperature, iGas, Data);
    iGas++;
  }

  V_LOG("Allocating RT_Data\n");
  cudaSetDevice(devs[0]);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    RT_Data *Data_dev_i = new RT_Data();
    cudaMalloc(&(Data_dev_i->MemSpace), sizeOfHostMemSpace);
    if (i == 0) {
      // Only copy data once. This gets broadcast to all devices in loop over time.
      cudaMemcpy(Data_dev_i->MemSpace, Data.MemSpace, Data.NBytes, cudaMemcpyHostToDevice);
      checkCUDAErrors("Memcpy failed", __FILE__, __LINE__);
    }
    Data_dev_i->NBytes = Data.NBytes;
    Data_dev_i->NumCells = Data.NumCells;
    RT_Data_Assign(Data_dev_i);
    lists.data.push_back(Data_dev_i);
  }

  V_LOG("Allocating RT_Rates\n");
  lists.rates.resize(nDev);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    lists.rates[i] = RT_CUDA_RatesBlock_Allocate(nParticles);
    lists.rates[i].NumCells = nParticles;
  }

  V_LOG("Generating Healpix ray directions\n");
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    thrust::device_vector<float> *ray_dir = new thrust::device_vector<float>();
    auto ierr = generateHealpixRayDirections(_Parameters.N_rays, *ray_dir);
    if (ierr == EXIT_FAILURE) {
      printf("ERROR: %s: %i: Failed to generate Healpix Ray Directions.\n", __FILE__, __LINE__);
      (void)fflush(stdout);
    }
    lists.ray_dir.push_back(ray_dir);
  }
  cudaSetDevice(devs[0]);

#ifdef DEBUG
  /* Sanity Check */
  double TotalMassInVolume = 0.0;
  bool PerformSanityCheck = true;
  switch (_Parameters.ProblemType) {
    case 5: /* Test 2 with hydrodynamical response */
      /* Note: Assuming (30 kpc)^3 volume. Not the (15 kpc)^3 volume described
       *       in the test documentation because we put the source at the centre */
      TotalMassInVolume = 1.3276e+39;
      break;
    default:
      printf("%s: %i: WARNING: Unknown ProblemType for Sanity Check: %i\n",
             __FILE__,
             __LINE__,
             (int)_Parameters.ProblemType);
      PerformSanityCheck = false;
  }
  if (PerformSanityCheck) {
    double BoxVolume = (double)All.BoxSize * (double)All.BoxSize * (double)All.BoxSize * (double)UnitLength *
                       (double)UnitLength * (double)UnitLength;
    double MeanDensity = 0.0;
    for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
      MeanDensity += Data.Density[iParticle];
    }
    MeanDensity /= (double)nParticles;
    double Mean_nH = 0.0;
    for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
      Mean_nH += (double)Data.n_H[iParticle];
    }
    Mean_nH /= (double)nParticles;
    double SumOfVolumes = 0.0;
    for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
      SumOfVolumes += (double)Data.dR[iParticle] * (double)Data.dR[iParticle] * (double)Data.dR[iParticle];
    }
    SumOfVolumes *= (4.0 / 3.0) * M_PI;

    printf("Mean_nH=%9.3e; BoxVolume=%9.3e; BoxSize=%9.3e; UnitLength=%9.3e\n",
           Mean_nH,
           BoxVolume,
           All.BoxSize,
           UnitLength);
    printf("SANITY CHECK (works for constant density only)\n");
    printf(" MeanDensity * BoxVolume      = %9.3e kg\n", MeanDensity * BoxVolume);
    printf(" Mean_nH*m_H*BoxVolume/(1-Y)  = %9.3e kg\n", Mean_nH * 1.6737e-27 * BoxVolume / (1.0 - _Constants.Y));
    printf(" nParticles * GasParticleMass = %9.3e kg\n", nParticles * GasParticleMass);
    printf("  Should be:                    %9.3e kg\n", TotalMassInVolume);
    printf(" (Sum of volumes)/(Total volume) = %9.3e\n", SumOfVolumes / BoxVolume);
    printf("  Should be                          %f\n", 1.0);
  }
  /* END OF SANITY CHECK */
#endif

if (ert_DumpFlag == 1) {
  if(ThisTask == 0 && All.Time != 0.0) {
    printf("Writing RT snapshot...");
    ert_write_snapshot(ert_snapshot_num);
    printf("done.\n");
  }
  ert_DumpFlag=0;
}

#ifdef TRACK_IONIZATION_RATE
  double NumberOfIonizedHydrogenAtoms_start = 0.0, NumberOfIonizedHydrogenAtoms_end = 0.0;
  for (int i = 0; i < nParticles; ++i) {
    NumberOfIonizedHydrogenAtoms_start += (double)Data.f_H2[i];
  }
  NumberOfIonizedHydrogenAtoms_start *= NumberOfHydrogenAtomsPerParticle;
#endif

  rt_float const t_start = _current_time;  // Stored globally from end of last call to parent_routine
  rt_float t = t_start;                    // Keep track of current time as we loop in this call
                                           // to parent_routine
  rt_float t_stop, redshift, ExpansionFactor;
  switch (All.ComovingIntegrationOn) {
    case 0:
      t_stop = All.Time * UNIT_TIME_IN_CGS;  // current gizmo time in seconds
      redshift = 0;
      ExpansionFactor = 1.0;
      break;
    case 1:
      t_stop = cosmological_time(All.Time, _Constants);
      redshift = 1. / All.Time - 1.0;
      ExpansionFactor = All.Time;
      break;
    default:
      std::cerr << __FILE__ ": " << __LINE__ << ": "
                << "ERROR: unknown value of ComovingIntegrationOn:" << All.ComovingIntegrationOn << std::endl;
      return;
  }

  printf("t_start %e s (%5.3e Ma), t_stop %e s (%5.3e Ma), dt=%5.3e Ma\n",
         t_start,
         t_start / _Constants.Ma,
         t_stop,
         t_stop / _Constants.Ma,
         (t_stop - t_start) / _Constants.Ma);

  rt_float RayLength = 1.8 * All.BoxSize;  // should be a function of the source

  rt_float InitialTemperature = 100.0;
  printf("done.\n");

  cudaDeviceSynchronize();
  V_LOG("Starting loopOverTime...\n");
  V_LOG("nParticles = %lu \n", nParticles);

  loopOverTime(_Source,
               _num_sources,
               t_start,
               t_stop,
               UnitLength,
               redshift,
               All.BoxSize,
               RayLength,
               _Parameters.N_rays,
               NumberOfHydrogenAtomsPerParticle,
               NumberOfHeliumAtomsPerParticle,
               _Constants,
               nDev,
               devs,
               nParticles,
               &lists,
               InitialTemperature /* Only used for monochromatic radiation in Test 1 */);

  cudaMemcpy(Data.MemSpace, lists.data[0]->MemSpace, Data.NBytes, cudaMemcpyDeviceToHost);
  rt_float *G = (rt_float *)malloc(sizeof(rt_float) * N_gas);
  rt_float *L = (rt_float *)malloc(sizeof(rt_float) * N_gas);
  cudaMemcpy(G, lists.rates[0].G, sizeof(rt_float) * N_gas, cudaMemcpyDeviceToHost);
  cudaMemcpy(L, lists.rates[0].L, sizeof(rt_float) * N_gas, cudaMemcpyDeviceToHost);

  checkCUDAErrors("Memcpy failed", __FILE__, __LINE__);
  V_LOG("Updating GIZMO Particle Energies...");
  for (iParticle = 0, iGas = 0; iParticle < N_gas; iParticle++) {
    if (P[iParticle].Type != 0) {
      continue;
    }
    size_t index = (*lists.map[0])[iGas];  // now we have the new index

    /* Update the neutral & ionised number densities. */
    rt_float const n_H2 = Data.n_H[iGas] * Data.f_H2[iGas];
    rt_float const n_He2 = Data.n_He[iGas] * Data.f_He2[iGas];
    rt_float const n_He3 = Data.n_He[iGas] * Data.f_He3[iGas];

    /* Electron density */
    rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

    /* Total number density */
    rt_float const n = Data.n_H[iGas] + Data.n_He[iGas] + n_e;
    rt_float const mu = (Data.n_H[iGas] + 4. * Data.n_He[iGas]) / n;
    rt_float GadgetUtoTFactor = 1.e6 * (2. / 3.) * mu * _Constants.mass_p / _Constants.k_B;
    rt_float newT = RT_TemperatureFromEntropy(_Constants, Data.Entropy[iGas], iGas, Data);
    rt_float unew = (1.0 / GadgetUtoTFactor) * newT;

    SphP[index].InternalEnergy = unew;
    SphP[index].InternalEnergyPred = unew;
    SphP[index].Pressure = get_pressure(index);

    SphP[index].Ne = n_e / Data.n_H[iGas];
    SphP[index].HI = Data.f_H1[iGas];
    SphP[index].HII = Data.f_H2[iGas];
    SphP[index].HeI = Data.f_He1[iGas];
    SphP[index].HeII = Data.f_He2[iGas];
    SphP[index].HeIII = Data.f_He3[iGas];

    SphP[index].Taranis_Temperature = Data.T[iGas];
    SphP[index].Taranis_Heating = G[iGas];
    SphP[index].Taranis_Cooling = L[iGas];
    iGas++;
  }
  _current_time = t_stop;
  V_LOG("done\n");

#ifdef TRACK_IONIZATION_RATE
  for (int i = 0; i < nParticles; ++i) {
    NumberOfIonizedHydrogenAtoms_end += (double)Data.f_H2[i];
  }
  NumberOfIonizedHydrogenAtoms_end *= NumberOfHydrogenAtomsPerParticle;
  double const IonizationRate =
      (NumberOfIonizedHydrogenAtoms_end - NumberOfIonizedHydrogenAtoms_start) / (t_stop - t_start);
  printf("Ionization rate = %5.3e; NumberOfIonizedHydrogenAtoms_start=%5.3e; NumberOfIonizedHydrogenAtoms_end=%5.3e;\n", IonizationRate, NumberOfIonizedHydrogenAtoms_start, NumberOfIonizedHydrogenAtoms_end);
#endif

  // free Data, Data_dev, Rates_dev
  RT_Data_Free(Data_p);
  for (int i = 0; i < nDev; i++) {
    cudaError = cudaFree((*lists.data[i]).MemSpace);
    if (cudaError != cudaSuccess) {
      printf("cudaFree on device %i returns: %s\n", devs[i], cudaGetErrorString(cudaError));
    }
  }

  for (int i = 0; i < nDev; i++) {
    RT_CUDA_RatesBlock_Free(lists.rates[i]);
  }

  for (int i = 0; i < nDev; i++) {
    delete lists.tree[i];
    delete lists.map[i];
    delete lists.particles[i];
    delete lists.ray_dir[i];
    delete lists.data[i];
  }
  free(devs);
  free(G);
  free(L);

  printf("Total time spent in Taranis = %e seconds\n", getTimeInSeconds() - tStart);
}