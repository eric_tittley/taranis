#include <cstdlib>
#include <fstream>

#include <thrust/host_vector.h>


/* Dump the column densities to file for examination */
int dumpColumnDensitiesAndMaps(thrust::host_vector<float> &Ncol_HI,
                               thrust::host_vector<float> &Ncol_HeI,
                               thrust::host_vector<float> &Ncol_HeII,
                               thrust::host_vector<unsigned int> &particle_indices,
                               thrust::host_vector<unsigned int> &gadget_map
                              ) {

#pragma omp single nowait
 {
  /* Only one thread needs to perform this, and no one needs to wait */
  std::ofstream ofile;
  std::string ColDensFileName = "ColumnDensities.dat";
  ofile.open(ColDensFileName.c_str(), std::ios::binary);
  size_t LengthOfNHI   = Ncol_HI.size();
  size_t LengthOfNHeI  = Ncol_HeI.size();
  size_t LengthOfNHeII = Ncol_HeII.size();
  size_t LengthOfParticleIndices   = particle_indices.size();
  size_t LengthOfGadgetMap   = gadget_map.size();
  ofile.write((char *)(&LengthOfNHI  ), sizeof(size_t));
  ofile.write((char *)(&LengthOfNHeI ), sizeof(size_t));
  ofile.write((char *)(&LengthOfNHeII), sizeof(size_t));
  ofile.write((char *)(&LengthOfParticleIndices  ), sizeof(size_t));
  ofile.write((char *)(&LengthOfGadgetMap  ), sizeof(size_t));
  ofile.write((char *)thrust::raw_pointer_cast(Ncol_HI.data()  ),Ncol_HI.size()  *sizeof(float));
  ofile.write((char *)thrust::raw_pointer_cast(Ncol_HeI.data() ),Ncol_HeI.size() *sizeof(float));
  ofile.write((char *)thrust::raw_pointer_cast(Ncol_HeII.data()),Ncol_HeII.size()*sizeof(float));
  ofile.write((char *)thrust::raw_pointer_cast(particle_indices.data()  ),LengthOfParticleIndices*sizeof(unsigned int));
  ofile.write((char *)thrust::raw_pointer_cast(gadget_map.data()  ),LengthOfGadgetMap*sizeof(unsigned int));
  ofile.close();
  printf("done\n");
 }
 return EXIT_SUCCESS;
}
