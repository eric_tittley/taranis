/*
 * Loops over the simulation time and updates Data_dev
 *
 *
 *
 */
#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <thrust/sort.h>
#include <thrust/system_error.h>

#include "Manidoowag.h"
#include "RT_CUDA.h"
#include "Taranis.cuh"
#include "Taranis.h"
#include "cudasupport.h"
#include "grace/ray.h"
#include "nccl.h"
#include "omp.h"

#define gpuErrchk(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true) {
  if (code != cudaSuccess) {
    fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}

#define NCCLCHECK(cmd)                                                                      \
  do {                                                                                      \
    ncclResult_t r = cmd;                                                                   \
    if (r != ncclSuccess) {                                                                 \
      printf("Failed, NCCL error %s:%d '%s'\n", __FILE__, __LINE__, ncclGetErrorString(r)); \
      exit(EXIT_FAILURE);                                                                   \
    }                                                                                       \
  } while (0)

void reduceRTRates(std : vector<RT_RatesBlockT> &rates_list, size_t nParticles, ncclComm_t *comms, cudaStream_t *s) {
  NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_H1,
                       (void *)rates_list[tid].I_H1,
                       nParticles,
                       ncclFloat,
                       ncclSum,
                       0,  // root
                       comms[tid],
                       s[tid]));
  NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_He1,
                       (void *)rates_list[tid].I_He1,
                       nParticles,
                       ncclFloat,
                       ncclSum,
                       0,  // root
                       comms[tid],
                       s[tid]));
  NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_He2,
                       (void *)rates_list[tid].I_He2,
                       nParticles,
                       ncclFloat,
                       ncclSum,
                       0,  // root
                       comms[tid],
                       s[tid]));
  NCCLCHECK(ncclReduce((const void *)rates_list[tid].Gamma_HI,
                       (void *)rates_list[tid].Gamma_HI,
                       nParticles,
                       ncclFloat,
                       ncclSum,
                       0,  // root
                       comms[tid],
                       s[tid]));
  NCCLCHECK(ncclReduce((const void *)rates_list[tid].G,
                       (void *)rates_list[tid].G,
                       nParticles,
                       ncclFloat,
                       ncclSum,
                       0,  // root
                       comms[tid],
                       s[tid]));
  // synchronizing on CUDA streams to wait for completion of NCCL operation
  cudaStreamSynchronize(s[tid]);
}

void loopOverSources(RT_SourceT *const source,
                     size_t const nSource,
                     rt_float const unitLength,
                     rt_float const redshift,
                     size_t const N_rays,
                     int const nDev,
                     double const N_H_per_particle,
                     double const N_He_per_particle,
                     size_t const nParticles,
                     RT_ConstantsT Constants,
                     std::vector<grace::CudaBvh *> &tree_list,
                     std::vector<thrust::host_vector<unsigned int> *> &map_list,
                     std::vector<thrust::device_vector<float> *> &ray_dir_list,
                     std::vector<thrust::device_vector<SphereType> *> &particle_list,
                     std::vector<RT_Data *> &data_list,
                     std::vector<RT_RatesBlockT> &rates_list,
                     std::vector<RT_IntegralInvolatilesT> &involatiles_list,
                     std::vector<RT_IntegralInvolatilesT> &d_involatiles_list,
                     double *CodeTimeArray) {
  V_LOG("Loop over %d sources...\n", num_sources_this_step);
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  float CheckPointTime;

  for (size_t iSource = i_source_start; iSource < i_source_end; ++iSource) {
    /* Loop over each source */
    V_LOG("Setting Involatiles Luminosity..");
    if (tid == 0) cudaEventRecord(start);
    ierr = RT_IntegralInvolatiles_setLuminosity(source[iSource],
                                                Constants,
                                                redshift, /* SourceRedshift */
                                                involatiles_list[tid]);
    assert(ierr == EXIT_SUCCESS);
    ierr = RT_CUDA_copyIntegralInvolatilesToDevice(involatiles_list[tid], d_involatiles_list[tid]);
    assert(ierr == EXIT_SUCCESS);

    if (tid == 0) {
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&CheckPointTime, start, stop);
      CodeTimeArray[InvolatilesLuminosity] += CheckPointTime;
    }

    V_LOG("done\n");
    // Set the distance from the source to each particle
    V_LOG("Calculating particle-source distances...");

    if (tid == 0) cudaEventRecord(start);
    setSourceToParticleDistance(*particle_list[tid], source[iSource], boxSize, unitLength, (*data_list[tid]));
    if (tid == 0) {
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&CheckPointTime, start, stop);
      CodeTimeArray[Distances] += CheckPointTime;
    }
    V_LOG(" done\n");

    /* Calculate the source-Particle pair rates. */
    if (tid == 0) cudaEventRecord(start);
    int ray_start = (iSource - i_source_start) * N_rays;  // id of first ray to this source
    int ray_end = ray_start + N_rays;                     // id of last ray to this source
    int start_offset = ray_offsets[ray_start];
    int end_offset = ray_offsets[ray_end] - 1;  // need to copy up to but not including the next set of rays
    if (ray_end == n_rays_this_step) end_offset = column_H1_SPP_dev.size() - 1;
    const size_t nSourceParticlePairs = end_offset - start_offset;

    /*
     * Create Iterators that point to the start of the current source's column densities and associated particle
     * indices.
     */
    thrust::device_vector<float>::iterator column_H1_SPP_dev_local = column_H1_SPP_dev.begin() + start_offset;
    thrust::device_vector<float>::iterator column_He1_SPP_dev_local = column_He1_SPP_dev.begin() + start_offset;
    thrust::device_vector<float>::iterator column_He2_SPP_dev_local = column_He2_SPP_dev.begin() + start_offset;
    thrust::device_vector<unsigned int>::iterator particle_indices_local = d_hit_indices.begin() + start_offset;

    V_LOG("Calculating %d source-Particle pair rates ...", end_offset - start_offset);
    calculateSppRates((*data_list[tid]),
                      nSourceParticlePairs,
                      column_H1_SPP_dev_local,
                      column_He1_SPP_dev_local,
                      column_He2_SPP_dev_local,
                      /* This should be the source redshift as seen from the cell/particle.
                       * Hence it will be different for each cell/particle.  This should be
                       * a vector with an element for each source-Particle Pair. */
                      redshift,
                      source[iSource],
                      Constants,
                      particle_indices_local,
                      d_involatiles_list[tid],
                      /* out */
                      rates_list[tid]);

    if (tid == 0) {
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&CheckPointTime, start, stop);
      CodeTimeArray[SPPRates] += CheckPointTime;
    }
    V_LOG(" done\n");
  } /* End of loop over all sources */
  V_LOG("...done loop over sources\n");
}

void calculateIonizationRates(RT_SourceT *const source,
                              size_t const nSource,
                              rt_float const unitLength,
                              rt_float const redshift,
                              size_t const N_rays,
                              int const nDev,
                              double const N_H_per_particle,
                              double const N_He_per_particle,
                              size_t const nParticles,
                              RT_ConstantsT Constants,
                              std::vector<grace::CudaBvh *> &tree_list,
                              std::vector<thrust::host_vector<unsigned int> *> &map_list,
                              std::vector<thrust::device_vector<float> *> &ray_dir_list,
                              std::vector<thrust::device_vector<SphereType> *> &particle_list,
                              std::vector<RT_Data *> &data_list,
                              std::vector<RT_RatesBlockT> &rates_list,
                              std::vector<RT_IntegralInvolatilesT> &involatiles_list,
                              std::vector<RT_IntegralInvolatilesT> &d_involatiles_list,
                              double *CodeTimeArray) {
  const int max_sources_at_once = 32;
  int tid = omp_get_thread_num();
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  float CheckPointTime;
  int n_sources_local = (nSource + nDev - 1) / nDev;  // round up
  int num_sources_at_once, trace_start, num_traces;
  if (n_sources_local != 0) {
    num_sources_at_once = std::min(n_sources_local, max_sources_at_once);
    trace_start = n_sources_local * tid / num_sources_at_once;
    if (tid + 1 == nDev) n_sources_local -= nSource % nDev;
    num_traces = (n_sources_local + num_sources_at_once - 1) / num_sources_at_once;  // round up
  } else {
    num_sources_at_once = 0;
    trace_start = 0;
    num_traces = 0;
  }
  // last device does nSource%nDev sources less

  for (int i_trace = trace_start; i_trace < trace_start + num_traces; i_trace++) {
    V_LOG("Creating Rays...");
    if (tid == 0) cudaEventRecord(start);
    // Create rays from the set of sources i_source_start to i_source_end
    int i_source_start = i_trace * num_sources_at_once;
    int i_source_end = i_source_start + n_sources_local;
    if (i_source_end > nSource) i_source_end = nSource;  // hit the end of the source list
    int num_sources_this_step = i_source_end - i_source_start;
    int n_rays_this_step = num_sources_this_step * N_rays;
    V_LOG("Thread %d on dev %d tracing sources %d to %d\n", tid, devs[tid], i_source_start, i_source_end);

    thrust::device_vector<grace::Ray> d_rays(n_rays_this_step);
#ifdef HEALPIX_RAYS
    generateHealpixRaysFromMultipleSources(d_rays,
                                           N_rays,  // rays per source
                                           num_sources_this_step,
                                           i_source_start,
                                           source,               // source list
                                           rayLength,            // TODO: make this a vector
                                           *ray_dir_list[tid]);  // healpix directions
#else
    if (source[iSource].AttenuationGeometry == RaySphericalWave) {
      ierr = generateRandomRays(d_rays, source[iSource], rayLength);
    } else if (source[iSource].AttenuationGeometry == RayPlaneWave) {
      ierr = generatePlaneParallelRays(d_rays, source[iSource], rayLength);
    }
#endif
    assert(ierr == EXIT_SUCCESS);
    if (tid == 0) {
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&CheckPointTime, start, stop);
      CodeTimeArray[RayCreation] += CheckPointTime;
    }
    V_LOG("done\n");

    V_LOG("Tracing Rays...");
    if (tid == 0) cudaEventRecord(start);
    thrust::device_vector<float> d_kernel_integrals;
    thrust::device_vector<unsigned int> d_hit_indices;
    thrust::device_vector<int> d_ray_offsets(n_rays_this_step);
    thrust::host_vector<unsigned int> particle_indices;
    thrust::host_vector<float> kernel_integrals;
    thrust::host_vector<int> ray_offsets;

    ierr = trace_rays(/* Input, not modified */
                      *particle_list[tid],
                      *tree_list[tid],
                      d_rays,
                      /* Output, used by grace */
                      d_kernel_integrals,
                      d_hit_indices,
                      d_ray_offsets,
                      /* Output, made available to host code */
                      particle_indices,   // unused - edit columnDensities.cu
                      kernel_integrals);  // if you need to use them
    assert(ierr == EXIT_SUCCESS);
    if (tid == 0) {
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&CheckPointTime, start, stop);
      CodeTimeArray[RayTracing] += CheckPointTime;
    }
    ray_offsets = d_ray_offsets;
    V_LOG(" done\n");

    /* particle_indices is ...
     *  Ncol_HeII[i] is the cumulative column density to the particle
     *  with index particle_indices[i] into the sorted list of particles.
     *  The index into the Gadget list is gadget_map[particle_indices[i]]
     *  So,
     *   particle at r[1,gadget_map[particle_indices[i]]], r[2,...], r[3,...]
     *   has total cumulative column density Ncol_HeII[i], but noting that
     * there will be one or more total cumulative column densities to each
     * particle within Ncol_HeII Recall that particle_indices[i] == nParticles
     * (length of Gadget list), indicates the end of a sight line.
     */

    /* Wait for the RT_Data bcast to complete */
    if (nDev > 1) {
      if (sync) {
        double t_start = omp_get_wtime();
        cudaStreamSynchronize(s[tid]);
        sync = false;
        double t_end = omp_get_wtime();
        if (tid == 0) CodeTimeArray[RTDataBcast] += t_end - t_start;
      }
    }
    //    cudaDeviceSynchronize();  // shouldn't be needed but testing to see if it fixed a rare crash
    /* Find column densities to each particle from this source */
    V_LOG("Assigning column densities...");
    if (tid == 0) cudaEventRecord(start);
    /*  The column densities to be filled in */
    thrust::device_vector<float> column_H1_SPP_dev(d_kernel_integrals.size());
    thrust::device_vector<float> column_He1_SPP_dev(d_kernel_integrals.size());
    thrust::device_vector<float> column_He2_SPP_dev(d_kernel_integrals.size());
    V_LOG("Going into ColumDensities.cu\n");
    ierr = columnDensities(/* Input, not modified. Supplied by host code. */
                           N_H_per_particle,
                           N_He_per_particle,
                           unitLength,
                           /* Input, not modified. */
                           (*data_list[tid]).f_H1,   // X_HI_dev,
                           (*data_list[tid]).f_He1,  // X_HeI_dev,
                           (*data_list[tid]).f_He2,  // X_HeII_dev,
                           d_kernel_integrals,
                           d_hit_indices,
                           d_ray_offsets,
                           /* Output, made available to host code */
                           column_H1_SPP_dev,
                           column_He1_SPP_dev,
                           column_He2_SPP_dev);
    V_LOG("Returned from ColumnDensities.cu\n");
    assert(ierr == EXIT_SUCCESS);
    if (tid == 0) {
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&CheckPointTime, start, stop);
      CodeTimeArray[ColumnDensities] += CheckPointTime;
    }
    cudaDeviceSynchronize();
    setMeanColumnDensity(column_H1_SPP_dev, column_He1_SPP_dev, column_He2_SPP_dev, d_hit_indices, (*data_list[tid]));
    cudaDeviceSynchronize();
    V_LOG(" done\n");
    /** Loop Over Sources to get SPP rates **/
    loopOverSources();
    // sum components into G
    V_LOG("Updating heating rates ...");
    if (tid == 0) cudaEventRecord(start);
    updateCumulativeHeatingRate(rates_list[tid]);
    if (tid == 0) {
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&CheckPointTime, start, stop);
      CodeTimeArray[UpdateHeatingRates] += CheckPointTime;
    }
    cudaDeviceSynchronize();
    cudaEventDestroy(start);
    cudaEventDestroy(end);
    V_LOG(" done\n");
  }
}

int loopOverTime(RT_SourceT *const source,
                 size_t const nSource,
                 std::vector<thrust::device_vector<SphereType> *> &particle_list,
                 rt_float const t_start,
                 rt_float const t_stop,
                 rt_float const unitLength,
                 rt_float const redshift,
                 rt_float const boxSize,
                 float rayLength,
                 size_t const N_rays,
                 double const N_H_per_particle,
                 double const N_He_per_particle,
                 RT_ConstantsT Constants,
                 int const nDev,
                 const int *devs,
                 std::vector<grace::CudaBvh *> &tree_list,
                 size_t const nParticles,
                 std::vector<thrust::host_vector<unsigned int> *> &map_list,
#ifdef HEALPIX_RAYS
                 std::vector<thrust::device_vector<float> *> &ray_dir_list,
#endif
                 /* Modified */
                 std::vector<RT_Data *> &data_list,
                 std::vector<RT_RatesBlockT> &rates_list,
/* Optional, not modified */
#ifdef DUMP_RT_DATA
                 rt_float const *const OutputTimes,
                 rt_float const outputTimesUnit,
                 size_t const nOutputTimes,
                 size_t OutputTimeIndex,
                 size_t const DumpIterationCount,
                 char *Dir,
#endif
                 rt_float InitialTemperature) {

  double CodeTimeArray[nCodeTimes];
  double const StartTime = getTimeInSeconds();
  float CheckPointTime;
  double TotalTime = 0.0;
  size_t IterationCount = 0;

  for (size_t i = 0; i < nCodeTimes; ++i) {
    CodeTimeArray[i] = 0.0;
  }

  /* Initialize NCCL communicator and individual device streams */
  ncclComm_t comms[nDev];
  cudaStream_t *s = (cudaStream_t *)malloc(sizeof(cudaStream_t) * nDev);

  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    cudaStreamCreate(s + i);
  }

  // initializing NCCL
  printf("ncclCommInitAll\n");
  NCCLCHECK(ncclCommInitAll(comms, nDev, devs));

  // synchronizing on CUDA streams to wait for completion of NCCL operation
  for (int i = 0; i < nDev; ++i) {
    cudaSetDevice(devs[i]);
    cudaStreamSynchronize(s[i]);
  }

  /* Cuda events to time kernel calls */
  cudaSetDevice(devs[0]);
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  rt_float const ExpansionFactor = 1. / (redshift + 1.);
  rt_float dt_RT;

  int ierr;

  /* Initialize involatile (mostly) variables of the photoionization integral */
  V_LOG("Allocating involatiles...");
  std::vector<RT_IntegralInvolatilesT> involatiles_list(nDev);
  for (int i = 0; i < nDev; i++) {
    if (source[0].SourceType != SourceMonochromatic) {
      ierr = RT_IntegralInvolatiles_allocate(Constants, &involatiles_list[i]);
      assert(ierr == EXIT_SUCCESS);

      ierr = RT_IntegralInvolatiles_setFrequenciesWeightsAlphas(Constants, involatiles_list[i]);
      assert(ierr == EXIT_SUCCESS);

    } else { /* Monochromatic (Test 1) */
      involatiles_list[i].nFrequencies = 1;
      involatiles_list[i].nSpecies = 3;
      involatiles_list[i].Frequencies = (rt_float *)malloc(sizeof(rt_float));
      involatiles_list[i].Frequencies[0] = source[0].nu;
      involatiles_list[i].Weights = (rt_float *)malloc(sizeof(rt_float));
      involatiles_list[i].Weights[0] = 1.0;
      involatiles_list[i].Alphas = (rt_float *)malloc(3 * sizeof(rt_float));
      involatiles_list[i].Alphas[0] = H1_H2(source[0].nu, Constants);
      involatiles_list[i].Alphas[1] = He1_He2(source[0].nu, Constants);
      involatiles_list[i].Alphas[2] = He2_He3(source[0].nu, Constants);
      involatiles_list[i].Luminosities = (rt_float *)malloc(sizeof(rt_float));
    }
  }
  /* Luminosity has yet to be set */
  /* Allocate space on device */
  std::vector<RT_IntegralInvolatilesT> d_involatiles_list(nDev);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    ierr = RT_CUDA_allocateIntegralInvolatilesOnDevice(involatiles_list[i], &d_involatiles_list[i]);
    assert(ierr == EXIT_SUCCESS);
  }
  V_LOG("done\n");

  /* Main loop */
  rt_float t = t_start; /* s */
  unsigned char LastIterationFlag = FALSE;
#pragma omp parallel num_threads(nDev) default(shared)
  {
    int tid = omp_get_thread_num();
    cudaSetDevice(devs[tid]);

    while (!LastIterationFlag) {
      // Broadcast RT_Data from root to all ranks
      if (nDev > 1) {
        NCCLCHECK(ncclBcast((void *)data_list[tid]->MemSpace,
                            nParticles * 12,
                            ncclFloat,
                            0,  // root
                            comms[tid],
                            s[tid]));
      }
      bool sync = true;
      /* Initialize Rates */
      if (tid == 0) {
        V_LOG("Zeroing Rates...");
        cudaEventRecord(start);
      }
      zeroRates(rates_list[tid]);
      if (tid == 0) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[ZeroingRates] += CheckPointTime;
        V_LOG(" done\n");
      }
#pragma omp barrier
      calculateIonizationRates(nSource,
                               unitLength,
                               N_rays,
                               nDev,
                               N_H_per_particle,
                               N_He_per_particle,
                               nParticles,
                               Constants,
                               tree_list,
                               map_list,
                               ray_dir_list,
                               particle_list,
                               data_list,
                               rates_list,
                               &CodeTimeArray[0]);

      t_start = omp_get_w_time();
      if (nDev > 1) reduceRTRates(rates_list, nParticles, &comms[0], s);
      t_end = omp_get_w_time();
      if (tid == 0) CodeTimeArray[RTRatesReduction] += t_end - t_start;
#pragma omp master
      {
        /* Now we have photoionization rates for each particle */

        V_LOG("Recombination rates ...");

        cudaEventRecord(start);
        calculateRecombinationRatesInclCooling(ExpansionFactor, Constants, *data_list[0], rates_list[0]);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[RecombinationRates] += CheckPointTime;

        V_LOG(" done\n");

#ifndef NO_COOLING
        /* Cooling rates */
        V_LOG("Cooling rates ...");
        cudaEventRecord(start);
        RT_CUDA_CoolingRates(ExpansionFactor, Constants, *data_list[0], rates_list[0]);
        /* RT_CUDA does not currently perform error checking */
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[CoolingRates] += CheckPointTime;
        V_LOG(" done\n");
#endif

#ifdef MONITOR_GPU_MEMORY
        printMemory(CudaDeviceTaranis);
#endif

#ifdef COLLISIONAL_IONIZATION
        collisionalIonizationRate(Constants, *data_list[0], rates_list[0]);
#endif

        /* Calculate hydrogen equilibrium values */
        V_LOG("Setting equilibrium fractions ...");
        cudaEventRecord(start);
        RT_CUDA_setHydrogenEquilibriumValues(Constants, *data_list[0], rates_list[0]);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
        CodeTimeArray[SetHydrogenEquilibriumValues] += CheckPointTime;
        V_LOG(" done\n");
        /* Find the largest timestep that allows us to safely integrate through dt
         */
        V_LOG("Finding the timestep...");
        cudaEventRecord(start);
#ifdef DUMP_RT_DATA
        rt_float NextOutputTime;
        if (NULL != OutputTimes) {
          /* NextOutputTime should be set to the next in the list of output times */
          size_t const NumSkipped =
              skipMissedOutputTimes(t / outputTimesUnit, OutputTimes, nOutputTimes, &OutputTimeIndex);
          if (NumSkipped > 0) {
            printf(
                "Skipping %lu Output Times. Next Output time is %5.3e Ma (%5.3e "
                "s)\n",
                NumSkipped,
                OutputTimes[OutputTimeIndex] * (outputTimesUnit / Constants.Ma),
                OutputTimes[OutputTimeIndex] * outputTimesUnit);
          }
          NextOutputTime = FMIN(OutputTimes[OutputTimeIndex] * outputTimesUnit, t_stop);
        } else {
          NextOutputTime = t_stop;
        }
#else
        rt_float NextOutputTime = t_stop;
#endif
        findNextTimestep(nParticles,
                         NextOutputTime,
                         t,
                         *data_list[0],
                         rates_list[0],
                         Constants,
                         /* Output */
                         dt_RT,
                         LastIterationFlag);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
#ifdef DUMP_RT_DATA
        /* Correct if LastIterationFlag was incorrectly set.
         * TODO: LastIterationFlag should be removed from *findNextTimestep. */
        if (nOutputTimes > 0) {
          if (OutputTimeIndex < nOutputTimes - 1) {
            LastIterationFlag = FALSE;
          } else {
            /* If the time at the end of this step is greater than or equal to the
             * last output time, then this is the last iteration. Otherwise we are
             * approaching the last output time but haven't exceeded it. */
            if ((t + dt_RT) >= (OutputTimes[OutputTimeIndex] * outputTimesUnit)) {
              LastIterationFlag = TRUE;
            } else {
              LastIterationFlag = FALSE;
            }
          }
        }
#endif
        /* t_stop is a hard upper limit */
        if ((t + dt_RT) >= t_stop) LastIterationFlag = TRUE;
        CodeTimeArray[FindingTimestep] += CheckPointTime;
        V_LOG(" done\n");
        std::cout << "t= " << t << " (" << t / Constants.Ma << " Ma)"
                  << " dt_RT= " << dt_RT << " (" << dt_RT / Constants.Ma << " Ma)" << std::endl;
#ifdef MONITOR_GPU_MEMORY
        printMemory(CudaDeviceTaranis);
#endif

        // Update the ionization fractions and entropies of the cells/particles
        V_LOG("Updating particles...");
#ifdef DEBUG
        RT_CUDA_RT_Data_CheckData(*data_list[0], __LINE__);
#endif
        cudaEventRecord(start);
        updateParticles(dt_RT,
                        redshift,
                        Constants,
                        rates_list[0],
                        /* Updated */ *data_list[0]);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&CheckPointTime, start, stop);
#ifdef DEBUG
        RT_CUDA_RT_Data_CheckData(*data_list[0], __LINE__);
#endif
        CodeTimeArray[UpdatingParticles] += CheckPointTime;
        V_LOG(" done\n");

        if (source->SourceType == SourceMonochromatic) {
          /* Test 1 is ISOTHERMAL
           * Reset Entropy and Temperature
           * This is a horrible test making a horrible assumption. */
          resetEntropyAndTemperature(InitialTemperature, Constants, *data_list[0]);
        }

        t += dt_RT;
#ifdef DUMP_RT_DATA
        bool DumpRTData = false;
        rt_float const t_outputTimeUnits = t / outputTimesUnit;
        if (NULL != OutputTimes) {
          if (isOutputTime(t_outputTimeUnits, OutputTimes, nOutputTimes, OutputTimeIndex)) {
            DumpRTData = true;
            OutputTimeIndex++;
          }
        } else if (DumpIterationCount != 0 && 0 == (IterationCount % DumpIterationCount)) {
          /* Every nth iteration was requested */
          DumpRTData = true;
        }

        if (DumpRTData) {
          CheckPointTime = getTimeInSeconds();
          cudaDeviceSynchronize();
          CreateSnapshot(*data_list[0], rates_list[0], *map_list[0], redshift, t, outputTimesUnit, Dir);
          V_LOG(" done\n");
          cudaDeviceSynchronize();
          CodeTimeArray[IO] += getTimeInSeconds() - CheckPointTime;
        }
#endif
        IterationCount++;

        cudaDeviceSynchronize();
        double LastIterationTime = getTimeInSeconds() - StartTime - TotalTime;
        TotalTime = getTimeInSeconds() - StartTime;
        std::cout << "Iteration walltime = " << LastIterationTime << " s" << std::endl;
      }  // omp master
#pragma omp barrier
    }                 /* end of loop over time */
  }                   // omp parallel
#pragma omp taskwait  // wait for last snapshot to finish writing

  //  char Dir[] = "save/";
  //  CreateSnapshot(Data_dev, Rates_dev, map_list[0], redshift, t_stop, Constants.Ma, Dir);

  for (size_t i = 0; i < nCodeTimes; ++i) {
    if (i == RTDataBcast || i == RTRatesReduction) continue;  // use omp_get_w_time so already in seconds
    CodeTimeArray[i] *= 1e-3;                                 // convert to seconds
  }
  std::cout << "Walltime                 = " << TotalTime << "s" << std::endl;
  std::cout << "Iteration count          = " << IterationCount << std::endl;
  std::cout << "Time per iteration       = " << TotalTime / (double)IterationCount << "s" << std::endl;
  std::cout << "Zeroing Rates            = " << 100.0 * CodeTimeArray[ZeroingRates] / TotalTime << " %" << std::endl;
  std::cout << "Ray Creation             = " << 100.0 * CodeTimeArray[RayCreation] / TotalTime << " %" << std::endl;
  std::cout << "Ray Tracing              = " << 100.0 * CodeTimeArray[RayTracing] / TotalTime << " %" << std::endl;
  std::cout << "Column Densities         = " << 100.0 * CodeTimeArray[ColumnDensities] / TotalTime << " %" << std::endl;
  std::cout << "Setting Distances        = " << 100.0 * CodeTimeArray[Distances] / TotalTime << " %" << std::endl;
  std::cout << "Involatiles Luminosity   = " << 100.0 * CodeTimeArray[InvolatilesLuminosity] / TotalTime << " %"
            << std::endl;
  std::cout << "source-Particle Rates    = " << 100.0 * CodeTimeArray[SPPRates] / TotalTime << " %" << std::endl;
  std::cout << "Recombination Rates      = " << 100.0 * CodeTimeArray[RecombinationRates] / TotalTime << " %"
            << std::endl;
  std::cout << "Updating Heating Rates   = " << 100.0 * CodeTimeArray[UpdateHeatingRates] / TotalTime << " %"
            << std::endl;
  std::cout << "Cooling Rates            = " << 100.0 * CodeTimeArray[CoolingRates] / TotalTime << " %" << std::endl;
  std::cout << "Set Hydrogen Equilibrium = " << 100.0 * CodeTimeArray[SetHydrogenEquilibriumValues] / TotalTime << " %"
            << std::endl;
  std::cout << "Finding Timestep         = " << 100.0 * CodeTimeArray[FindingTimestep] / TotalTime << " %" << std::endl;
  std::cout << "Updating Particles       = " << 100.0 * CodeTimeArray[UpdatingParticles] / TotalTime << " %"
            << std::endl;
  std::cout << "IO time                  = " << 100.0 * CodeTimeArray[IO] / TotalTime << " %" << std::endl;
  if (nDev > 1) {
    std::cout << "RTData Broadcast wait    = " << 100.0 * CodeTimeArray[RTDataBcast] / TotalTime << " %" << std::endl;
    std::cout << "RTRates Reduction wait   = " << 100.0 * CodeTimeArray[RTRatesReduction] / TotalTime << " %"
              << std::endl;
  }

  double SumKnownTimes = 0.0;
  for (size_t i = 0; i < nCodeTimes; ++i) {
    SumKnownTimes += CodeTimeArray[i];
  }
  std::cout << "Other                    = " << 100.0 * (TotalTime - SumKnownTimes) / TotalTime << " %" << std::endl;

  for (int i = 0; i < nDev; i++) {
    RT_CUDA_freeIntegralInvolatilesOnDevice(d_involatiles_list[i]);
  }

  free(s);

  return EXIT_SUCCESS;
}
