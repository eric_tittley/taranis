#include "Taranis.cuh"

__device__ void
retrieveColumnDensityThroughParticleInSPPorder(size_t const indexIntoSPPList,
                                               RT_Data const Data,
	                                       unsigned int const * const gadget_map,
	                                       unsigned int const * const particle_indices,
					       /* out */
                                               rt_float &N_H1_SPP,
					       rt_float &N_He1_SPP,
					       rt_float &N_He2_SPP		      
                                              ) {
 size_t const nParticles = Data.NumCells;

 /* The mean path length of a ray through a sphere */
 rt_float const meanPathLengthThroughSphere = 4.0/3.0;

 size_t const PI = (size_t) particle_indices[indexIntoSPPList];
 if( PI < nParticles ) {
  size_t const GM = gadget_map[PI];
  N_H1_SPP  = Data.n_H[GM]  * Data.f_H1[GM]  * meanPathLengthThroughSphere * Data.dR[GM];
  N_He1_SPP = Data.n_He[GM] * Data.f_He1[GM] * meanPathLengthThroughSphere * Data.dR[GM];
  N_He2_SPP = Data.n_He[GM] * Data.f_He2[GM] * meanPathLengthThroughSphere * Data.dR[GM];
 #ifdef DEBUG
  if( (N_H1_SPP < 0.0) || !isfinite(N_H1_SPP) ) {
   printf("ERROR: %s: %i: N_H1_SPP=%5.3e; n_H[%lu]=%5.3e; f_H1=%5.3e; dR=%5.3e\n",
          __FILE__,__LINE__,N_H1_SPP,GM,Data.n_H[GM],Data.f_H1[GM],Data.dR[GM]);
   return;
  }
 #endif
 } else {
  /* Index points to an End-Of-Ray marker */
  N_H1_SPP  = 1.;
  N_He1_SPP = 1.;
  N_He2_SPP = 1.;
 }
}
