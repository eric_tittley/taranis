#include "ERT_gizmo.h"
#include "Taranis.h"
#include "Taranis.cuh"
#include "taranis_vars.h"
#include "RT.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

#include "allvars.h"

char ert_DumpFlag = 0;
int ert_snapshot_num = 0;

#if 1
void ert_write_snapshot(int snapshot_num) {return;}
#else /* Disable until updated to match Manitou's method */
int CreateSnapshotGizmo(RT_Data &Data_src,
                        RT_RatesBlockT Rates_dev,
                        thrust::host_vector<unsigned int> &gadget_map,
                        rt_float Redshift,
                        rt_float t,
                        rt_float outputTimesUnit,
                        char * Dir){

 printf("Writing Snapshot...\n");
 rt_float const ExpansionFactor = 1./(Redshift+1.);
 size_t const LABEL_STR_SIZE=32;
 char RTFile[LABEL_STR_SIZE];
 rt_float const t_outputTimeUnits = t/outputTimesUnit;

 size_t nParticles = Data_src.NumCells;
 RT_Data * Data_tmp = RT_Data_Allocate(nParticles);

 #pragma omp parallel for
  for(size_t i=0; i<nParticles; i++){
    size_t index = gadget_map[i];

    Data_tmp->R[index]       = Data_src.R[i];
    Data_tmp->dR[index]      = Data_src.dR[i];
    Data_tmp->Density[index] = Data_src.Density[i];
    Data_tmp->Entropy[index] = Data_src.Entropy[i];
    Data_tmp->T[index]       = Data_src.T[i];

    Data_tmp->n_H[index]   = Data_src.n_H[i];
    Data_tmp->f_H1[index]  = Data_src.f_H1[i];
    Data_tmp->f_H2[index]  = Data_src.f_H2[i];
    Data_tmp->f_He1[index] = Data_src.f_He1[i];
    Data_tmp->f_He2[index] = Data_src.f_He2[i];
    Data_tmp->f_He3[index] = Data_src.f_He3[i];

    Data_tmp->column_H1[index]  = Data_src.column_H1[i];
    Data_tmp->column_He1[index] = Data_src.column_He1[i];
    Data_tmp->column_He2[index] = Data_src.column_He2[i];
  }

 /* Dump data */
#if VERBOSE > 1
 std::cout << "Saving data...";
#endif
 int ierr;
 ierr = snprintf(RTFile, LABEL_STR_SIZE-1, "%s/RTData_t=%07.3f", Dir, t_outputTimeUnits);
 if(ierr>(int)LABEL_STR_SIZE) {
  printf("ERROR: %s: %i: Path too long %s\n",__FILE__,__LINE__,RTFile);
  return EXIT_FAILURE;
 }
 RT_FileHeaderT FileHeader;
 FileHeader.ExpansionFactor = (float)ExpansionFactor;
 FileHeader.Redshift        = (float)Redshift;
 FileHeader.Time            = (float)t;
 RT_Data *PointerToRT_Data = Data_tmp;
 RT_Data **PointerToArrayOfPointersToRT_Data = &PointerToRT_Data;
 RT_SaveData(RTFile, PointerToArrayOfPointersToRT_Data, 1, FileHeader);

 /* * Save Rates to file * */
 /*   Allocate host memory for Rates   */
 RT_RatesBlockT Rates_host;
 Rates_host.NumCells=Data_src.NumCells;
 ierr = RT_RatesBlock_Allocate(Data_src.NumCells,&Rates_host);
 if(ierr!=EXIT_SUCCESS) {
  printf("ERROR: %s: %i: Failed to allocate host memory for Rates\n",
         __FILE__,__LINE__);
  return EXIT_FAILURE;
 }
 /*   Copy Rates from device to host   */

 RT_CUDA_CopyRatesDeviceToHost(Rates_dev,Rates_host);
 sortRatesIntoGadgetOrder(&Rates_host, gadget_map);
 /*   Write Rates to file              */
 ierr = snprintf(RTFile, LABEL_STR_SIZE-1, "%s/Rates_t=%07.3f", Dir, t_outputTimeUnits);
 if(ierr>(int)LABEL_STR_SIZE) {
  printf("ERROR: %s: %i: Path too long %s\n",__FILE__,__LINE__,RTFile);
  return EXIT_FAILURE;
 }
 FILE *fid = fopen(RTFile, "w");
 if (fid == NULL) {
  printf("ERROR: %s: %i: Unable to open file %s\n", __FILE__, __LINE__, RTFile);
  ReportFileError(RTFile);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
 ierr = RT_RatesBlock_Write(Rates_host,fid);
 if(ierr!=EXIT_SUCCESS) {
  printf("ERROR: %s: %i: Failed to write Rates to file %s\n",
        __FILE__,__LINE__,RTFile);
  return EXIT_FAILURE;
 }
 /* Close the file */
 ierr = fclose(fid);
 if (ierr != 0) {
  printf("ERROR: %s: Unable to close file %s\n", __FILE__, RTFile);
  ReportFileError(RTFile);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }

 /*   Free local Rates memory          */
 RT_RatesBlock_Free(Rates_host);
 printf("Finished writing Snapshot\n");
 return EXIT_SUCCESS;
}

void ert_write_snapshot(){
 rt_float Redshift = 1.0/(All.Time) -1;
 rt_float t = All.Time*All.UnitTime_in_s;
 char Dir[] = "./save";
 rt_float outputTimesUnit = 3.15576e13;

 int ierr = CreateSnapshotGizmo(_Data_old,
                                _Rates_dev,
                                _gadget_map,
                                Redshift,
                                t,
                                outputTimesUnit,
                                Dir);


}
#endif /* 0 */