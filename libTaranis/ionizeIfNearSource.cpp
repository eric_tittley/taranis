#include "Taranis.h"

#define TARANIS_IONIZED_WITHIN_HSML_FACTOR 2.0

#define TARANIS_MIN_fH1 1e-5
#define TARANIS_MIN_fHe1 1e-5

void ionizeIfNearSource(double const px, double const py, double const pz,
                        double const hsml,
                        RT_SourceT const * const Source,
                        size_t const nSources,
                        rt_float * const IonizationFractions)
{
 for(size_t iSource=0;iSource<nSources;++iSource) {
  double d2 = ((px - Source[iSource].r[0]) * (px - Source[iSource].r[0])
             + (py - Source[iSource].r[1]) * (py - Source[iSource].r[1])
             + (pz - Source[iSource].r[2]) * (pz - Source[iSource].r[2]));
  double threshold = TARANIS_IONIZED_WITHIN_HSML_FACTOR * hsml
                   * TARANIS_IONIZED_WITHIN_HSML_FACTOR * hsml;
  if (d2 < threshold) {
   IonizationFractions[0] = TARANIS_MIN_fH1;
   IonizationFractions[1] = (rt_float)1 - IonizationFractions[0];
   // FIXME: How do we want to divide the ionization between fHe2 and fHe3?
   //        Probably *not* fHe2 ~ 1, fHe3 = 0, as here.
   IonizationFractions[2] = TARANIS_MIN_fHe1;
   IonizationFractions[3] = (rt_float)1 - IonizationFractions[2];
   IonizationFractions[4] = 0;

   // The modification to IonizationFractions is identical for any close-enough
   // source.
   break;
  }
 }
}
