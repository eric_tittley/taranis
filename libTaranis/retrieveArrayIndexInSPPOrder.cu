/* retrieveArrayIndexInSPPOrder: Returns the index into the host-sorted Data
 *                               given the grace column density index.
 * 
 * Recall that the grace column densities are a single vector comprised of the
 * contantenation of the source-particle cumulative column density along each
 * ray.
 *
 * The particle index along the vector is stored in particle_indices, but it is
 * in grace particle order, not the input.
 */

#include "Taranis.cuh"

__device__ size_t
retrieveArrayIndexInSPPOrder(size_t const i, /* Index in SPP order */
			     size_t const nParticles,
                             unsigned int const * const particle_indices,
                             unsigned int const * const gadget_map,
                             size_t const FillValueForEoRMarker ) {
 size_t ElementIndex;

 size_t const PI = (size_t) particle_indices[i];
 if( PI < nParticles ) {
  ElementIndex = gadget_map[PI];
 } else {
  /* Index points to an End-Of-Ray marker */
  ElementIndex = FillValueForEoRMarker;
 }
 return ElementIndex;
}
