/* updateIonizationFraction: Update the ionization fractions on the Grace CUDA
 *                           device with values from the RT CUDA device.
 *
 */
#include "cuda.h"

#include <thrust/host_vector.h>

#include "Taranis.h"
#include "Taranis.cuh"

void
uIF_CopyDeviceToDevice(size_t const nParticles,
                       thrust::host_vector<unsigned int> &gadget_map,
                       rt_float *X_dev_RT,
                       thrust::device_vector<float> &X_dev_GRACE) {

 /* Cast a double pointer to a thrust device double pointer */
 thrust::device_ptr<rt_float> dev_ptr(X_dev_RT);

 /* Allocate Host memory */
 rt_float *X_RT_order = (rt_float *) malloc(nParticles*sizeof(rt_float));

 /* Copy the contents of X_dev_RT from the device to the host */
 cudaMemcpy(X_RT_order,
            X_dev_RT,
            nParticles*sizeof(rt_float),
            cudaMemcpyDeviceToHost);

 /* Initialise host memory for the GRACE-order fractions */
 thrust::host_vector<float> X_Grace_order(X_dev_GRACE.size());

 /* Copy in GRACE order, casting from double to float on the fly. */
 #pragma omp parallel for
 for(size_t iParticle=0;iParticle<nParticles;++iParticle) {
  X_Grace_order[iParticle] = (float)X_RT_order[gadget_map[iParticle]];
 }

 free(X_RT_order);

 /* Copy from host memory to GRACE device memory */
 X_dev_GRACE = X_Grace_order;

}
			     

void
updateIonizationFractions(/* Input, not modified */
                          RT_Data const Data_dev_RT,
                          thrust::host_vector<unsigned int> &gadget_map,
                          /* Input, modified */
                          thrust::device_vector<float> &X_HI_dev_Grace,
                          thrust::device_vector<float> &X_HeI_dev_Grace,
                          thrust::device_vector<float> &X_HeII_dev_Grace) {

 const size_t nParticles=Data_dev_RT.NumCells;

 /* Copy the ionization fractions from the RT device to host memory,
  * resorting in host memory. */

 uIF_CopyDeviceToDevice(nParticles, gadget_map,
                        Data_dev_RT.f_H1,  X_HI_dev_Grace  );

 uIF_CopyDeviceToDevice(nParticles, gadget_map,
                        Data_dev_RT.f_He1, X_HeI_dev_Grace );

 uIF_CopyDeviceToDevice(nParticles, gadget_map,
                        Data_dev_RT.f_He2, X_HeII_dev_Grace);
			     

}
