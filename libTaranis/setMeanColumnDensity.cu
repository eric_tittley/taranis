#include <thrust/device_vector.h>

#include "Taranis.h"
#include "Taranis.cuh"

#include "RT_CUDA.h"

#include "cudasupport.h"

__global__ void
sMCD_sumColumnDensities(size_t const nSourceParticlePairs,
                        float const * const column_H1,
                        float const * const column_He1,
                        float const * const column_He2,
                        unsigned int const * const particle_indices,
                        RT_Data const Data,
                        unsigned int * const count
                       ) {

 size_t const nParticles = Data.NumCells;
 size_t const istart = blockIdx.x*blockDim.x+threadIdx.x;
 size_t const span   = gridDim.x * blockDim.x; // nBlocks * nThreadsPerBlock

 /* nSourceParticlePairs - 1 so we skip the final dummy particle.
  * This ensures that column_Hx[cellindex + 1] is always a valid address.
  */
 for( size_t cellindex=istart; cellindex < nSourceParticlePairs - 1; cellindex+=span ) {
  size_t const iParticle = particle_indices[cellindex];
  if(iParticle < nParticles ) {
   atomicAdd(&(Data.column_H1[iParticle]),  (rt_float)column_H1[cellindex]);
   atomicAdd(&(Data.column_He1[iParticle]), (rt_float)column_He1[cellindex]);
   atomicAdd(&(Data.column_He2[iParticle]), (rt_float)column_He2[cellindex]);
   atomicAdd(&(count[iParticle]), 1);
  }

 }

}

__global__ void
sMCD_normalizeColumnDensities(size_t const nParticles,
                              rt_float * const X,
                              unsigned int const * const count) {
 size_t const istart = blockIdx.x*blockDim.x+threadIdx.x;
 size_t const span   = gridDim.x * blockDim.x; // nBlocks * nThreadsPerBlock
 for( size_t cellindex=istart; cellindex < nParticles; cellindex+=span ) {
  if(count[cellindex]>0) X[cellindex] /= (rt_float)count[cellindex];
 }

}

void
setMeanColumnDensity(thrust::device_vector<float> &column_H1,
                     thrust::device_vector<float> &column_He1,
                     thrust::device_vector<float> &column_He2,
                     thrust::device_vector<unsigned int> &particle_indices,
                     RT_Data const Data_dev
                    ) {
 size_t const nSourceParticlePairs = column_H1.size();
 size_t const nParticles           = Data_dev.NumCells;

 cudaError_t error;


 /* Set column densities to zero */
 error = cudaMemsetAsync(Data_dev.column_H1,  0, nParticles*sizeof(rt_float));
 checkCudaAPICall(error,__FILE__, __LINE__, true);
 error = cudaMemsetAsync(Data_dev.column_He1, 0, nParticles*sizeof(rt_float));
 checkCudaAPICall(error,__FILE__, __LINE__, true);
 error = cudaMemsetAsync(Data_dev.column_He2, 0, nParticles*sizeof(rt_float));
 checkCudaAPICall(error,__FILE__, __LINE__, true);

 /* Allocate count on device */
 unsigned int * count;
 error = cudaMalloc(&count, nParticles*sizeof(unsigned int));
 checkCudaAPICall(error,__FILE__, __LINE__, true);
 error = cudaMemsetAsync(count, 0, nParticles*sizeof(unsigned int));
 checkCudaAPICall(error,__FILE__, __LINE__, true);

 error=cudaDeviceSynchronize();
 checkCudaAPICall(error,__FILE__, __LINE__, true);

 int gridSize;       // The actual grid size needed, based on input
                     // size
 int blockSize;      // The launch configurator returned block size
 setGridAndBlockSize(nSourceParticlePairs, (void *)sMCD_sumColumnDensities,
                     &gridSize, &blockSize);

 checkCudaKernelLaunch(__FILE__, __LINE__);

 sMCD_sumColumnDensities<<<gridSize, blockSize>>>
                        (nSourceParticlePairs,
                         column_H1.data().get(),
                         column_He1.data().get(),
                         column_He2.data().get(),
                         particle_indices.data().get(),
                         Data_dev,
                         count);
 checkCudaKernelLaunch(__FILE__, __LINE__);

 setGridAndBlockSize(nSourceParticlePairs, (void *)sMCD_normalizeColumnDensities,
                     &gridSize, &blockSize);
 sMCD_normalizeColumnDensities<<<gridSize, blockSize>>>
                              (nParticles,Data_dev.column_H1,count);
 sMCD_normalizeColumnDensities<<<gridSize, blockSize>>>
                              (nParticles,Data_dev.column_He1,count);
 sMCD_normalizeColumnDensities<<<gridSize, blockSize>>>
                              (nParticles,Data_dev.column_He2,count);
 checkCudaKernelLaunch(__FILE__, __LINE__);

 cudaFree(count);
}
