#include <Taranis.h>
#include <Taranis.cuh>

void
sortRatesIntoGadgetOrder(RT_RatesBlockT * const Rates_src,
                         thrust::host_vector<unsigned int> gadget_map) {

  size_t nParticles = Rates_src->NumCells;

  RT_RatesBlockT Rates_tmp;
  RT_RatesBlock_Allocate(nParticles, &Rates_tmp);

 #pragma omp parallel for
  for(size_t i=0; i<nParticles; i++){
    size_t index = gadget_map[i];

    Rates_tmp.I_H1[index]   = Rates_src->I_H1[i];
    Rates_tmp.I_He1[index]  = Rates_src->I_He1[i];
    Rates_tmp.I_He2[index]  = Rates_src->I_He2[i];
    Rates_tmp.G[index]      = Rates_src->G[i];
    Rates_tmp.G_H1[index]   = Rates_src->G_H1[i];
    Rates_tmp.G_He1[index]  = Rates_src->G_He1[i];
    Rates_tmp.G_He2[index]  = Rates_src->G_He2[i];

    Rates_tmp.alpha_H1[index]  = Rates_src->alpha_H1[i];
    Rates_tmp.alpha_He1[index] = Rates_src->alpha_He1[i];
    Rates_tmp.alpha_He2[index] = Rates_src->alpha_He2[i];
    Rates_tmp.L[index]      = Rates_src->L[i];

    Rates_tmp.Gamma_HI[index]         = Rates_src->Gamma_HI[i];
    Rates_tmp.n_HI_Equilibrium[index] = Rates_src->n_HI_Equilibrium[i];
    Rates_tmp.n_HII_Equilibrium[index] = Rates_src->n_HII_Equilibrium[i];
    Rates_tmp.TimeScale[index]        = Rates_src->TimeScale[i];
  }

  /* Rates_tmp.MemSpace now points to the correctly sorted data.
   * and Rates_tmp.<Vectors> are pointers to the correct space.
   * Need to free Rates_src's memory and set Rates_src to Rates_tmp's Memspace
   * then set Rates_src's pointers.
   * Finally, free Rates_tmp (but not Rates_tmp.MemSpace)
   */
  free(Rates_src->I_H1);
  free(Rates_src->I_He1);
  free(Rates_src->I_He2);
  free(Rates_src->G);
  free(Rates_src->G_H1);
  free(Rates_src->G_He1);
  free(Rates_src->G_He2);
  free(Rates_src->alpha_H1);
  free(Rates_src->alpha_He1);
  free(Rates_src->alpha_He2);
  free(Rates_src->L);
  free(Rates_src->Gamma_HI);
  free(Rates_src->n_HI_Equilibrium);
  free(Rates_src->n_HII_Equilibrium);
  free(Rates_src->TimeScale);

  Rates_src->I_H1      = Rates_tmp.I_H1;
  Rates_src->I_He1     = Rates_tmp.I_He1;
  Rates_src->I_He2     = Rates_tmp.I_He2;
  Rates_src->G         = Rates_tmp.G;
  Rates_src->G_H1      = Rates_tmp.G_H1;
  Rates_src->G_He1     = Rates_tmp.G_He1;
  Rates_src->G_He2     = Rates_tmp.G_He2;
  Rates_src->alpha_H1  = Rates_tmp.alpha_H1;
  Rates_src->alpha_He1 = Rates_tmp.alpha_He1;
  Rates_src->alpha_He2 = Rates_tmp.alpha_He2;
  Rates_src->L         = Rates_tmp.L;
  Rates_src->Gamma_HI  = Rates_tmp.Gamma_HI;
  Rates_src->n_HI_Equilibrium  = Rates_tmp.n_HI_Equilibrium;
  Rates_src->n_HII_Equilibrium = Rates_tmp.n_HII_Equilibrium;
  Rates_src->TimeScale = Rates_tmp.TimeScale;

}
