#include "Taranis.h"
#include "Taranis.cuh"
#include "RT.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

#include <thrust/host_vector.h>

#include <omp.h>

/* Dumps RT data to a file */

int CreateSnapshot(RT_Data Data_dev,
                   RT_RatesBlockT Rates_dev,
                   thrust::host_vector<unsigned int> &gadget_map,
                   rt_float Redshift,
                   rt_float t,
                   rt_float outputTimesUnit,
                   char * Dir){
 #pragma omp taskwait
 printf("Writing Snapshot...\n");
 int ierr;
 // Copy data from device
 RT_Data * Data_host_p = RT_Data_Allocate(gadget_map.size());
 RT_Data Data_host = *Data_host_p;
 cudaError_t cudaError = cudaMemcpy( Data_host.MemSpace,
                         Data_dev.MemSpace,
                         Data_host.NBytes,
                         cudaMemcpyDeviceToHost );
 checkCudaAPICall(cudaError, __FILE__, __LINE__);

 /*   Allocate host memory for Rates   */
 RT_RatesBlockT Rates_host;
 Rates_host.NumCells=Data_host.NumCells;
 ierr = RT_RatesBlock_Allocate(Data_host.NumCells,&Rates_host);
 if(ierr!=EXIT_SUCCESS) {
  printf("ERROR: %s: %i: Failed to allocate host memory for Rates\n",
         __FILE__,__LINE__);
  return EXIT_FAILURE;
 }
 /*   Copy Rates from device to host   */
 RT_CUDA_CopyRatesDeviceToHost(Rates_dev,Rates_host);
 //shared vars should be private but they don't change outside of this function
 //and we don't want to make copies of them (they could be huge!)
 //private vars need to be copied, loopOverTime can change them while writing a file.
 #pragma omp task shared(gadget_map) firstprivate(Data_host_p, Redshift, t, outputTimesUnit, Dir)
 {
  Data_host = *Data_host_p; //Data_host as shared and then using that doesn't work?!?!
  rt_float const ExpansionFactor = 1./(Redshift+1.);
  size_t const LABEL_STR_SIZE=32;
  char RTFile[LABEL_STR_SIZE];
  rt_float const t_outputTimeUnits = t/outputTimesUnit;
  sortRTDataIntoGadgetOrder(&Data_host, gadget_map);

  /* Dump data */
 #if VERBOSE > 1
  std::cout << "Saving data...";
 #endif
  ierr = snprintf(RTFile, LABEL_STR_SIZE-1, "%s/RTData_t=%07.3f", Dir, t_outputTimeUnits);
  if(ierr>(int)LABEL_STR_SIZE) {
   printf("ERROR: %s: %i: Path too long %s\n",__FILE__,__LINE__,RTFile);
   //return EXIT_FAILURE;
  }
  RT_FileHeaderT FileHeader;
  FileHeader.ExpansionFactor = (float)ExpansionFactor;
  FileHeader.Redshift        = (float)Redshift;
  FileHeader.Time            = (float)t;
  RT_Data *PointerToRT_Data = &Data_host;
  RT_Data **PointerToArrayOfPointersToRT_Data = &PointerToRT_Data;
  RT_SaveData(RTFile, PointerToArrayOfPointersToRT_Data, 1, FileHeader);

  /* * Save Rates to file * */
  sortRatesIntoGadgetOrder(&Rates_host, gadget_map);
  /*   Write Rates to file              */
  ierr = snprintf(RTFile, LABEL_STR_SIZE-1, "%s/Rates_t=%07.3f", Dir, t_outputTimeUnits);
  if(ierr>(int)LABEL_STR_SIZE) {
   printf("ERROR: %s: %i: Path too long %s\n",__FILE__,__LINE__,RTFile);
   //return EXIT_FAILURE;
  }
  FILE *fid = fopen(RTFile, "w");
  if (fid == NULL) {
   printf("ERROR: %s: %i: Unable to open file %s\n", __FILE__, __LINE__, RTFile);
   ReportFileError(RTFile);
   (void)fflush(stdout);
   //return EXIT_FAILURE;
  }
  ierr = RT_RatesBlock_Write(Rates_host,fid);
  if(ierr!=EXIT_SUCCESS) {
   printf("ERROR: %s: %i: Failed to write Rates to file %s\n",
         __FILE__,__LINE__,RTFile);
   //return EXIT_FAILURE;
  }
  /* Close the file */
  ierr = fclose(fid);
  if (ierr != 0) {
   printf("ERROR: %s: Unable to close file %s\n", __FILE__, RTFile);
   ReportFileError(RTFile);
   (void)fflush(stdout);
   //return EXIT_FAILURE;
  }

  /*   Free local Rates memory          */
  //RT_Data_Free(&Data_host);
  free(Data_host.MemSpace);
  free(Data_host_p);
  RT_RatesBlock_Free(Rates_host);
  printf("Finished writing Snapshot\n");
 }//omp task 
 return EXIT_SUCCESS;
}
