#include <cstdlib>
#include <cmath>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <vector_types.h>

#include "Taranis.h"
#include "Taranis.cuh"

/* Spherical wave source. */
__global__ void
setSWSTPD_kernel(SphereType *d_particles,
                 float const r0,
                 float const r1,
                 float const r2,
                 rt_float const UnitLength,
                 RT_Data Data_dev) {

 size_t const istart = blockIdx.x*blockDim.x+threadIdx.x;
 size_t const span   = gridDim.x * blockDim.x; // nBlocks * nThreadsPerBlock

 for( size_t iParticle = istart; iParticle < Data_dev.NumCells; iParticle+=span ) {
 /* Set the distance from the source to each particle */
 /* The distance to the source [m] */
  Data_dev.R[iParticle] = (rt_float)
      sqrtf( (d_particles[iParticle].x-r0)*(d_particles[iParticle].x-r0)
            +(d_particles[iParticle].y-r1)*(d_particles[iParticle].y-r1)
            +(d_particles[iParticle].z-r2)*(d_particles[iParticle].z-r2) )
         * UnitLength;
 }

}

/* Plane wave source. */
__global__ void
setPWSTPD_kernel(SphereType *d_particles,
                 float const dir0,
	               float const dir1,
	               float const dir2,
                 rt_float const BoxSize,
                 rt_float const UnitLength,
                 /* The distance to the source [m] */
	               RT_Data Data_dev) {

 size_t const istart = blockIdx.x*blockDim.x+threadIdx.x;
 size_t const span   = gridDim.x * blockDim.x; // nBlocks * nThreadsPerBlock

  /* Set the distance from the source to each particle */

 if (dir0 != 0) {
  /* Rays along x axis. +ve x means origin at x = 0 plane; -ve x means origin
   * at x = BoxSize plane.
   */
  float origin_x = dir0 > 0 ? 0 : BoxSize;
  for( size_t iParticle = istart; iParticle < Data_dev.NumCells; iParticle+=span ) {
   Data_dev.R[iParticle] = (rt_float)(abs(d_particles[iParticle].x - origin_x)
                                      * UnitLength);
  }
 }

 else if (dir1 != 0) {
  /* Rays along y axis. +ve y means origin at y = 0 plane; -ve y means origin
   * at y = BoxSize plane.
   */
  float origin_y = dir1 > 0 ? 0 : BoxSize;
  for( size_t iParticle = istart; iParticle < Data_dev.NumCells; iParticle+=span ) {
   Data_dev.R[iParticle] = (rt_float)(abs(d_particles[iParticle].y - origin_y)
                                      * UnitLength);
  }
 }

 else if (dir2 != 0) {
  /* Rays along z axis. +ve z means origin at z = 0 plane; -ve z means origin
   * at z = BoxSize plane.
   */
  float origin_z = dir0 > 0 ? 0 : BoxSize;
  for( size_t iParticle = istart; iParticle < Data_dev.NumCells; iParticle+=span ) {
   Data_dev.R[iParticle] = (rt_float)(abs(d_particles[iParticle].z - origin_z)
                                      * UnitLength);
  }
 }

}


void
setSourceToParticleDistance(thrust::device_vector<SphereType> &d_particles,
                            RT_SourceT const &Source,
                            /* Same units as h_particles. */
                            rt_float const BoxSize,
                            rt_float const UnitLength,
                            RT_Data Data_dev) {

 int gridSize;       // The actual grid size needed, based on input
 		     // size
 int blockSize;      // The launch configurator returned block size

 if (Source.AttenuationGeometry == SphericalWave ||
     Source.AttenuationGeometry == RaySphericalWave) {

  setGridAndBlockSize(Data_dev.NumCells, (void *)setSWSTPD_kernel,
                      &gridSize, &blockSize);
  setSWSTPD_kernel<<<gridSize, blockSize>>>(
   d_particles.data().get(),
   (float)Source.r[0],
   (float)Source.r[1],
   (float)Source.r[2],
   UnitLength,
   Data_dev);
  checkCudaKernelLaunch(__FILE__, __LINE__);
 }
 else if (Source.AttenuationGeometry == PlaneWave ||
          Source.AttenuationGeometry == RayPlaneWave) {

  setGridAndBlockSize(Data_dev.NumCells, (void *)setPWSTPD_kernel,
                      &gridSize, &blockSize);
  setPWSTPD_kernel<<<gridSize, blockSize>>>(
   d_particles.data().get(),
   (float)Source.r[0],
   (float)Source.r[1],
   (float)Source.r[2],
   BoxSize,
   UnitLength,
   Data_dev);
  checkCudaKernelLaunch(__FILE__, __LINE__);
 }
}
