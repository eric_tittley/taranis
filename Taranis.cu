/* Compile-time configuration */
#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <cstdlib>
#include <fstream>

#ifdef DEBUG
#  include <cstdio>
#endif

#include <cmath>
#include <string>

/* For getopt */
#include <unistd.h>

/* For mkdir() */
#include <cuda_runtime.h>
#include <sys/stat.h>

/* Turn off thrust debugging. Needed because RT uses DEBUG for debugging. */
#define NDEBUG
#include <thrust/host_vector.h>

#include "RT_Precision.h"

extern "C" {
/* RT includes */
#include "RT.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"

/* Gadget2 includes */
#include "allvars.h"
#include "proto.h"
/* Declares:
 *  header Header info
 *  P      Particle data
 *  SphP   Extra SPH particle data
 */
}
#include "RT_ConstantsT.h"
#include "RT_FileHeaderT.h"
#include "RT_SourceT.h"
#include "cudasupport.h"

/* Grace */
#include "grace/types.h"

/* Local */
#include "Manidoowag.h"
#include "Parameters.h"
#include "Taranis.h"

int main(int argc, char **argv) {
  int ierr;

  char const *RestartOptionString = "r:";
  char *RestartFile = NULL;
  int optind;
  while ((optind = getopt(argc, argv, RestartOptionString)) != EOF) {
    switch (optind) {
      case 'r':
        RestartFile = optarg;
        break;
      default:
        exit(EXIT_FAILURE);
    }
  }

  /* check if the data dump folder exists */
  mkdir("save", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  /* Initialize run parameters. */
  char const *const ParametersFile = "Taranis.params";
  ParametersT Parameters;
  ierr = readParameters(ParametersFile, &Parameters);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Parameters file.";
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }

#ifndef NO_COOLING
  if (Parameters.ProblemType == 1) {
    std::cout << "ERROR: If you want to run the Test 1, you want to set NO_COOLING";
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }
#endif

  /* Report on GPU available and being used */
  int nDev = 0;
  cudaGetDeviceCount(&nDev);
  if (nDev != 1) {
    std::cerr << __FILE__ ": " << __LINE__ << ": "
              << "ERROR: nDev=" << nDev << std::endl
              << "       Check environment variable CUDA_VISIBLE_DEVICES is set correctly." << std::endl;
    exit(EXIT_FAILURE);
  }
  std::cout << "Using " << nDev << " device" << std::endl;
  cudaSetDevice(0);
  std::cout << "Properties of assigned CUDA device." << std::endl
            << "If this is not the expected device, check environment variable CUDA_VISIBLE_DEVICES is set correctly."
            << std::endl;
  RT_CUDA_Util_PrintDeviceProperties(0);

  /* Read in the Constants file */
  char const *const ConstantsFile = "Constants.params";
  RT_ConstantsT Constants;
  ierr = RT_ReadConstantsFile(ConstantsFile, &Constants);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Constants file.";
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }

  /* Read in the output times, unless an every-nth-iteration has been specified */
  rt_float *OutputTimes = NULL;
  size_t nOutputTimes = 0;
  rt_float outputTimesUnit; /* To convert outputTimes[i] to s */
  if (Parameters.OutputIterationCount == 0) {
    nOutputTimes = readOutputTimes(Parameters.OutputTimesFile, outputTimesUnit, OutputTimes);
    if (nOutputTimes == 0) {
      std::cout << "ERROR: " << __FILE__ << ": " << __LINE__ << std::endl;
      std::cout << " Unable to read output times file: ";
      std::cout << Parameters.OutputTimesFile << std::endl;
      std::cout << "  Check file exists and is correctly formatted." << std::endl;
      exit(EXIT_FAILURE);
    }
  }

  /* Load Data */
  std::cout << "Loading data...";
  ThisTask = 0;
  All.ICFormat = 1;
  RestartFlag = 1;
  /* read_file expects MPI, so we will need to fake an MPI environment */
  NTask = 1;                 /* Gadget2 global */
  All.PartAllocFactor = 1.0; /* The factor for allocating memory for particle data. */
  All.BufferSize = 1024.0;   /* The size (in MByte per processor) of a communication buffer. */
  if (!(CommBuffer = malloc(All.BufferSize * 1024 * 1024))) {
    printf("failed to allocate memory for `CommBuffer' (%i MB).\n", All.BufferSize);
    exit(EXIT_FAILURE);
  }
  int readTask = 0;
  int lastTask = 0;
  read_file(Parameters.GadgetFile, readTask, lastTask);
  free(CommBuffer);
  std::cout << " done" << std::endl;
  /* Now set:
   *  header.npart[6]
   *        .mass[6]
   *        .time
   *        .redshift
   *        .flag_sfr
   *        .flag_feedback
   *        .npartTotal[6]
   *        .flag_cooling
   *        .num_files
   *        .BoxSize
   *        .Omega0
   *        .OmegaLambda
   *        .HubbleParam
   *        .flag_stellarage
   *        .flag_metals
   *        .npartTotalHighWord[6]
   *        .flag_entropy_instead_u
   *  P.Pos[3]
   *   .Mass
   *   .Vel[3]
   *   .GravAccel[3]
   *   .Potential
   *   .ID
   *   .Type
   *
   * SphP
   *   .Entropy
   *   .Density**
   *   .Hsml**
   * ** = used
   */

  if (Parameters.ProblemType == 1 || Parameters.ProblemType == 2) {
    /* Test 1 or 2 */
    if (header.redshift > 0.0) {
      printf("Setting redshift to 0\n");
      header.redshift = 0.0;
    }
  }

  size_t const nParticles = (size_t)header.npart[0]; /* Gas only */

  /* There are two ways the RT library can package data:  Arrays of structures
   * or structures of arrays.  Arrays of structures are good for OpenMP and
   * OpenMPI, where having all the data for a cell close together in memory is
   * good.  Structures of arrays are good for vectorization, but only up to
   * array lengths that can be fit in the computing vector length (2 doubles
   * or so for SIMD and threads per warp for GPU computing.
   * Arrays of structures are easily produced by:
   *  Data_hosts = (RT_Cell *)  malloc(nParticles*sizeof(RT_Cell));
   * A single structure of arrays is produced by:
   *  Data = RT_Data_Allocate(NumCells);
   * where Data is RT_Data *
   * A series of vectors can be produced with the poorly named
   *  Data = RT_AllocateMemory(Nlos, Ncells);
   * where Data is RT_Data ** representing Nlos structures of arrays, each
   * array of length Ncells.
   */

  /* Allocate memory for SPH-RT fields : Structure of Arrays. */
  std::cout << "Allocating memory for RT Fields....";
  RT_Data *Data_host_p = RT_Data_Allocate(nParticles);
  /* Make a copy of the contents of the pointer, which is mostly just pointers. */
  if (Data_host_p == NULL) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to allocate memory for RT data.";
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }
  RT_Data Data_host = *Data_host_p;
  std::cout << "done" << std::endl;

  /* Number densities for H & He */
  double UnitMass_in_g = 1.989e+43 / (double)header.HubbleParam; /* g */
  /* Gadget Unit Length is 1 kpc */
  rt_float UnitLength_in_cm = 3.08568e+21 / (rt_float)header.HubbleParam / (1.0 + (rt_float)header.redshift); /* cm */
  double UnitMass = UnitMass_in_g / 1000.0;                                                                   /* kg */
  rt_float UnitLength = UnitLength_in_cm / 100.0;                                                             /* m */
  /* GadgetDensityUnit is 10^(43-3 - (3*18)) = 10^-14 so fits in single precision
   * but needs some assistance.
   *  Change to 10^(43-3 - 18 - 18 - 18) */
  rt_float GadgetDensityUnit =
      (rt_float)((UnitMass / (double)UnitLength) / (double)UnitLength) / (double)UnitLength; /* kg/m^3) */

  double GasParticleMass;
  if (header.mass[0] > 0.0) {
    GasParticleMass = (double)header.mass[0] * UnitMass; /* kg */
  } else {
    GasParticleMass = (double)P[0].Mass * UnitMass; /* kg */
  }
  rt_float density_to_n_H = (1.0 - Constants.Y) / (Constants.mass_p + Constants.mass_e) * GadgetDensityUnit;
  rt_float n_H_to_n_He = 0.25 * Constants.Y / (1.0 - Constants.Y); /* Nan if Y==1 */
  rt_float mu = 4.0 / (4.0 - 3.0 * Constants.Y);
  /* NumberOfHydrogenAtomsPerParticle ~ 10^(40 - (-27)) = 10^-67 so need double */
  double NumberOfHydrogenAtomsPerParticle =
      GasParticleMass * (double)(1.0 - Constants.Y) / (double)(Constants.mass_p + Constants.mass_e);
  double NumberOfHeliumAtomsPerParticle =
      GasParticleMass * (double)Constants.Y / (double)(4.0 * (Constants.mass_p + Constants.mass_e));
  float RayLength = 1.8 * header.BoxSize; /* Gadget Units. Should be a Parameter */

  /* Gadget's "u" == "Entropy" is Energy/Mass with units (km/s)^2 */
  /* GadgetUtoTFactor ~ 10^(6 -27 -(-23)) = 10^-2 */
  rt_float GadgetUtoTFactor = 1.e6 * (2. / 3.) * mu * Constants.mass_p / Constants.k_B;

  std::cout << "Setting Sources....";
  RT_SourceT *Source = (RT_SourceT *)malloc(Parameters.nSources * sizeof(RT_SourceT));
  Taranis_SetSources(Parameters, (rt_float)header.BoxSize, Constants, Source, UnitLength);
  /* Initialize the source functions */
  for (size_t i = 0; i < Parameters.nSources; ++i) {
    RT_SetSourceFunction(Source + i); /* Pointer Arithmetic */
  }
  std::cout << "done" << std::endl;

  /* Report the number if ionizing photons per second */
  printf("Multiply the following flux by N_Rays=%lu\n", Parameters.N_rays);
  RT_InitialiseSpectrum(0, Constants, &(Source[0]));

  size_t iParticle;

  /* Initialize Data_host (SPH particle) fields */
  if (RestartFile == NULL) {
    std::cout << "Initializing RT data...";
#pragma omp parallel for
    for (iParticle = 0; iParticle < nParticles; ++iParticle) {
      Data_host.R[iParticle] = 0.0; /* Distance to source (will be set later) [m] */
      Data_host.Density[iParticle] = GadgetDensityUnit * (rt_float)SphP[iParticle].Density; /* kg m^-3 */
      Data_host.dR[iParticle] = (rt_float)SphP[iParticle].Hsml * UnitLength;                /* m */
      Data_host.n_H[iParticle] = density_to_n_H * (rt_float)SphP[iParticle].Density;        /* m^-3 */
      ;
      if (Constants.Y < 1.) {
        Data_host.n_He[iParticle] = n_H_to_n_He * Data_host.n_H[iParticle];
      } else {
        /* Y==1 */
        Data_host.n_He[iParticle] = (rt_float)SphP[iParticle].Density / (4.0 * (Constants.mass_p + Constants.mass_e));
      }

      rt_float InitialTemperature = 0.0;
      rt_float InitialFractions[5];
      InitialFractions[0] = Parameters.InitialFractions[0]; /* f_H1 */
      InitialFractions[1] = Parameters.InitialFractions[1]; /* f_H2 */
      InitialFractions[2] = Parameters.InitialFractions[2]; /* f_He1 */
      InitialFractions[3] = Parameters.InitialFractions[3]; /* f_He2 */
      InitialFractions[4] = Parameters.InitialFractions[4]; /* f_He3 */
      switch (Parameters.ProblemType) {
        case 0: /* N sources placed in halos in mass order */
          InitialTemperature = GadgetUtoTFactor * (rt_float)SphP[iParticle].Entropy;
          break;

        case 1: /* Test 1 */
          InitialTemperature = Parameters.InitialTemperature;
          break;

        case 2: /* Test 2 */
        case 20:
          InitialTemperature = Parameters.InitialTemperature;
          break;

        case 3: /* Test 3 */
        {
          /* Clump of radius 0.8 kpc at (5, 3.3, 3.3) kpc in a 6.6 kpc box. */
          double clumpPos[3] = {5.0 * header.BoxSize / 6.6, 3.3 * header.BoxSize / 6.6, 3.3 * header.BoxSize / 6.6};
          double clumpR2 = 0.8 * header.BoxSize / 6.6;
          double R2 = (P[iParticle].Pos[0] - clumpPos[0]) * (P[iParticle].Pos[0] - clumpPos[0]) +
                      (P[iParticle].Pos[1] - clumpPos[1]) * (P[iParticle].Pos[1] - clumpPos[1]) +
                      (P[iParticle].Pos[2] - clumpPos[2]) * (P[iParticle].Pos[2] - clumpPos[2]);

          /* Clump has overdensity factor 200, temperature factor 1/200 relative to
           * environment.
           */
          InitialTemperature = Parameters.InitialTemperature;
          if (R2 <= clumpR2) {
            InitialTemperature /= 200.0;
          }
        } break;
        case 35: /* Modified Test 3 */
        {
          InitialTemperature = 100;

        } break;
        case 4: /* Test 4 */
          InitialTemperature = Parameters.InitialTemperature;
          break;

        default:
          printf("%s: %i: ERROR: Unknown ProblemType %i\n", __FILE__, __LINE__, (int)Parameters.ProblemType);
      }
      Data_host.f_H1[iParticle] = InitialFractions[0];
      Data_host.f_H2[iParticle] = InitialFractions[1];
      Data_host.f_He1[iParticle] = InitialFractions[2];
      Data_host.f_He2[iParticle] = InitialFractions[3];
      Data_host.f_He3[iParticle] = InitialFractions[4];
      /* Entropy [J/kg (m^3/kg)^(gamma_ad-1) */
      Data_host.Entropy[iParticle] = RT_EntropyFromTemperature(Constants, InitialTemperature, iParticle, Data_host);
      Data_host.T[iParticle] = InitialTemperature; /* Temperature [K] */
    }
  } else {
    /* Restart from a dump file */
    std::cout << "Attempting to restart from " << RestartFile << std::endl;
    RT_FileHeaderT FileHeader;
    size_t const NLos = RT_LoadData(RestartFile, &Data_host_p, &FileHeader);
    if (NLos == 0) {
      std::cout << "ERROR: Unable to read from " << RestartFile << std::endl;
      exit(EXIT_FAILURE);
    }
    Data_host = *Data_host_p;
    Parameters.t_start = (rt_float)FileHeader.Time;
    printf("Restarting at t=%5.3e s\n", Parameters.t_start);
  }
  std::cout << " done" << std::endl;

  /* Skip over output times which are in the past.
   * Parameters.t_start is now correct, even if a restart file was provided
   */
  size_t FirstOutputTimeIndex = 0;
  if (OutputTimes != NULL) {
    skipMissedOutputTimes(Parameters.t_start / outputTimesUnit, /* Million years */
                          OutputTimes,
                          nOutputTimes,
                          &FirstOutputTimeIndex);
    if (FirstOutputTimeIndex < nOutputTimes) {
      printf("First output time: %5.3e Ma\n", OutputTimes[FirstOutputTimeIndex] * outputTimesUnit / Constants.Ma);
    }
  }
  if ((OutputTimes == NULL && Parameters.OutputIterationCount == 0) || FirstOutputTimeIndex == nOutputTimes) {
    std::cout << "*****************************************" << std::endl
              << "WARNING: No output files will be written!" << std::endl
              << "*****************************************" << std::endl;
  }

  /* Sanity check */
  double TotalMassInVolume = 0.0;
  switch (Parameters.ProblemType) {
    case 0: /* N sources placed in halos in mass order */
      TotalMassInVolume = 3.549e+43;
      break;

    case 1: /* Test 1 */
      TotalMassInVolume = 1.131e+38;
      break;

    case 2: /* Test 2 */
    case 20:
      TotalMassInVolume = 1.131e+38;
      break;

    case 3: /* Test 3 */
      TotalMassInVolume = 7.021e+36;
      break;

    case 4: /* Test 4 */
      TotalMassInVolume = 4.238e+39;
      break;

    default:
      printf("%s: %i: ERROR: Unknown ProblemType %i\n", __FILE__, __LINE__, (int)Parameters.ProblemType);
  }
  double BoxVolume = (double)header.BoxSize * (double)header.BoxSize * (double)header.BoxSize * (double)UnitLength *
                     (double)UnitLength * (double)UnitLength;
  double MeanDensity = 0;
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    MeanDensity += (double)Data_host.Density[iParticle];
  }
  MeanDensity /= (double)nParticles;
  double Mean_nH = 0;
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    Mean_nH += (double)Data_host.n_H[iParticle];
  }
  Mean_nH /= (double)nParticles;
  printf("SANITY CHECK\n");
  printf(" MeanDensity * BoxVolume      = %5.3e kg\n", MeanDensity * BoxVolume);
  printf(" Mean_nH * m_H * BoxVolume    = %5.3e kg\n", Mean_nH * (Constants.mass_p + Constants.mass_e) * BoxVolume);
  printf(" nParticles * GasParticleMass = %5.3e kg\n", nParticles * GasParticleMass);
  printf(" Should be:                     %5.3e kg\n", TotalMassInVolume);
  double MeanTemperature = 0.;
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    MeanTemperature += (double)Data_host.T[iParticle];
  }
  MeanTemperature /= (double)nParticles;
  double MinEntropy = 1e30;
  double MeanEntropy = 0.;
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    MeanEntropy += (double)Data_host.Entropy[iParticle];
    if (MinEntropy > Data_host.Entropy[iParticle]) MinEntropy = Data_host.Entropy[iParticle];
  }
  MeanEntropy /= (double)nParticles;
  printf(" <T>          = %5.3e\n", MeanTemperature);
  printf(" <Entropy>    = %5.3e\n", MeanEntropy);
  printf(" min(Entropy) = %5.3e\n", MinEntropy);
  printf(" nParticles   = %lu; NumCells=%lu\n", nParticles, Data_host.NumCells);

  std::cout << "Loading data into Grace's format...";
  thrust::host_vector<SphereType> h_particles(nParticles); /* x,y,z, h (.w) */
  // thrust::host_vector<unsigned int> h_gadget_IDs(nParticles); /* ID */
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    h_particles[iParticle].x = P[iParticle].Pos[0];
    h_particles[iParticle].y = P[iParticle].Pos[1];
    h_particles[iParticle].z = P[iParticle].Pos[2];
    h_particles[iParticle].r = SphP[iParticle].Hsml;
  }
  std::cout << " done" << std::endl;

  std::cout << "Calling performRT..." << std::endl;
  ierr = performRT(/* Input, not modified. Supplied by host code. */
                   Source,
                   Parameters.nSources,
                   h_particles,
                   Parameters.t_start,
                   Parameters.t_stop,
                   UnitLength,
                   (rt_float)header.redshift,
                   (rt_float)header.BoxSize,
                   RayLength,
                   Parameters.N_rays,
                   NumberOfHydrogenAtomsPerParticle,
                   NumberOfHeliumAtomsPerParticle,
                   Constants,
                   0,
                   Data_host,
                   OutputTimes,
                   outputTimesUnit,
                   nOutputTimes,
                   FirstOutputTimeIndex,
                   Parameters.OutputIterationCount);

  if (ierr != EXIT_SUCCESS) {
    printf("%s: %i: performRT failed\n", __FILE__, __LINE__);
    exit(EXIT_FAILURE);
  }
  std::cout << " Returned from performRT" << std::endl;

  if (OutputTimes != NULL) {
    free(OutputTimes);
    OutputTimes = NULL;
  }

  // commenting this out does not remove the segfault
  cudaSetDevice(0);
  std::cout << "Resetting RT device" << std::endl;
  cudaError_t cudaError = cudaDeviceReset();
  if (cudaError != cudaSuccess) {
    printf("cudaDeviceReset on device %i returns: %s\n", 0, cudaGetErrorString(cudaError));
  }
}
