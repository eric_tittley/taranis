ROOT_DIR := $(CURDIR)
MAKE_CONFIG := $(ROOT_DIR)/make.config

include $(MAKE_CONFIG)

EXEC = Taranis

EXEC_SRC = Taranis.cpp

INCS = $(INCS_BASE) \
       $(INC_GRACE) \
       $(INC_GSL) \
       $(INC_MPI) \
       $(INC_RT) \
       $(NVCC_INC)\
       $(INC_LIBCUDASUPPORT) \
       $(INC_LIBMANIDOOWAG) 

ExtraPackages = grace RT RT_CUDA libPF libcudasupport libManidoowag

LOCAL_LIBS = libTaranis/libTaranis.a \
             libManidoowag/libManidoowag.a \
             RT_CUDA/libRT_CUDA.a \
             RT/libRT.a \
             libPF/libPF.a \
             libChealpix/libchealpix.a \
             libcudasupport/libcudasupport.a

SUBDIRS = libcudasupport libTaranis RT RT_CUDA libPF libChealpix libManidoowag

ifeq ($(GADGET_BINDINGS),1)
 INCS += $(INC_GADGET2)
 ExtraPackages += Gadget2
 LOCAL_LIBS += libGadget2/libGadget2.a
 SUBDIRS +=  libGadget2
 GADGET2_OBJS = libGadget2/read_ic.o libGadget2/allvars.o
 DEFS += -DPMGRID
endif

GITDIRS = . RT RT_CUDA grace libPF libcudasupport libManidoowag

LIBS =  $(LOCAL_LIBS) $(LIB_ARCH) -lm -lrt -ldl \
        $(LIB_GSL) $(LIB_MPI) $(NVCC_LIBS) $(NCCL_LIBS)

DEFS += -DHAVE_CONFIG

export CC CFLAGS CXX DEFS INCS_BASE AR AR_FLAGS LD LD_FLAGS

all: libs

.PHONY: libs
libs: $(ExtraPackages) $(LOCAL_LIBS)

$(EXEC): $(ExtraPackages) Taranis.o $(LOCAL_LIBS) revision
	$(NVCC) $(NVCCFLAGS) -dlink -o Taranis_link.o Taranis.o
	$(LD) -o $(EXEC) $(LD_FLAGS) Taranis.o Taranis_link.o $(LIBS)

Taranis.o: RT Gadget2 libcudasupport grace libManidoowag

.PHONY: install
install:
	install $(EXEC) $(INSTALL_DIR)

# Dependency tips:
# If you need a library to produce a CUDA link.o file, then depend on the
# library.
# If you need a directory to access a header file and that directory is not
# part of the repository, then depend on the directory unless the library in
# the directory is already a dependency from the first tip.

libTaranis/libTaranis.a: RT grace libcudasupport RT_CUDA/libRT_CUDA.a libManidoowag/libManidoowag.a
	$(MAKE) ROOT_DIR=${ROOT_DIR} -C libTaranis

RT/libRT.a: RT
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=${MAKE_CONFIG} -C RT

RT_CUDA/libRT_CUDA.a: RT_CUDA RT
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=${MAKE_CONFIG} RT_CUDA_MEMORY_MODEL=${RT_CUDA_MEMORY_MODEL} -C RT_CUDA

libPF/libPF.a: libPF
	$(MAKE) -C libPF

libGadget2/libGadget2.a: Gadget2
	$(MAKE) -C libGadget2 lib

libChealpix/libchealpix.a: RT
	$(MAKE) CC=$(CC) OPT="$(CFLAGS)" WITHOUT_CFITSIO=1 -C libChealpix

libcudasupport/libcudasupport.a: libcudasupport RT
	$(MAKE) CC=$(CC) CFLAGS="$(CFLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(CXXFLAGS)" \
		NVCC=$(NVCC) NVCCFLAGS="$(NVCCFLAGS)" \
		ROOT_DIR=$(ROOT_DIR) INC_CUDA=$(NVCC_INC) \
		AR=$(AR) ARFLAGS="$(ARFLAGS)" -C libcudasupport

libManidoowag/libManidoowag.a: libManidoowag RT RT_CUDA libcudasupport
	$(MAKE) ROOT_DIR=$(ROOT_DIR) -C libManidoowag

grace:
	$(GIT_CLONE_CMD)eric_tittley/grace.git --depth 1

RT:
	$(GIT_CLONE_CMD)eric_tittley/rt.git --depth 1
	mv rt RT

RT_CUDA:
	$(GIT_CLONE_CMD)eric_tittley/rt_cuda.git --depth 1
	mv rt_cuda RT_CUDA

libManidoowag:
	$(GIT_CLONE_CMD)eric_tittley/libManidoowag.git --depth 1

libPF:
	$(GIT_CLONE_CMD)eric_tittley/libpf.git --depth 1
	mv libpf libPF

libcudasupport:
	$(GIT_CLONE_CMD)eric_tittley/libcudasupport --depth 1

Gadget2:
	wget -q http://www.mpa-garching.mpg.de/gadget/gadget-2.0.7.tar.gz
	tar xzf gadget-2.0.7.tar.gz
	ln -s Gadget-2.0.7/Gadget2
	rm gadget-2.0.7.tar.gz

.PHONY: clean
clean:
	-rm *.o
	-rm *~
	-rm Make.log
	-rm -f splint.log
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir clean ) \
	done

.PHONY: distclean
distclean: clean
	-rm $(EXEC)
	-rm -f Revisions.tmp
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir distclean ) \
	done

.PHONY: rebuild
rebuild: distclean all

.PHONY: check
check:
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir check ) \
	done
	splint +posixlib -nullderef -nullpass $(INCS) $(DEFS) $(EXEC_SRC) >> splint.log

.PHONY: update
update:
	rm -f $(DEPS)
	for dir in $(GITDIRS) ; do \
	(cd $$dir && echo $$dir && git pull); \
	done

.PHONY: status
status:
	rm -f $(DEPS)
	for dir in $(GITDIRS) ; do \
	 (cd $$dir && echo $$dir && git status && echo); \
	done

.PHONY: diff
diff:
	rm -f $(DEPS)
	for dir in $(GITDIRS) ; do \
		(cd $$dir && echo $$dir && git diff --stat && echo); \
	done

.PHONY: push
push:
	rm -f $(DEPS)
	for dir in $(GITDIRS) ; do \
	 (cd $$dir && git push); \
	done

.PHONY: revision
revision:
	echo > Revisions.tmp
	for dir in $(GITDIRS) ; do \
	 (cd $$dir && \
	 $(ROOT_DIR)/bin/gitReportBranchVersion.csh >> $(ROOT_DIR)/Revisions.tmp && \
	 echo >> $(ROOT_DIR)/Revisions.tmp); \
	done
	echo "\n------------- config.h ------------------" >> Revisions.tmp 
	cat config.h >> Revisions.tmp
	echo "-------------- end ----------------------" >> Revisions.tmp 
	echo "\nCFLAGS:\n $(CFLAGS)" >> Revisions.tmp
	echo "\nCXXFLAGS:\n $(CXXFLAGS)" >> Revisions.tmp
	echo "\nNVCCFLAGS:\n $(NVCCFLAGS)" >> Revisions.tmp
