#ifndef _TARANIS_H_
#define _TARANIS_H_

#include <assert.h>

#include <cmath>
#include <cstdlib>
#include <iostream>
/* Defines CUDA_VERSION */
#include <cuda.h>
#include <thrust/host_vector.h>

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_Precision.h"
#include "grace/sphere.h"
#include "grace/types.h"

extern "C" {
/* RT includes */
#include "Logic.h"
#include "RT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"
#include "RT_TimeScale.h"
}
#include "Parameters.h"
#include "RT_ConstantsT.h"
#include "RT_SourceT.h"

/********
 * Debug and Logging
 * A_LOG(...) is always output and can be used instead of printf
 * V_LOG(...) replaces #ifdef VERBOSE... with a neater call
 ********/
#define A_LOG(...)              \
  do {                          \
    if (1) printf(__VA_ARGS__); \
  } while (0)

#define V_LOG(...)                         \
  do {                                     \
    if (VERBOSE >= 1) printf(__VA_ARGS__); \
  } while (0)

#define DEBUG_LOG(...)              \
  do {                              \
    if (DEBUG) printf(__VA_ARGS__); \
  } while (0)

#define IDUMP(varname) fprintf(stderr, "%s = %d\n", #varname, varname);
#define FDUMP(varname) fprintf(stderr, "%s = %e\n", #varname, varname);

typedef grace::Sphere<float> SphereType;


enum CodeTimes {
  ZeroingRates,
  RayCreation,
  RayTracing,
  ColumnDensities,
  Distances,
  SPPRates,
  RecombinationRates,
  CoolingRates,
  UpdateHeatingRates,
  SetHydrogenEquilibriumValues,
  FindingTimestep,
  UpdatingParticles,
  IO,
  InvolatilesLuminosity,
  RTDataBcast,
  RTRatesReduction,
  nCodeTimes
};

#define TARANIS_GOT_TO std::cerr << "At " << __FILE__ << "@" << __LINE__ << std::endl;

void checkCudaAPICall(cudaError_t code, const char *file, int line, bool terminate = true);

cudaError_t checkCudaKernelLaunch(const char *file, int line, bool terminate = true);

void copyRTDataToDevice(RT_Data const Data_src, RT_Data *Data_dest);

int performRT(/* Input, not modified. Supplied by host code. */
              RT_SourceT *const Source,
              size_t const nSource,
              thrust::host_vector<SphereType> &h_particles,
              rt_float const t_start,
              rt_float const t_stop,
              rt_float const UnitLength,
              rt_float const Redshift,
              rt_float const BoxSize,
              float const RayLength,
              size_t const N_rays,
              double const N_H_per_particle,
              double const N_He_per_particle,
              RT_ConstantsT Constants,
              int const CudaDevice,
              /* Modified */
              RT_Data Data_host,
              /* Optional, not modified */
              rt_float const *const OutputTimes = NULL,
              rt_float const outputTimesUnit = 0.0,
              size_t const nOutputTimes = 0,
              size_t const FirstOutputTimeIndex = 0,
              size_t const DumpIterationCount = 0);

void printMemory(int const CudaDevice1);

int dumpColumnDensitiesAndMaps(thrust::host_vector<float> &Ncol_HI,
                               thrust::host_vector<float> &Ncol_HeI,
                               thrust::host_vector<float> &Ncol_HeII,
                               thrust::host_vector<unsigned int> &particle_indices,
                               thrust::host_vector<unsigned int> &gadget_map);

void ionizeIfNearSource(double const px,
                        double const py,
                        double const pz,
                        double const hsml,
                        RT_SourceT const *const Source,
                        size_t const nSources,
                        rt_float *const IonizationFractions);

int readParameters(char const *const ParametersFile, ParametersT *const Parameters);

void setGridAndBlockSize(size_t const nLoopElements, void *KernelPointer, int *gridSize, int *blockSize);

void Taranis_SetSources(ParametersT const Parameters,
                        rt_float const BoxSize,
                        RT_ConstantsT const Constants,
                        RT_SourceT *const Source,
                        rt_float const UnitLength);

#ifdef PROGRESSIVE_N_RAYS
size_t progressiveHealpixRays(rt_float FractionOfTotalTime, size_t MaxNRays);
#endif

#ifdef FOF_SOURCES
int GIZMO_IntegralInvolatiles_setLuminosity(int Source_id,
                                            RT_ConstantsT const Constants,
                                            RT_IntegralInvolatilesT Involatiles);
#endif

#endif
