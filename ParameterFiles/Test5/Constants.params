# Physical Constants 

# Ionisation thresholds (physics.NIST.gov)
nu_0   3.288086858e15
nu_1   5.945204211e15
nu_2  13.158157146e15

# Constants for interpolation formula for cross-section (m^2),
# Osterbrock p.36 
sigma_0  6.30e-22
sigma_1  7.83e-22
sigma_2  1.58e-22

# From CODATA (physics.nist.gov) 
c        2.99792458e8  # Speed of light (m/s) 
h_p      6.6260688e-34 # Planck constant. (J s) 
k_B      1.380650e-23  # Boltzmann constant (J/K) 
mass_e   9.1093819e-31 # electron mass (kg) 
mass_p   1.6726217e-27 # proton mass (kg) 
sigma_T  6.6524585e-29 # Thomson cross section (m^2) 
a        7.565760e-16  # Radiation density constant ( J m^-3 K^-4 ) 

# From Astrophysical Formulae (Lang) 
Mpc      3.085677582e22 # 1 Mpc (m) 

# From Kaplinghat & Turner (2001 PhRvL 86 385K) 
T_CMB_0  2.725         # Present CMB temperature (K) 

# Adiabatic index for a monoatomic gas
gamma_ad 1.66666666666667 # 5/3

# Cosmological Constants 
Y       0.0 # Helium mass fraction 

h       1.0 # h_100.  Must match H_0, omega_[bmv] below 
omega_b 1.0 #
omega_m 1.0 # Density of all matter / critical density 
omega_v 0.0 # Vacuum energy density 

# Configuration Parameters 

# MIN_DT is misleadingly named. It is #not# a hard lower limit to timesteps.
# If ADAPTIVE_TIMESCALE is defined, then MIN_DT is applied when timescales
# are not yet known: on the first RT iteration (1 per PM iteration) and when 
# the lightfront first enters a cell.  So mostly in the epoch before the
# light front has swept through the volume.
# If ADAPTIVE_TIMESCALE is not defined, then MIN_DT is the time step in the
# Radiative Transfer Section. As a guide, MIN_DT < 1e14 s 
MIN_DT                            1e6

# MAX_DT sets a hard maximum timestep. Should not be needed with adaptive
# timesteps, but may be needed to sychronise LOS segments on different
# grids. For reference, the Hubble time is 4e17s.
# Tests (10 04 27) indicate very little runtime penalty (and potentially
# a benefit) until MAX_DT < 5e11 but clear benefits for MAX_DT < 5e13 .
# MAX_DT < MIN_DT will probably break things. 
MAX_DT                            5e13

# If ADAPTIVE_TIMESCALE, then the RT timestep is set to
# (Timestep_Factor * the suggested adaptive timescale).  Tests find
# convergence at 0.2. Don't exceed 0.5 or instabilities arise.
Timestep_Factor                   0.2

OnePlusEps                        1.000001
OneMinusEps                       0.999999

# xHIEquilibriumThreshold defines the HI fraction below which species fractions
# for Hydrogen are set to equilibrium values.
# This functionality is necessary to prevent timestepping instabilities which
# crop up when fractions approach equilibrium values.
xHIEquilibriumThreshold     0.01

# The number of sample frequencies in each of the three intervals:
#  nu_0 to nu_1
#  nu_1 to nu_2
#  nu_2 to infinity

NLevels 3
 4
 4
 8
