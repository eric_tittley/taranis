# ProblemType
#  0 N sources, placed in halo centres in decreasing halo mass [static]
#  1 Test 1 A source in a uniform medium, constant temperature [static]
#  2 Test 2 A source in a uniform medium, temperature allowed to vary [static]
#  3 test 3 Spherical clump ionized by plane-parallel rays [static]
#  4 Test 4 Static cosmological field
#  5 Test 5 A source in a constant-temperature uniform medium [hydrodynamic]
#  6 Test 6 A source in a clump [hydrodynamic]
ProblemType 5

InitialTemperature 100 # K
t_start            0.0 # s
t_stop             1.5779e+16 # 500 Ma
Nsph               32 # Number of particles in an SPH search radius
nSources 1   # Number of sources

# The number of rays to trace per source per radiative transfer timestep.
# If HEALPIX_RAYS is defined in config.h (it should be for cosmological runs),
# N_rays must satisfy
#  N_rays = 12 n^2 [Healpix distributions]
#  N_rays = 32 m   [rays per cuda warp]
#   where n and m are integers.
#  Some valid values are:
#   192         768        1728        3072        4800
#  6912        9408       12288       15552       19200
# 23232       27648       32448       37632       43200
# 49152       55488       62208       69312       76800
# 84672       92928      101568      110592      120000
N_rays          19200

# Initial fractions X=[X_H1 X_H2 X_He1 X_He2 X_He3]
InitialFractions 5
 0.99999 # ~ 1.0
 1e-5    # ~ 0.0 but reduces minimum timestep for 'unionized' particles
 1.0
 0.0
 0.0

# RT snapshot output times in units of Ma
OutputTimesFile output_times.dat
