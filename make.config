# The Compiler Suite: INTEL or GCC: ONLY GCC WORKS AT THE MOMENT
SUITE = GCC

# Set GIT_ANONYMOUS if you don't want write access to the repositories or
# you don't have a BitBucket account
GIT_ANONYMOUS = 0

HOST ?= $(shell hostname)
#HOST = cuillin

# MPI_IMP is the MPI implementation and can be one of OPENMPI, MPICH, LAMMPI
# Using OPENMPI will work for most implementations in addition to OpenMPI
# OPENMPI is the default
MPI_IMP = OPENMPI

# PROFILING (0==off, 1==on).
PROFILE = 0

# Debugging symbol information (and set -O0) (0==off, 1==on)
DEBUG = 0

#SERIAL == 1 compiles a serial version of RT() instead of a parallel.
SERIAL = 0

#OPENMP == 1 compiles an OpenMP version of RT().
OPENMP = 1

#MPI == 1 compiles a MPI version of RT().
#MPI = 0

# If any component needs CUDA, then set to 1.
# Comment out to disable, don't set to zero
CUDA = 1

# Process RT through GPU using CUDA
# See also config.h
RT_CUDA = 1

# Set to 1 if bindings to Gizmo are to be built
GIZMO_BINDINGS = 1

# Set to 1 if bindings to Gadget are to be built
GADGET_BINDINGS = 0

# Default archiving tool and flags
AR = ar
#AR_FLAGS = 

MAKE = make

# Libraries to link to. NOT their location, just the library names.
MPI_LIBS = -lmpi -lmpi_cxx
GSL_LIB = -lgsl -lgslcblas
ifeq ($(MPI_IMP),MPICH)
 MPI_LIBS = -lmpich
endif

# GNU Compiler
ifeq ($(SUITE),GCC)
 MAKE         = make
 CC           = gcc
 CXX          = g++
 DEBUGFLAGS   = -O0 -g
 OPTFLAGS     = -O2
 WARNFLAGS    = -Wall
 LD           = mpiCC
 LD_FLAGS     = 
 DEPEND       = cpp
 DEPEND_FLAGS = -MM
 INC_MPI      =
 MPI_LIBS     = -lmpi
 # "As of version 4.2, nvcc does not support device debugging Thrust code.
 #  Thrust functions compiled with (e.g., nvcc -G,
 #  nvcc --device-debug 0, etc.) will likely crash.
 #NVCC_DEBUGFLAGS = -g -G
 NVCC_DEBUGFLAGS = -O0 -g -DGRACE_DEBUG -D_DEBUG -DTHRUST_DEBUG
 NVCC_OPTFLAGS   = -O3 -use_fast_math
# NVCC_DIAGNOSTICS = --ptxas-options=--verbose
 GENCODE_20 = -gencode arch=compute_20,code=sm_20
 GENCODE_30 = -gencode arch=compute_30,code=sm_30
 GENCODE_35 = -gencode arch=compute_35,code=sm_35
 GENCODE_52 = -gencode arch=compute_52,code=sm_52
 GENCODE_61 = -gencode arch=compute_61,code=sm_61
 GENCODE_70 = -gencode arch=compute_70,code=sm_70
 GENCODE_75 = -gencode arch=compute_75,code=sm_75
 # The following over-ride the defaults set above
 ifeq ($(HOST),cuillin)
  $(info REQUIRED: module load cuda nccl hdf5-serial openmpi )
  CC          = gcc
  CXX         = g++
  CFLAGS_ARCH = -march=native
  OPTFLAGS    = -O3 -flto -ftree-vectorize
  INC_MPI     = -I$(MPI_INC)
  LIB_MPI     = -L$(MPI_LIB) $(MPI_LIBS)
  AR          = gcc-ar
  # worker001 has 3 2080ti  -  sm 75
  # worker093 has 4 6000 Ada Lovelace - sm 89
  NVCC_ARCH   = $(NVCC_GENCODE_6000)
  #NVCC_ARCH   = $(NVCC_GENCODE_2080)
  NVCC_LIBS   = -L$(CUDA_LIB) -lcudart -lcurand -lcudadevrt 
  # For Gizmo (incl HDF5_DIR)
  ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
  GIZMO_DIR   = $(ROOT_DIR)/../gizmo-rt
 endif
 ifeq ($(HOST),arrakis)
  $(info REQUIRED: module load openmpi hdf5-serial cuda/12.4 nccl/12.4 )
  CC          = gcc-13
  CXX         = g++-13
  CFLAGS_ARCH = -march=native -ftree-vectorize
  OPTFLAGS    = -O3 -flto
  AR          = gcc-ar-13
  INC_MPI     = -I$(MPI_INC)
  MPI_LIBS    = -lmpi
  LIB_MPI     = -L$(MPI_LIB) $(MPI_LIBS)
  # arrakis has two GPUs:
  #  GTX 1080ti (GENCODE_61)
  #  TITAN V    (GENCODE_70)
  NVCC_ARCH   = $(GENCODE_70)
  NVCC_LIBS   = -L$(CUDA_LIB) -lcudart -lcurand -lcudadevrt
  NCCL_LIBS   = -L$(NCCL_LIB) -lnccl_static
  # For Gizmo (incl HDF5_DIR)
  ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
  GIZMO_DIR   = $(ROOT_DIR)/../gizmo-rt
 endif
 ifeq ($(HOST),phlebas)
  $(info REQUIRED: module load openmpi hdf5-serial cuda/12.8 nccl/12.8 )
  CC          = gcc-14
  CXX         = g++-14
  # --ffast-math reorders my carefully ordered operations.
  CFLAGS_ARCH = -march=native -ftree-vectorize
  OPTFLAGS    = -O3 -flto
  INC_MPI     = -I$(MPI_INC)
  MPI_LIBS    = -lmpi
  LIB_MPI     = -L$(MPI_LIB) $(MPI_LIBS)
  # Until binutils is updated, the wrapper gcc-ar is needed to invoke ar
  # when the Link Time Optimization flag (-flto) is used.
  AR          = gcc-ar-14
  NVCC_ARCH   = $(NVCC_GENCODE)
  NVCC_LIBS   = -L$(CUDA_LIB) -lcudart -lcurand -lcudadevrt
  NCCL_LIBS   = -L$(NCCL_LIB) -lnccl_static
  # For Gizmo (incl HDF5_DIR)
  ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
  GIZMO_DIR   = $(ROOT_DIR)/../gizmo-rt
 endif

 LD = $(CXX)
 ifeq ($(PROFILE),1)
  OPTFLAGS   += -pg
  LD_FLAGS   += -pg
 endif
 ifeq ($(OPENMP),1)
  OMPFLAGS = -fopenmp
 endif
 NVCC        = nvcc
endif

ifeq ($(DEBUG),1)
 OPTFLAGS      = $(DEBUGFLAGS)
 NVCC_OPTFLAGS = $(NVCC_DEBUGFLAGS)
endif
ifeq ($(PROFILE),1)
 NVCC_OPTFLAGS += -pg -lineinfo
endif
CFLAGS    = $(CFLAGS_ARCH) $(OMPFLAGS) $(OPTFLAGS) $(WARNFLAGS)
CXXFLAGS  = $(CFLAGS)
NVCCFLAGS = $(NVCC_ARCH) $(NVCC_OPTFLAGS) $(NVCC_DIAGNOSTICS) --expt-relaxed-constexpr
ifeq ($(OPENMP),1)
 NVCCFLAGS += -Xcompiler -fopenmp
endif
LD_FLAGS += $(OPTFLAGS) $(OMPFLAGS) 

INCS_BASE = -I. -I$(ROOT_DIR) -I$(NCCL_INC)

INC_RT = -I$(ROOT_DIR)/RT

INC_RT_CUDA = -I$(ROOT_DIR)/RT_CUDA
RT_CUDA_MEMORY_METHOD = SM
LIB_RT_CUDA = $(ROOT_DIR)/RT_CUDA/libRT_CUDA.a
LIB_MANIDOOWAG = $(ROOT_DIR)/libManidoowag/libManidoowag.a
LIB_NCCL = -L$(NCCL_LIB) -lnccl_static 

INC_GRACE = -I$(ROOT_DIR)/grace/include

INC_GADGET2 = -I$(ROOT_DIR)/Gadget2

INC_LIBCUDASUPPORT = -I$(ROOT_DIR)/libcudasupport

INC_LIBMANIDOOWAG = -I$(ROOT_DIR)/libManidoowag

INC_GIZMO = -I$(GIZMO_DIR)
INC_HDF5  = -I$(HDF5_INC)

# THRUST
INC_THRUST  = $(NVCC_INC)

LIB_GSL     = -L$(GSL_DIR)/lib $(GSL_LIB)

# One or the other must be set
RT_CUDA_MEMORY_MODEL = SM
#RT_CUDA_MEMORY_MODEL = NO_SM

GIT_CLONE_CMD = git clone git@bitbucket.org:
ifeq ($(GIT_ANONYMOUS),1)
 GIT_CLONE_CMD = git clone https://bitbucket.org/
endif

# On some systems we need the quotes (?) which fails on others.
#GIT_CLONE_CMD = "git clone git@bitbucket.org:"
#ifeq ($(GIT_ANONYMOUS),1)
# GIT_CLONE_CMD = "git clone https://bitbucket.org/"
#endif

.SUFFIXES: .o .c .cu

.c.o:
	$(CC) $(CFLAGS) $(DEFS) $(INCS) -c $<

.cpp.o:
	$(CXX) $(CFLAGS) $(DEFS) $(INCS) -c $<

.cu.o:
	$(NVCC) $(NVCCFLAGS) $(DEFS) $(INCS) -dc $<
